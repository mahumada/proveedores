const mongoose = require("mongoose");

const ChatSchema = new mongoose.Schema(
  {
    members: {
      type: Array,
    },
    messages: { type: Array },
  },
  { timestamps: true }
);

module.exports = mongoose.model("Chat", ChatSchema);
