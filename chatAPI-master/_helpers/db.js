const mongoose = require( "mongoose");
require("dotenv").config();
// Use new db connection
mongoose.connect(process.env.mongodburl, {
  useUnifiedTopology: true,
  //   useCreateIndex: true,
  useNewUrlParser: true
});

console.log("DB COnectada")
console.log(process.env.mongodburl)
module.exports = {
  // User: require("../users/user.model"),
  Chat: require("../chats/chat.model"),
  Message: require("../messages/message.model"),
};;