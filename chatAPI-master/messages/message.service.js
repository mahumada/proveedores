const db = require("../_helpers/db");
const Message = db.Message;

module.exports = {
  newMessage,
  getMessages,
  getMessage
};

// Create a new message

async function newMessage(chatId, sender, text) {
  const message = new Message({ chatId, sender, text, seen: false });
  const savedMessage = await message.save();
  console.log(message, savedMessage);
  return savedMessage;
}

// Get all chat messages

async function getMessages(chatId) {
  const allMessages = await Message.find({ chatId })
    .select("sender text createdAt")
    .lean();
  return allMessages;
}

async function getMessage(id){
  const msg = await Message.findById(id);
  return msg;
}