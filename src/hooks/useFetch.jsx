import { useEffect, useState } from 'react'
import axios from 'axios'

const useFetch = (url, method, body) => {
	const [data, setData] = useState([])
	const [error, setError] = useState('')
	const [loading, setLoading] = useState(false)

	useEffect(() => {
		const getData = async () => {
			if (!url || !method) return
			setLoading(true)

			try {
				const response = await axios({
					method,
					headers: { 'Content-Type': 'application/json' },
					url,
					body,
				})
				setData(response.data)
			} catch (error) {
				console.log(error)
				setError(error)
				setLoading(false)
			} finally {
				setLoading(false)
			}
		}
		getData()
	}, [])

	return { data, error, loading }
}
export default useFetch