import mongoose from "mongoose";

const QuoteSchema = new mongoose.Schema({
  pdc: String,
  paymentMethod: {
    type: String,
    required: true,
  },
  
  notes: String
})

const ProposalSchema = new mongoose.Schema({
  minimum: {
    type: String,
    required: true,
  },
  shippingInterval: {
    type: String,
    required: true,
  },
  endDate: {
    type: Date,
    required: true
  },
  paymentTerms: String,
  includesTransport: Boolean,
  notes: String,
})
const ContactSchema = new mongoose.Schema({
  
  sellingAddress: {
    type: String,
    required: true
  },
  contactName: {
    type: String,
    required: true
  },
})
const VariantsSchema = new mongoose.Schema({
  variantType: {
    type: String,
    required: true
  },
  quantity: {
    type: String,
    required: true
  },
  price: {
    type: String,
    required: true
  },
  description: {
    type: String,
  },
  brand: {
    type: String,
  },
  color: {
    type: String,
  },
  image: {
    type: String
  },
  unit: {
    type: String
  }
})

const PostSchema = new mongoose.Schema({
  seller_id: {
    type: mongoose.ObjectId,
    required: true,
  },
  quote: QuoteSchema,
  proposal: ProposalSchema,
  contact: ContactSchema,
  variants: [VariantsSchema],
  categories: {
    type: Array
  },
  views: Number
}, { timestamps: { createdAt: "createdAt", updatedAt: "updatedAt" } });

module.exports = mongoose.models.Post || mongoose.model("Post", PostSchema, "Posts");
