import mongoose from 'mongoose';

const UserSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true
  },
  userId: {
    type: String
  }
}, {timestamps: {createdAt: "createdAt", updatedAt: "updatedAt"}});

module.exports = mongoose.models.AskedCategory || mongoose.model("AskedCategory", UserSchema, "AskedCategories");