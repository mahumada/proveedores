import mongoose from 'mongoose';

const SellerSchema = new mongoose.Schema({
  socialReason: String,
  fantasyName: String,
  address: String,
  userType: String,
  user_id: {
    type: mongoose.ObjectId,
    required: true,
  },
  username: {
    type: String,
    required: true,
  },
  email: {
    type: String,
    unique: true,
    required: true,
  },
  cuit: {
    type: String,
    required: true
  },
  postalCode: {
    type: String,
    required: true,
  },
  city: {
    type: String,
    required: true,
  },
  province: {
    type: String,
    required: true,
  },
  category: String,
  phone: {
    type: String,
    required: true,
  },
  interactions: Number
}, { timestamps: { createdAt: "createdAt", updatedAt: "updatedAt" } });

module.exports = mongoose.models.Seller || mongoose.model("Seller", SellerSchema, "Seller");


