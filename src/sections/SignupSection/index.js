import Form from '../../components/Login/Form'
import Info from '../../components/Login/Info'
import Switch from '../../components/Login/Switch'
import ErrorAlert from '../../components/common/ErrorAlert';
import {
	Container,
	FormContainer,
	Slides,
	SlideButtons,
	SlideBtn,
	Square,
	LogoContainer,
	Text,
	Image,
	MiluxPreview,
	Logo
} from './styles'
import React, { useEffect } from 'react'
import Slide from '../../components/Login/Slide'
import axios from 'axios'

const SignupSection = () => {

	useEffect(() => {
		localStorage.getItem('loggedUser') && axios.get(
			`/api/user/getseller?_id=${JSON.parse(localStorage.getItem('loggedUser')).id}`
			).catch(err => window.location = '/sellerSignup');
	}, [])

	const slides = [
		{
			title: 'Compra y Vende Gratis',
			subtitle: 'Solo con un par de clicks',
			imgUrl: '/img/slide1.png',
		},
		{
			title: 'Contribuye con la comunidad',
			subtitle: 'Encontrá personas que venden productos',
			imgUrl: '/img/slide2.png',
		},
		{
			title: 'Compra y Vende Gratis',
			subtitle: 'Solo con un par de clicks',
			imgUrl: '/img/slide1.png',
		},
	]
	const [slideIndex, setSlideIndex] = React.useState(0)
	const [error, setError] = React.useState('')
	return (
		<>
		{/* <ErrorAlert error={error} /> */}
		<Container>
			<Slides>
				<MiluxPreview>
					<Logo src="/img/loginPreviewLogo.png" />
					<Image src="/img/loginPreview.png" />
				</MiluxPreview>
			</Slides>
			<FormContainer>
				<Info
					title='Empieza ahora'
					subtitle='Crea tu cuenta para ingresar'
				/>
				<Form setError={setError} mode='signup' />
				<Switch mode='signup' />
			</FormContainer>
		</Container>
		</>
	)
}

export default SignupSection
