import styled from 'styled-components'

export const Container = styled.div` 
    /* display: flex; */
	/* flex-wrap: wrap; */
	width: 100%;
	height: ${props => props.height};
	/* justify-content: space-between;
	align-items: center; */
	scroll-snap-type: y mandatory;
    overflow-y: auto;
		position: relative;

	@media (max-width: 1075px) {
		justify-content: center;
		padding-top: 24px;
	}
`