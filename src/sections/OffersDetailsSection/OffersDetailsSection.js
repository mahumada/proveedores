import { Container } from './styles';
import { useEffect, useState } from 'react';
import axios from 'axios';
import Post from '../../components/common/PostHome';

const OffersDetailsSection = ({ id, socket, height }) => {
    const [post, setPost] = useState(null)
    const [data, setData] = useState();
    
    useEffect(()=>{
        const a = async() => {
            const newData = await fetch(`/api/offer?_id=${id}`).then(res => res.json()).catch(err => console.error(err));
            console.log(newData);
            setData(newData);
        }
        if(id){
            a();
        }
    }, [id]);

    
    useEffect(() => {
        if (data != undefined) {
            axios.get(`/api/post/${data?.post_id}`, 'get').then(response => {setPost(response.data.data)}).catch(error => console.log(error))
        }

    }, [data])

    useEffect(() => {
        const a = async() => {
            await axios.get(`/api/offer/read?id=${id}`);
        }
        if(data && data.buyer_id && JSON.parse(localStorage.getItem('loggedUser')).id !== data.buyer_id){
            a();
        }
    }, [data, id])

    useEffect( () => { 
		document.querySelector("body").style.height = `${height}px`; 
	}, [height]);

    const [sellers, setSellers] = useState([]);
	const [users, setUsers] = useState([]);
	useEffect(() => {
		const a = async() => {
			const s = await axios.get('/api/seller');
			const u = await axios.get('/api/user/');
			setSellers(s.data);
			setUsers(u.data);
		}
		a();
	}, [])

    return (<Container height={`${height - 70}px`}>
        {post && <Post
                                    offered
									socket={socket}
									datos={{
										id: post._id,
										/* Cotizacion*/
										pcd: post.quote.pdc,
										payment: post.quote.paymentMethod,
                    observations1: post.quote.notes,
                    /* Propuesta*/
                    validationInit: post.createdAt,
                    billing: post.proposal.minimum,
                    validation: post.proposal.endDate,
                    includesTransportation: post.proposal.includesTransport,
											paymentTerms: post.proposal.paymentTerms,
											deliveryDate: post.proposal.shippingInterval,
                    /*paymentConditions: post.proposal.,*/
                    observations2: post.proposal.notes,
										/*Contacto*/
										deliveryAdress:
											post.contact.sellingAddress,
										contactName: post.contact.contactName,
										variants: post.variants,
										id: post._id,
										categories: post.categories
									}}
									sellerId={post.seller_id}
									categories={post.categories}
                                    seller={sellers.filter(seller => seller._id === post.seller_id)[0]}
										sellerData={users.filter(user => user._id === sellers.filter(seller => seller._id === post.seller_id)[0]?.user_id)[0]}
                                    />}

    </Container>)


}
export default OffersDetailsSection