import styled from "styled-components";

export const Wrapper = styled.div`
  width: 100%;
  height: 100%;
  margin-top: 40px;
  display: flex;
  flex-direction: column;
  align-items: center;
`

export const Form = styled.form`
	width: min(90%, 400px);
	display: flex;
	gap: 20px;
	flex-direction: column;
  margin-top: 24px;
`

export const InputContainer = styled.div`
	width: 100%;
	display: flex;
	flex-direction: column;
	gap: 11px;
`
export const Label = styled.label`
	font-weight: 500;
	font-size: 16px;
	letter-spacing: -0.408px;
	color: #000000;
`

export const Input = styled.input`
	width: 100%;
	height: 47px;
	background: #ffffff;
	border: 1px solid #ffffff;
	box-shadow: 0px 0px 5px rgba(0, 0, 0, 0.13);
	border-radius: 14px;
	outline: none;
	padding-left: 10px;
`

export const Send = styled.button`
	align-self: center;
	width: 97.78px;
	height: 35px;
	letter-spacing: -0.408px;
	font-weight: 500;
	font-size: 16px;
	color: #333333;
	background: #ffffff;
	border: 1px solid #476ade;
	box-shadow: 0px 0px 5px rgba(0, 0, 0, 0.13);
	border-radius: 14px;
	cursor: pointer;
`