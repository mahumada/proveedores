import React from 'react'
import { Form, Input, InputContainer, Label, Send, Wrapper } from './styles'
import Title from '../../components/common/Title'
import { useRouter } from 'next/router'
import { useForm } from 'react-hook-form'

const RecoverySection = () => {

  const { id } = useRouter().query;
  const { register, handleSubmit, watch, formState: { errors } } = useForm({mode:"all"});

  const onSubmit = async(data) => {
    const body = {
      ...data,
      id
    }
    const response = await fetch('/api/user/recover', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(body)
    }).then(response => response.json());
  } 

  return (
    <Wrapper>
      <Title title="Restablecer contraseña" blob={'AYUDA'} />
      <Form onSubmit={handleSubmit(onSubmit)}>
        <InputContainer>
          <Label>Nueva Contraseña</Label>
          <Input type="password" {...register('password')} />
        </InputContainer>
        <InputContainer>
          <Label>Repetir Contraseña</Label>
          <Input type="password" {...register('repeatPassword')}/>
        </InputContainer>
        <Send type="submit">Enviar</Send>
      </Form>
    </Wrapper>
  )
}

export default RecoverySection