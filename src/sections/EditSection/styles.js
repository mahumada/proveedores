import styled from "styled-components";

export const Wrapper = styled.div`
  width: 100%;
  display: flex;
  flex-direction: column;
`
export const Head = styled.div`
  display: flex;
  flex-direction: row;
  width: 80%;
  justify-content: center;
  margin: auto;
  margin-top: 100px;
`

export const Title = styled.p`
  font-weight: 500;
  font-size: 24px;
  line-height: 22px;
  margin-right: 20px;
  letter-spacing: -0.408px;
  color: #000000;
`

export const Save = styled.button`
  background: #FFFFFF;
  border: 1px solid #476ADE;
  box-sizing: border-box;
  border-radius: 8px;
  width: 150px;
  height: 30px;
  margin-right: 20px;
  cursor: pointer;
  @media (max-width: 1024px) {
  margin-top: 20px;
  margin-right: 0px;
  }
 
`
export const Delete = styled.button`
  background: none;
  border: none;
  font-weight: 500;
  font-size: 16px;
  line-height: 22px;
  letter-spacing: -0.408px;
  cursor: pointer;
  color: #DB2C2C;
  @media (max-width: 1024px) {
  margin-top: 20px;
 
  }
  
`