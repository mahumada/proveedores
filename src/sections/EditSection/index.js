import React, {useEffect, useState} from 'react';
import PostUpdate from '../../components/common/Posts/EditPost/index.js';
import { Wrapper, Head, Title, Delete, Save} from './styles.js';
import useFetch from '../../hooks/useFetch';
import { render } from 'react-dom';
import axios from 'axios';

const EditSection = ({id}) => {
  const [data, setData] = useState(null);
  const [loading, setLoading] = useState(true);
  // const { data, loading } = useFetch(`/api/post/${id}` , 'get');
  const categories = useFetch(`/api/category` , 'get');
  //

  useEffect(() => {
    
    async function getData(){
      if(id) {
        let datos = await axios.get(`/api/post/${id}`);
        setData(datos.data);
        setLoading(false);
      }
        return
      }
    getData()
  }, [id])
  
  return (
   <Wrapper>
     <Head>
       <Title>Editar Publicación</Title>
     </Head>
     {
       loading === false  ? 
       <PostUpdate props={data} id={id} categories={categories.data} />
       : null
     }
     
   </Wrapper>
  )
}

export default EditSection;
