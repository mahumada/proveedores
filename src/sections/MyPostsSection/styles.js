import styled, { keyframes } from 'styled-components'

export const Wrapper = styled.div`
	display: flex;
	flex-direction: column;
	width: 100%;
	height: 100%;
`
export const PostsContainer = styled.div`
	/* display: flex; */
	/* flex-wrap: wrap; */
	width: 100%;
	height: ${props => props.height};
	/* justify-content: space-between;
	align-items: center; */
	scroll-snap-type: y mandatory;
    overflow-y: auto;
		position: relative;

	@media (max-width: 1075px) {
		justify-content: center;
	}
`


export const TitleAndCategories = styled.div`
	display: flex;
	flex-direction: column;
	gap: 20px;
	width: 100%;
	height:100%;
`

export const TitleAndSection = styled.div`
	display: flex;
	flex-direction: column;
`

export const NotFound = styled.p`
	color: black;
	font-size: 24px;
	font-weight: 500;
	position: absolute;
	width: 100%;
	top: 50%;
	text-align: center;
	z-index: 0;
`

const slideIn = keyframes`
  0% { right: -400px }
  100% { right: 12px }
`

export const SearchNotification = styled.p`
  background: #476ade;
  width: 325px;
  height: 60px;
  border-radius: 14px;
  box-shadow: 0 0 4 rgba(0, 0, 0, .4);
  display: flex;
  align-items: center;
  justify-content: center;
  position: fixed;
  bottom: 12px;
  right: 12px;
  animation: ${slideIn} .6s ease-out;
  animation-iteration-count: 1;
  animation-fill-mode: forwards;
	font-size: 24px;
	color: #FFF;
	z-index: 999999999999999;
`