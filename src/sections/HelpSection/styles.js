import styled from 'styled-components'
import { IoMdArrowBack } from "react-icons/io";

export const Container = styled.div`
	width: min(95%, 1700px);
	margin: 0 auto;
	min-height: calc(100vh - 70px);
	display: flex;
	flex-flow: row wrap-reverse;
	justify-content: space-evenly;
	align-items: flex-start;
	padding-top: 70px;
	padding-bottom: 70px;
	position: relative;
`

export const Text = styled.p`
	margin: 0;
	font-size: 14px;
	color: #828282;
`

export const Row = styled.div`
	flex: 450px 1;
	display: flex;
	flex-direction: column;
	justify-content: flex-start;
	height: 810px;

	&:nth-child(1) {
		gap: 18px;
	}

	&:nth-child(2) {
		gap: 18px;
		align-items: center;
	}

	@media (max-width: 1050px){
		margin-top: 22px;
		height: min-content;
	}
`
export const Small = styled.small`
	color:red;
`
export const AccordionContainer = styled.div`
	display: flex;
	flex-direction: column;
	width: 100%;
	padding: 16px;
	background: #fff;
	box-shadow: 0px 0px 4px rgba(0, 0, 0, 0.2);
	border-radius: 13px;
	margin-bottom: 18px;

	&:last-child {
		margin-bottom: 45px;
	}
`

/*FORMULARIO DE CONTACTO&*/

export const TitleContact = styled.p`
	width: min(90%, 540px);
	text-align: center;
	font-weight: 500;
	font-size: 16px;
	letter-spacing: -0.408px;
	color: #000000;
`

export const Description = styled.p`
	width: min(90%, 540px);
	text-align: center;
	font-weight: 500;
	font-size: 14px;
	letter-spacing: -0.408px;
	color: #828282;
`

export const Form = styled.form`
	width: min(90%, 540px);
	height: 100%;
	display: flex;
	gap: 18px;
	flex-direction: column;
`

export const InputContainer = styled.div`
	width: 100%;
	display: flex;
	flex-direction: column;
	gap: 10px;
`
export const Label = styled.label`
	font-weight: 500;
	font-size: 14px;
	letter-spacing: -0.408px;
	color: #000000;
`

export const Input = styled.input`
	width: 100%;
	height: 42px;
	background: #ffffff;
	border: 1px solid #ffffff;
	box-shadow: 0px 0px 5px rgba(0, 0, 0, 0.13);
	border-radius: 13px;
	outline: none;
	padding-left: 10px;
`

export const Textarea = styled.textarea`
	width: 100%;
	min-height: 180px;
	background: #ffffff;
	border: 1px solid #ffffff;
	box-shadow: 0px 0px 5px rgba(0, 0, 0, 0.13);
	border-radius: 13px;
	outline: none;
	resize: none;
	overflow-y: auto;
	padding: 11px;
`

export const Send = styled.button`
	align-self: flex-end;
	width: 97.78px;
	height: 32px;
	letter-spacing: -0.408px;
	font-weight: 500;
	font-size: 14px;
	color: #333333;
	background: #ffffff;
	border: 1px solid #476ade;
	box-shadow: 0px 0px 5px rgba(0, 0, 0, 0.13);
	border-radius: 13px;
	cursor: pointer;
`
export const Back = styled(IoMdArrowBack)` 
	cursor: pointer;
	position: absolute;
	top: 110px;
	left: 45px;
	z-index: 2;
`

export const Sent = styled.div`
	display: flex;
	flex-direction: column;
	align-items: center;
	justify-content: space-evenly;
	width: min(450px, 90%);
	height: min(360px, 90%);
	border-radius: 11px;
	background-color: #FFF;
	box-shadow: 0 0 4px rgba(0, 0, 0, .2);
	position: absolute;
	z-index: 99999999;
	top: calc(50% - 225px);
	right: calc(50% - 225px);
`

export const SentImg = styled.img`
	width: 60%;
`

export const SentText = styled.p`
	margin: 0;
	font-size: 16px;
	font-weight: 500;
`