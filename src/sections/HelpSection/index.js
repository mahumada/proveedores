import {
	AccordionContainer,
	Container,
	Row,
	Text,
	TitleContact,
	Description,
	Form,
	InputContainer,
	Label,
	Input,
	Textarea,
	Send,
	Small,
	Back,
	Sent,
	SentImg,
	SentText
} from './styles'
import Title from '../../components/common/Title'
import Accordion from '../../components/common/Accordion'
import React, { useState, useEffect } from 'react'
import { useForm } from "react-hook-form";
import axios from "axios"
import { IoMdArrowBack } from "react-icons/io";

const LoginSection = () => {
	const [token, setToken] = useState()

	useEffect(() => {
		setToken(localStorage.getItem("IdToken"))
	}, [])

	const [accordionIndex, setAccordionIndex] = React.useState(-1)
	const accordionData = [
		{ title: 'Ingreso',
			logged: false,
		 	faqs: [
				{
					title: '¿Quedan registros de ventas en las bases de datos de Milux?',
					info: 'No, las publicaciones no quedan registradas como ventas y no se informan a ninguna entidad gubernamental y/o privada.',
					index: 0
				},
				{
					title: '¿Como me registro?',
					info: '¡Es simple! Haz click en el botón registrarse, completa los campos con tus datos y luego espera a la verificación de tu cuenta por parte de nuestro equipo y ya estarás habilitado para usar Milux.',
					index: 1
				},
				{
					title: '¿Hay que pagar comisión?',
					info: '¡No! Solo se cobra el monto de la suscripción mensual',
					index: 2
				},
				{
					title: 'Tengo un problema y no figura acá, ¿qué hago?',
					info: 'Contáctate con el soporte por mail a soporte@milux.com.ar indicando su usuario y problema o bien llena el formulario en nuestra página de ayuda.',
					index: 3
				},
				{
					title: '¿Como se si soy proveedor o comprador?',
					info: 'Si eres proveedor significa que usted vende algún insumo/producto, si eres vendedor significa que usted consume ese insumo/producto.',
					index: 4
				},
				{
					title: '¿Me olvide mi contraseña, que hago?',
					info: 'Cuando va a iniciar sesión, toque en el botón “olvidaste tu contraseña?”. Siga los pasos a continuación.',
					index: 5
				},
				{
					title: '¿Qué pasa cuando finaliza el mes de prueba gratis?',
					info: 'Cuando este mes finaliza, se le mandara la información para hacer el pago correspondiente de la suscripción, en caso de no hacerlo su cuenta quedara suspendida.',
					index: 6
				},
				{
					title: '¿Como se paga la suscripción que elijo?',
					info: 'El método de pago para las suscripciones es a través de transferencia bancaria a la cuenta proporcionada por nuestro equipo y luego, enviar el comprobante de pago indicando el usuario, mes correspondiente de la cuota al mail: tesoreria@milux.com.ar',
					index: 7
				},
			]
		},
		{ title: 'Suscripcion',
			logged: true,
		 	faqs: [
				{
					title: '¿Qué pasa si no pago un mes? ',
					info: 'Al no realizarse el pago arancelario por un mes, el servicio quedara suspendido hasta la regularización del saldo',
					index: 8
				},
				{
					title: '¿Porque figuro como usuario deshabilitado?',
					info: 'Esto puede deberse a dos motivos:\n• Por falta de pagos, hasta que no se regularice el saldo no se habilitara el uso de la web\n• Recién se ha registrado. Si recién se registro y pasaron menos de 24hs hábiles, por favor espere a que nuestro equipo corrobore los datos de registro ingresados y lo habiliten. Si han pasado más de 24hs hábiles desde su registro y todavía se encuentra deshabilitado, por favor contacte al soporte al mail soporte@milux.com.ar indicando su e-mail, numero de cliente y que se encuentra deshabilitado pasadas las 24hs hábiles de espera.',
					index: 9
				},
				{
					title: '¿Hay que pagar comisión?',
					info: '¡No! Solo se cobra el monto de la suscripción mensual',
					index: 10
				},
				{
					title: '¿Qué pasa cuando finaliza el mes de prueba gratis?',
					info: 'Cuando este mes finaliza, se le mandara la información para hacer el pago correspondiente de la suscripción, en caso de no hacerlo su cuenta quedara suspendida.',
					index: 11
				},
				{
					title: '¿Como me registro?',
					info: '¡Es simple! Haz click en el botón registrarse, completa los campos con tus datos y luego espera a la verificación de tu cuenta por parte de nuestro equipo y ya estarás habilitado para usar Milux.',
					index: 12
				},
			]
		},
		{ title: 'General',
			logged: true,
		 	faqs: [
				{
					title: '¿Es responsable Milux por las publicaciones de los usuarios?',
					info: 'No, Milux no se hace responsable por la veracidad de los datos ingresados por los usuarios en las publicaciones',
					index: 13
				},
				{
					title: '¿Quedan registros de ventas en las bases de datos de Milux?',
					info: 'No, las publicaciones no quedan registradas como ventas y no se informan a ninguna entidad gubernamental y/o privada.',
					index: 14
				},
				{
					title: 'Soy proveedor de una categoría que no figura disponible.',
					info: 'Si es proveedor de una categoría que no figura en el listado, por favor contacte al soporte al mail soporte@milux.com.ar indicando su e-mail, numero de cliente e indique que solicita el alta de una nueva categoría de la cual es proveedor.',
					index: 15
				},
				{
					title: '¿Como sugerir una categoría?',
					info: 'Para sugerir una categoría que aún no esté disponible, dirígete a la esquina superior derecha y haga clic sobre la foto de perfil, luego en ajustes y vera las categorías que son de su interés, abajo habrá un campo donde se puede sugerir una categoría que usted provee.',
					index: 16
				},
				{
					title: '¿Como se si soy proveedor o comprador?',
					info: 'Si eres proveedor significa que usted vende algún insumo/producto, si eres vendedor significa que usted consume ese insumo/producto.',
					index: 17
				},
				{
					title: '¿Me olvide mi contraseña, que hago?',
					info: 'Cuando va a iniciar sesión, toque en el botón “olvidaste tu contraseña?”. Siga los pasos a continuación.',
					index: 18
				},
				{
					title: '¿Como desactivar las notificaciones por mail?',
					info: 'Ve hacia la esquina superior derecha y toca en la foto de tu perfil, en el menú desplegable toca en mi perfil, y luego veras un botón para activar/desactivar las notificaciones por mail.',
					index: 19
				},
				{
					title: 'Tengo un problema y no figura acá, ¿que hago?',
					info: 'Contáctate con el soporte por mail a soporte@milux.com.ar indicando su usuario y problema o bien llena el formulario en nuestra página de ayuda.',
					index: 20
				},
				{
					title: '¿Como se paga la suscripción que elijo?',
					info: 'El método de pago para las suscripciones es a través de transferencia bancaria a la cuenta proporcionada por nuestro equipo y luego, enviar el comprobante de pago indicando el usuario, mes correspondiente de la cuota al mail: tesoreria@milux.com.ar',
					index: 21
				},
				{
					title: '¿Cuáles son los mails de Milux?',
					info: 'Los únicos mails verificados por parte de Milux son los siguientes:\nsoporte@milux.com.ar\ntesoreria@milux.com.ar\ninfo@milux.com.ar\nno-responder@milux.com.ar\nmarketing@milux.com.ar\nCualquier otro correo que no sean los mencionados anteriormente son una falsificación, por favor denunciarlos como spam.',
					index: 22
				},
			]
		},
	]

	const { register, handleSubmit, formState: { errors } } = useForm({ mode: "all" })

	async function onSubmit(data) {
		const response = await fetch('/api/sendEmail', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
				subject: `Ticket de ${data.name} (${data.email})`,
        template: `support`,
				email: "soporte@milux.com.ar",
        context: {
          message: data.message
        }
      })
    }).then(response => response.json());
		console.log(response)
		setSent(true);
    setTimeout(() => {
      setSent(false);
    }, 3000)
	}

	const [sent, setSent] = useState(false);

	return (
		<>
			{sent && <Sent>
        <SentImg src="/img/sent.png"/>
        <SentText>Mensaje enviado con exito!</SentText>
      </Sent>}
			<Back onClick={() => window.location = '/'}></Back>
			<Container>
				<Row>
					<Title title='¿Tenés alguna duda?' blob='AYUDA'/>
					<Text>Lee nuestras preguntas frecuentes.</Text>
					{accordionData.map((accordion, index) => {
						if(accordion.logged === (token === null)){ return }
						return (
							<div key={index}>
								<Title title={accordion.title} />
								<AccordionContainer>
									{accordion.faqs.map((faq, i) => {
										return (
											<div
												onClick={() => {
													accordionIndex === faq.index ? setAccordionIndex() : setAccordionIndex(faq.index)
												}}
												key={i}
												>
												<Accordion
													title={faq.title}
													info={faq.info}
													active={faq.index === accordionIndex}
													/>
											</div>
										)
									})}
								</AccordionContainer>
							</div>
						)
					})}
				</Row>
				<Row>
					<TitleContact>Contacta al soporte</TitleContact>
					<Description>
						Si tenes problemas con la plataforma no dudes en
						contactarnos. Luego de enviar tu mensaje, nos comunicaremos
						por mail con usted.
					</Description>
					<Form onSubmit={handleSubmit(onSubmit)} /*onSubmit={handleSubmit(onSubmitForm)}*/>
						<InputContainer>
							<Label htmlFor='name'>Nombre</Label>
							<Input name='name' type='text'
								{...register("name",
									{
										required: {
											value: true,
											message: "Ingresar un nombre es obligatorio"
										},
										minLength: {
											value: 2,
											message: "El nombre debe tener al menos 2 caracteres"
										},
										maxLength: {
											value: 60,
											message: "El nombre no puede superar los 60 caracteres"
										}
									})
								}
								id='name'
								style={errors.name ? { "border": " 1.5px solid red" } : null}
							></Input>
							<Small>
								{errors?.name?.message}
							</Small>
						</InputContainer>

						<InputContainer>
							<Label htmlFor='email'>Email</Label>
							<Input name="email" type='text' {...register("email", {
								required: {
									value: true,
									message: "Ingresar un email es obligatorio"
								},
								pattern: {
									value: /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i,
									message: "Ingrese un email valido"
								},

							}
							)} id='email'
								style={errors.email ? { "border": " 1.5px solid red" } : null}
							></Input>
							<Small>
								{errors?.email?.message}
							</Small>
						</InputContainer>

						<InputContainer>
							<Label htmlFor='msg'>Mensaje</Label>
							<Textarea name="msg"
								{...register("message",
									{
										required: {
											value: true,
											message: "Ingresar un mensaje es obligatorio"
										},
										maxLength: {
											value: 1000,
											message: "El mensaje no puede superar los 1000 caracteres"
										}
									}
								)} type='input'
								style={errors.msg ? { "border": " 1.5px solid red" } : null}
							></Textarea>
							<Small>
								{errors?.msg?.message}
							</Small>
						</InputContainer>
						<Send type='submit'>Enviar</Send>
					</Form>
				</Row>
			</Container></>

	)
}

export default LoginSection
