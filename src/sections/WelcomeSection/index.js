import WelcomeCategory from '../../components/common/WelcomeCategory'

const WelcomeSection = ({ welcome }) => {
	return <WelcomeCategory welcome={welcome}></WelcomeCategory>
}
export default WelcomeSection
