import React, { useState } from 'react'
import { Form, Input, InputContainer, Label, Send, Wrapper, Sent, SentImg, SentText } from './styles'
import Title from '../../components/common/Title'
import { useForm } from 'react-hook-form'

const SendRecoverySection = () => {

  const { register, handleSubmit, watch, formState: { errors } } = useForm({mode:"all"});
  const [sent, setSent] = useState(false);

  const onSubmit = async(data) => {
    const { id } = await fetch('/api/user/email', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({email: data.email})
    }).then(res => res.json()).catch(err => {throw err});

    const response = await fetch('/api/sendEmail', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        ...data,
        subject: 'Restablecer contraseña de Milux',
        template: `recover`,
        context: {
          link: `http://milux.com.ar/recovery?id=${id}`
        }
      })
    }).then(response => response.json());

    setSent(true);
		setTimeout(()=>{
			setSent(false);
		}, 3000);
  } 

  return (
    <Wrapper>
      {sent && <Sent>
        <SentImg src="/img/sent.png"/>
        <SentText>Link de recuperacion enviado</SentText>
      </Sent>}
      <Title title="Restablecer contraseña" blob={'AYUDA'} />
      <Form onSubmit={handleSubmit(onSubmit)}>
        <InputContainer>
          <Label>Email</Label>
          <Input {...register('email')} />
        </InputContainer>
        <Send type="submit">Enviar</Send>
      </Form>
    </Wrapper>
  )
}

export default SendRecoverySection