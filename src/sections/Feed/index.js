import { useEffect, useState } from 'react';
import axios from 'axios';
import useFetch from '../../hooks/useFetch';
import { io } from 'socket.io-client';

import {
  Wrapper,
  PostsContainer,
  TitleAndCategories,
  TitleAndSection,
	NotFound,
	SearchNotification
} from './styles';
import Title from '../../components/common/Title';
import CategorySelector from '../../components/Feed/CategorySelector';
import Post from '../../components/common/PostHome';
import { useCategoriesContext } from '../../context/categoriesContext';
import { user } from 'pg/lib/defaults';
import { useRouter } from 'next/router';

const Feed = ({ socket, orderBy, setCategory, offers }) => {
	const router = useRouter();
	const [isCategory, setIsCategory] = useState(false)
	const { handleCategory, category } = useCategoriesContext()

	const { data } = useFetch('/api/post/ontime', 'get')
	const [postsByCategory, setpostsByCategory] = useState([])
  const [post, setPost] = useState([]);

	useEffect(() => {
			const a = async() => {
				fetch(`/api/category/${router.query.category}`).then(res => res.json()).then(res => {
					res.data && handleCategory(res.data.name, res.data._id)
				});
			}
			if(router.query.category){
				a();
			}else{
				handleCategory('', '');
			}
	}, [router.query.category])
  useEffect(() => {
    setPost(data);
  }, [data]);
	// useEffect(() => {
	// 	fetch('/api/category/6239bdec771b43f8afeb1b92').then(res => res.json()).then(cat => setcategory({
	// 		...cat.data,
	// 		id: cat.data._id
	// 	}));
	// }, []);
  useEffect(() => {
    if (category && category.id !== null) {
      axios({
        method: 'post',
        url: '/api/post/categories',
        data: {
          categories: [category.id],
        },
      })
        .then((res) => res.json())
        .then((res) => {
					setpostsByCategory(res.data);
          setIsCategory(true);
        })
        .catch((error) => console.log(error));
    }
  }, [category]);
  useEffect(() => {
		switch(orderBy){
			case 'newest':
				setPost(post.sort((a, b) => {
					if(new Date(a.createdAt) > new Date(b.createdAt)){
						return 1;
					}else if(new Date(a.createdAt) < new Date(b.createdAt)){
						return -1;
					}else{
						return 0;
					}
				}));
				break;
			case 'olds':
				setPost(post.sort((a, b) => {
					if(new Date(a.createdAt) > new Date(b.createdAt)){
						return -1;
					}else if(new Date(a.createdAt) < new Date(b.createdAt)){
						return 1;
					}else{
						return 0;
					}
				}));
				break;
			default:
				break;
		}
  }, [orderBy]);


	const [sellers, setSellers] = useState([]);
	const [users, setUsers] = useState([]);
	useEffect(() => {
		const a = async() => {
			const s = await axios.get('/api/seller');
			const u = await axios.get('/api/user/');
			setSellers(s.data);
			setUsers(u.data);
		}
		a();
	}, [])

	const [searchNotification, setSearchNotification] = useState("");
	const [search, setNewSearch] = useState();

	useEffect(() => {
		if(search){	
			const amount = data.filter(post => (category.id && post.categories.indexOf(category.id) !== -1) || !category.id).filter(post => {if(post.variants.map(variant => variant.variantType.toLowerCase().includes(search.toLowerCase())).includes(true)){return true}else{return false}}).length;
			if(amount > 0){
				setSearchNotification(`${amount} resultados`);
			}
		}
	}, [search, category, data])

	useEffect(()=>{
		if(search){
			const timer = setTimeout(()=>{
				setSearchNotification("");
			}, 5000)
			return () => clearTimeout(timer);
		}
  }, [searchNotification])

	useEffect(() => {
		const newSearch = router.query.search;
		setNewSearch(newSearch);
	}, [router.query.search])

	const [dimensions, setDimensions] = useState({});

	useEffect(() => {
		setDimensions({
			height: window.innerHeight,
			width: window.innerWidth
		})
		setTimeout(() => {
			setDimensions({
				height: window.innerHeight,
				width: window.innerWidth
			})
		}, 1000)
	}, []);

	useEffect( () => { 
		document.querySelector("body").style.height = `${dimensions.height}px`; 
	}, [dimensions]);

  return (
		<Wrapper>
			<style jsx global>
  			{`
      		body {
          	overflow: hidden;
						height: 100vh;
       		}
   			`}
			</style>
      <TitleAndSection>
				{searchNotification.length > 0 && <SearchNotification>{searchNotification}</SearchNotification>}
        {/* <Title
					title='Publicaciones Recientes'
					blob={isCategory ? category.name : 'NUEVO'}
				/> */}
				<PostsContainer height={`${dimensions.height - 70}px`}>
				<NotFound>No hay resultados</NotFound>
					{function () {
						if (isCategory) {
							if (postsByCategory.length) {
								return postsByCategory.map((post, index) => (
									<Post
									setCategory={setCategory}
										key={index}
										socket={socket}
										datos={{
											id: post._id,
											/* Cotizacion*/
											pcd: post.quote.pdc,
											payment: post.quote.paymentMethod,
											
											observations1: post.quote.notes,
											/* Propuesta*/
											billing: post.proposal.minimum,
											validation: post.proposal.endDate,
											includesTransportation:
												post.proposal.includesTransport,
											paymentTerms: post.proposal.paymentTerms,
											deliveryDate:
												post.proposal.shippingInterval,
											/*paymentConditions: post.proposal.,*/
											observations2: post.proposal.notes,
											id: post._id,
											/*Contacto*/
											deliveryAdress:
												post.contact.sellingAddress,
											contact: post.contact.contactName,
											variants: post.variants,
										}}
										sellerId={post.seller_id}
										categories={post.categories}
										edit={false}
										seller={sellers.filter(seller => seller._id === post.seller_id)[0]}
										sellerData={users.filter(user => user._id === sellers.filter(seller => seller._id === post.seller_id)[0]?.user_id)[0]}
									></Post>
								))
							} else {
								return (<p>No hay publicaciones para esta categoria</p>)
							}
						} else {
							return data.filter(post => (category.id && post.categories.indexOf(category.id) !== -1) || !category.id).map((post, index) => {
								if(search && post.variants.map(variant => variant.variantType.toLowerCase().includes(search.toLowerCase())).indexOf(true) === -1){
									return;
								}
								if(post.accepted){
									return;
								}
								return <Post
									setCategory={setCategory}
									key={index}
									socket={socket}
									datos={{
										id: post._id,
										/* Cotizacion*/
										pcd: post.quote.pdc,
										payment: post.quote.paymentMethod,
                    observations1: post.quote.notes,
                    /* Propuesta*/
                    validationInit: post.createdAt,
                    billing: post.proposal.minimum,
                    validation: post.proposal.endDate,
                    includesTransportation: post.proposal.includesTransport,
											paymentTerms: post.proposal.paymentTerms,
											deliveryDate: post.proposal.shippingInterval,
                    /*paymentConditions: post.proposal.,*/
                    observations2: post.proposal.notes,
										/*Contacto*/
										deliveryAdress:
											post.contact.sellingAddress,
										contactName: post.contact.contactName,
										variants: post.variants,
										id: post._id,
										categories: post.categories
									}}
									sellerId={post.seller_id}
									categories={post.categories}
									seller={sellers.filter(seller => seller._id === post.seller_id)[0]}
									sellerData={users.filter(user => user._id === sellers.filter(seller => seller._id === post.seller_id)[0]?.user_id)[0]}
								></Post>
							})
						}
					}.call()}
				</PostsContainer>
			</TitleAndSection>
		</Wrapper>
	)
}

export default Feed;