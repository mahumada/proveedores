import Image from "next/image"
import styled from "styled-components"

export const Container = styled.div`
	width: 100%;
	height: 100%;
	display: flex;
	flex-direction: column;
	/*justify-content: center;*/
	align-items: center;
	padding: 63px 0;
	gap: 18px;
`

export const Title = styled.p`
	font-weight: 700;
	font-size: 20px;
	letter-spacing: -0.408px;
	color: #000000;

	@media (max-width: 350px) {
		font-size: 14px;
	}
`

export const TitleContainer = styled.div`
	display: flex;
	align-items: center;
	justify-content: center;
	gap: 11px;
	width: 90%;

	@media (max-width: 1050px){
		justify-content: flex-start;
		padding-left: 10%;
		gap: 0;
	}

	@media (max-width: 760px){
		justify-content: center;
		align-items: flex-start;
		padding: 0 0 45px 0;
	}
`

export const SwitchButton = styled.div`
	background: #DDD;
	border: none;
	padding: 0;
	border-radius: 90px;
	display: flex;
	align-items: center;
	position: absolute;
	right: 2.5%;
	width: 225px;
	overflow: hidden;
	@media (max-width: 760px){
		right: auto;
		margin-top: ${props => props.fixed ? "0" : "36px"};
	}
`

export const SwitchOptionBackground = styled.div`
	position: absolute;
	background: #476ade;
	width: 50%;
	height: 100%;
	left: ${props => props.mode === "received" ? 0 : props.mode === "sent" ? "50%" : "100%"};
	z-index: 0;
	border-radius: 90px;
	transition: .7s;
`

export const SwitchOption = styled.button`
	transition: .7s;
	font-size: 14px;
	color: ${props => props.selected ? "#FFF" : "#000"};
	font-weight: ${props => props.selected ? 600 : 400};
	background: none;
	width: 50%;
	height: 100%;
	border: none;
	padding: 5px 0;
	border-radius: 90px;
	z-index: 1;
	cursor: pointer;
`

export const ButtonIcon = styled.img`
	width: 16px;
	height: 16px;
`

export const Table = styled.div`
	width: 95%;
	height: calc(100vh - 200px);
	display: flex;
	flex-direction: column;
	font-size: 15px;
	@media screen and (max-width: 1050px) {
		font-size: 11px;
	}

	@media screen and (max-width: 625px) {
		display: none;
	}
`

export const Thead = styled.div`
	width: 100%;
	height: 54px;
	display: flex;
	font-size: 17px;
`

export const ColHead = styled.div`
	width: calc(100% / 8);
	height: 100%;
	display: flex;
	justify-content: center;
	align-items: center;
	gap: 9px;
	font-weight: 500;
	font-size: 1em;
	letter-spacing: -0.408px;
	color: #454545;
`

export const Arrow = styled.img`
	cursor: pointer;
`

export const Tbody = styled.div`
	width: 100%;
	height: 100%;
	display: flex;
	flex-direction: column;
	gap: 18px;
	overflow-y: auto;

	&::-webkit-scrollbar {
		-webkit-appearance: none;
		appearance: none;
	}

	&::-webkit-scrollbar:vertical {
		width: 3px;
	}

	&::-webkit-scrollbar-button:increment {
		display: none;
	}

	&::-webkit-scrollbar-button {
		display: none;
	}

	&::-webkit-scrollbar-thumb {
		background: #797979;
		border-radius: 18px;
	}
`

export const Row = styled.div`
	width: 100%;
	min-height: 68px;
	display: flex;
	background: #ffffff;
	border: 1px solid ${props => props.status == "rejected" ? "red" : props.status == "accepted" ? "green" : "none"};
	border-radius: 13px;
	position: relative;
`

export const Col = styled.div`
	width: calc(100% / 6);
	display: flex;
	flex-direction: column;
	justify-content: center;
	align-items: center;
	text-align: center;
	font-weight: 500;
	font-size: 1em;
`

export const MsgContainer = styled.div`
	width: 1.8em;
	height: 1.8em;
	display: flex;
	justify-content: center;
	align-items: center;
	/* background: #e8e8e8; */
	border-radius: 13px;
	cursor: pointer;
`
export const Msg = styled.img`
	width: 1.8em;
	height: 1.8em;
`

export const TableMobileContainer = styled.div`
	width: 100%;
	height: 100%;
	font-size: 14px;
	display: none;
	flex-direction: column;
	align-items: center;
	gap: 18px;
	overflow-y: scroll;

	@media screen and (max-width: 625px) {
		display: flex;
	}

	@media screen and (max-width: 365px) {
		font-size: 13px;
	}

	@media screen and (max-width: 260px) {
		font-size: 12px;
	}
`

export const TableMobile = styled.div`
	width: 90%;
	height: auto;
	font-size: 1em;
	background: #ffffff;
	border-radius: 13px;
	padding: 0px 13px 0px 13px;
	border: 1px solid ${props => props.status == "rejected" ? "red" : props.status == "accepted" ? "green" : "none"};
	position: relative;
`

export const RowMobile = styled.div`
	width: 100%;
	height: 45px;
	font-size: 1em;
	border: 1px solid ${props => props.status == "rejected" ? "red" : props.status == "accepted" ? "green" : "none"};
	display: flex;
	&:nth-child(6) {
		justify-content: flex-end;

		@media screen and (max-width: 260px) {
			justify-content: center;
		}
	}
`

export const ColMobile = styled.div`
	width: 50%;
	height: 100%;
	overflow: scroll;
	font-size: 0.9em;
	display: flex;
	justify-content: flex-start;
	align-items: center;
	white-space: nowrap;
`

export const Table2 = styled.div`
	width: 80%;
	display: flex;
	flex-direction: column;
	font-size: 15px;
	margin: 0 auto;
	margin-bottom: 18px;
`

export const ModalOpen = styled.div`
	position: absolute;
	width: 100%;
	height: 100%;
	background-color: rgba(220, 220, 220, 0.7);
	z-index: 8;
`

export const Head2 = styled.head`
	width: 100%;
	height: 54px;
	display: flex;
`

export const ColHead2 = styled.div`
	width: calc(100% / 3);
	height: 100%;
	display: flex;
	justify-content: center;
	align-items: center;
	gap: 9px;
	font-weight: 700;
	font-size: 0.95em;
	letter-spacing: -0.408px;
	color: #454545;
`

export const Tbody2 = styled.div`
	width: 100%;
	max-height: 360px;
	display: flex;
	flex-direction: column;
	gap: 18px;
	overflow-y: auto;

	&::-webkit-scrollbar {
		-webkit-appearance: none;
		appearance: none;
	}

	&::-webkit-scrollbar:vertical {
		width: 3px;
	}

	&::-webkit-scrollbar-button:increment {
		display: none;
	}

	&::-webkit-scrollbar-button {
		display: none;
	}

	&::-webkit-scrollbar-thumb {
		background: #797979;
		border-radius: 18px;
	}
`

export const Row2 = styled.div`
	width: 100%;
	height: 36px;
	display: flex;
	background: #ffffff;
	border-radius: 13px;
`

export const Col2 = styled.div`
	width: calc(100% / 3);
	display: flex;
	flex-direction: column;
	justify-content: center;
	align-items: center;
	text-align: center;
	font-weight: 400;
	font-size: 0.9em;
`

export const Dot = styled.div`
	background: ${props => props.mode === "received" ? "red" : props.mode === "sent" ? "#fc9403" : "#FFF"};
	width: 9px;
	height: 9px;
	position: absolute;
	top: 7.5px;
	right: 7.5px;
	z-index: 99;
	border-radius: 9px;
`

export const DateBlobContainer = styled.div`
	display: flex;
	justify-content: center;
	gap: 5.5px;
`

export const DateBlob = styled.p`
	font-size: 13px;
	font-weight: 600;
	background: #DDD;
	padding: 3px 14px;
	border-radius: 22px;
	cursor: pointer;
`

export const DateArrow = styled.img`
	width: 9px;
	height: 8px;
	margin-left: 6px;
`

export const SettingsModalContainer = styled.div`
	width: 100%;
	left: 0;
	top: 0;
	display: flex;
	flex-direction: column;
	gap: 20px;
	align-items: center;
	justify-content: center;
`

export const SwitchModalContainer = styled.div`
	position: relative;
	width: 235px;
	height: 28px;
`

export const AcceptButton = styled.button`
	font-size: 16px;
	font-weight: 700;
	color: white;
	background: #476ade;
	border: none;
	border-radius: 16px;
	padding: 4px 24px;
	cursor: pointer;
`

export const SettingsTitle = styled.p`
	font-size: 20px;
	font-weight: 500;
	color: black;
	margin: 0;
`

export const RespondButton = styled.button`
	width: 24px;
	aspect-ratio: 1;
	border: none;
	background: ${props => props.accept ? "lightgreen" : props.reject ? "red" : "grey"};
	border-radius: 6px;
	cursor: pointer;
`