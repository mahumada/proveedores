import axios from "axios"
import { useRouter } from "next/router"
import React, { useEffect, useState } from "react"
import Layout from "../../components/common/Layout"
import Modal from "../../components/common/Modal"
import Switch from "../../components/common/switchSeller"

import {
	Container,
	Table,
	Thead,
	ColHead,
	Tbody,
	Row,
	Col,
	Msg,
	MsgContainer,
	TableMobile,
	ColMobile,
	RowMobile,
	TableMobileContainer,
	Title,
	Table2,
	Head2,
	ColHead2,
	Tbody2,
	Row2,
	Col2,
	ModalOpen,
	Dot,
	SwitchButton,
	TitleContainer,
	ButtonIcon,
	SwitchOption,
	SwitchOptionBackground,
	DateBlob,
	DateBlobContainer,
	DateArrow,
	SwitchModalContainer,
	SettingsModalContainer,
	AcceptButton,
	SettingsTitle,
	RespondButton,

} from "./styles"

const OffersSection = ({ setInteracted, socket }) => {
	const [offers, setOffers] = useState(undefined)
	const [users, setUsers] = useState([])
	const [selected, setSelected] = useState(null)
	const [offer, setOffer] = useState(null)
	const [receivedOffers, setReceivedOffers] = useState([])
	const [modal, setModal] = useState(false)
	const [ownOffers, setOwnOffers] = useState([]);

	useEffect(() => {
		const a = async() => {
			const myoffers = await fetch(`/api/offer/own/${JSON.parse(localStorage.getItem('loggedUser')).id}`).then(res => res.json()).catch(err => console.error(err));
			setOwnOffers(myoffers);
		}
		a();
	}, [])

	const openModal = row => {
		setOffer(row)
		setModal(true)
	}

	const closeModal = () => {
		setModal(false)
	}

	let router = useRouter()
	/*onClick={() => router.push('/post/abc')}*/

	useEffect(() => {
		if (JSON.parse(localStorage.getItem("loggedUser")) === null) {
			window.location = "/login"
		}
	}, [])

	useEffect(()=>{
		const a = async() => {
			const user = await (await axios.get(`/api/user/${JSON.parse(localStorage.getItem('loggedUser')).id}`)).data.data;
			if(user.blocked){
				window.location = '/';
			}
		}
		a()
	}, [])

	useEffect(() => {
		async function a() {
			try {
				const seller = await axios.get(
					`/api/user/getseller?_id=${JSON.parse(localStorage.getItem("loggedUser")).id}`
				)
				const newOffers = await axios.get(`/api/offer/own?id=${seller.data._id}`);
				setReceivedOffers(newOffers.data);
				const newUsers = await axios.get(`/api/user`);
				setUsers(newUsers.data);
			} catch (error) {
				console.log(error)
			}
		}
		a()
	}, [])

	const [mode, setMode] = useState('received');
	const changeMode = () => {
		if(mode === 'received'){
			setMode('sent');
			setOffers(ownOffers);
		}else if(mode === 'sent'){
			setMode('received');
			setOffers(receivedOffers);
		}
		
	}

	const switchMode = () => {
		if(mode === 'received'){
			setOffers(receivedOffers);
		}else if(mode === 'sent'){
			setOffers(ownOffers);
		}
	}

	useEffect(() => {
		switchMode();
	}, [ownOffers, receivedOffers])

	const sendMessage = async(offer) => {
		console.log(offer)
		let receiver;
		try {
			receiver = await fetch(`/api/user/${offer.buyer_id}`).then(res => res.json());
		}catch(err){
			console.log(err);
			return;
		}
		
		socket.emit('private message', {
      receiver: { id: offer.buyer_id, name: receiver.data.username },
      text: 'Hola',
    });
    window.location = '/chat';
	}

	const meses = ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"];
	const [collapsedDates, setCollapsedDates] = useState([]);
	const handleDateCollapse = dateString => {
		if(collapsedDates.includes(dateString)){
			setCollapsedDates(collapsedDates.filter(date => date !== dateString));
		}else{
			setCollapsedDates([...collapsedDates, dateString]);
		}
	}

	const respondCounterOffer = async(settingsModalMode, offer) => {
		try{
			const response = await fetch('/api/offer/respond', {
				method: 'POST',
				headers: {
					'Content-Type': 'application/json'
				},
				body: JSON.stringify({
					id: offer._id,
					status: settingsModalMode ? "rejected" : "accepted"
				})
			}).then(res => res.json());
			if(Object.keys(response).includes("error")){
				return;
			}
		}catch(err){
			return console.log(err);
		}
		try {
			await fetch('/api/sendEmail', {
				method: 'POST',
				headers: {
					'Content-Type': 'application/json'
				},
				body: JSON.stringify({
					email: users?.filter(u => u._id == offer.buyer_id)[0].email,
					subject: `${settingsModalMode ? "Rechazaron" : "Aceptaron"} tu contraoferta en milux`,
					template: `accept`,
					context: {
						products: []
					}
				})
			});
		}catch(err){
			console.log(err);
		}
		window.location.reload();
	}

	return (
		<>
		{(modal) && <ModalOpen></ModalOpen>}
			<Container>
				{modal && <Modal>
					<Table2>
						<Head2>
							<ColHead2>Tipo</ColHead2>
							<ColHead2>Precio</ColHead2>
							<ColHead2>Cantidad</ColHead2>
						</Head2>
						<Tbody2>
							{offer?.variants.map((variant, index) => (
								<Row2 key={index}>
									<Col2>{variant.variantType}</Col2>
									<Col2>{variant.price}</Col2>
									<Col2>{variant.quantity}</Col2>
								</Row2>
							))}
						</Tbody2>
					</Table2>
					<p onClick={() => closeModal()} style={{ position: 'absolute', top: '0', right: '10px', fontSize: '30px', cursor: 'pointer', color: '#000000' }}>x</p>
				</Modal>}

				<TitleContainer>
					<Title>Actividad de las publicaciones</Title>
					<SwitchButton onClick={() => { changeMode() }}>
						<SwitchOptionBackground mode={mode} />
						<SwitchOption selected={mode === 'received'}>Recibidas</SwitchOption>
						<SwitchOption selected={mode === 'sent'}>Enviadas</SwitchOption>
					</SwitchButton>
				</TitleContainer>
				{offers ? (
					<>
						<Table>
							<Thead>
								{["Tipo", "Producto", "Cantidad", "Precio", "Usuario", "Medio", "Terminos", "Opciones"].map((col, index) => (
									<ColHead key={index}>
										{col}
									</ColHead>
								))}
							</Thead>
							<Tbody>
								{offers &&
									offers.map((row, index) => {
										const date = new Date(row.createdAt);
										const dateString = `${date.getUTCDate()} de ${meses[date.getMonth()]} de ${date.getFullYear()}`;
										let isFirstFromDate = false;
										if(index === 0){
											isFirstFromDate = true;
										}else{
											const previousDate = new Date(offers[index - 1].createdAt);
											isFirstFromDate = date.getUTCDate() !== previousDate.getUTCDate() || date.getMonth() !== previousDate.getMonth() || date.getFullYear() !== previousDate.getFullYear();
										}
										let precios = []
										let modelos = []
										let cantidades = []
										row.variants.map((v, i) => {
											if (i !== row.variants.length - 1) {
												precios.push(`${v.price}`)
												cantidades.push(v.quantity)
												return modelos.push(v.variantType)
											} else {
												precios.push(`${v.price}`)
												cantidades.push(v.quantity)
												return modelos.push(v.variantType)
											}
										})
										let username

										if (users) {
											username =
												users.filter(user => user && user._id === row.buyer_id).length &&
												users.filter(user => user && user._id === row.buyer_id)[0].username
										}

										return <React.Fragment key={index}>
											{isFirstFromDate && <DateBlobContainer onClick={() => handleDateCollapse(dateString)}>
												<DateBlob>{dateString}<DateArrow src={collapsedDates.includes(dateString) ? '/img/arrowUp.png' : '/img/arrowDown.png'} /></DateBlob>
											</DateBlobContainer>}
											{!collapsedDates.includes(dateString) && <Row status={row.status} key={index}>
												{!row.seen && <Dot mode={mode} />}
												<Col>{row.type}</Col>
												<Col>
													{modelos.length > 1 ? (
														''

													) : (
														modelos
													)}
												</Col>
												<Col>
													{cantidades.length > 1 ? (
														<p style={{ fontWeight: '700' }}>Multiples productos</p>

													) : (
														cantidades
													)}
												</Col>
												<Col>
													{precios.length > 1 ? (
														''
													) : (
														precios
													)}
												</Col>
												<Col>{username}</Col>
												{/* <Col>{row.createdAt.slice(0, 10)}</Col> */}
												<Col>{row.paymentMethod || "-"}</Col>
												<Col>{row.paymentTerms || "-"}</Col>
												<div style={{ display: 'flex', alignItems: 'center', justifyContent: 'center', gap: '8px', width: '16.5%' }}>
													{row.variants.length > 1 && <MsgContainer onClick={() => openModal(row)}>
														<Msg src="/img/formatlist2.svg"></Msg>
													</MsgContainer>}
													<MsgContainer onClick={() => router.push(`/offer/${row._id}`)}>
														<Msg src="/img/preview.svg"></Msg>
													</MsgContainer>
													{mode === "received" && <MsgContainer onClick={() => sendMessage(row)}>
														<Msg src="/img/chat.svg"></Msg>
													</MsgContainer>}
													{row.type === "Contraoferta" && mode === "received" && <>
														<MsgContainer>
															<RespondButton accept title="Aceptar contraoferta" onClick={() => respondCounterOffer(false, row)} />
														</MsgContainer>
														<MsgContainer>
															<RespondButton reject title="Aceptar contraoferta" onClick={() => respondCounterOffer(true, row)}/>
														</MsgContainer>
													</>}
												</div>
											</Row>}
										</React.Fragment>
									})}
							</Tbody>
						</Table>

						<TableMobileContainer>
							{offers &&
								offers.map((row, index) => {
									const date = new Date(row.createdAt);
									const dateString = `${date.getUTCDate()} de ${meses[date.getMonth()]} de ${date.getFullYear()}`;
									let isFirstFromDate = false;
									if(index === 0){
										isFirstFromDate = true;
									}else{
										const previousDate = new Date(offers[index - 1].createdAt);
										isFirstFromDate = date.getUTCDate() !== previousDate.getUTCDate() || date.getMonth() !== previousDate.getMonth() || date.getFullYear() !== previousDate.getFullYear();
									}
									let precios = []
									let modelos = []
									let cantidades = []
									row.variants.map((v, i) => {
										if (i !== row.variants.length - 1) {
											precios.push(`${v.price}`)
											cantidades.push(v.quantity)
											return modelos.push(v.variantType)
										} else {
											precios.push(`${v.price}`)
											cantidades.push(v.quantity)
											return modelos.push(v.variantType)
										}
									})
									let username
									if (users) {
										username =
											users.filter(user => user && user._id === row.buyer_id).length &&
											users.filter(user => user && user._id === row.buyer_id)[0].username
									}

									return <React.Fragment key={index}>
										{isFirstFromDate && <DateBlobContainer onClick={() => handleDateCollapse(dateString)}>
												<DateBlob>{dateString}<DateArrow width="16px" height="16px" src={collapsedDates.includes(dateString) ? '/img/arrowUp.png' : '/img/arrowDown.png'} /></DateBlob>
										</DateBlobContainer>}
										{!collapsedDates.includes(dateString) && <TableMobile key={index} status={row.status}>
												{!row.seen && <Dot />}

											<RowMobile>
												<ColMobile>Tipo</ColMobile>
												<ColMobile>{row.type}</ColMobile>
											</RowMobile>
											<RowMobile>
												<ColMobile>Producto</ColMobile>
												<ColMobile>{modelos.length > 1 ? '' : modelos}</ColMobile>
											</RowMobile>
											<RowMobile>
												<ColMobile>Unidades</ColMobile>
												<ColMobile>{cantidades.length > 1 ? (
													<p style={{ fontWeight: '700' }}>Multiples productos</p>

												) : (
													cantidades
												)}</ColMobile>
											</RowMobile>
											<RowMobile>
												<ColMobile>Precios</ColMobile>
												<ColMobile>{precios.length > 1 ? '' : precios}</ColMobile>
											</RowMobile>
											<RowMobile>
												<ColMobile>Comprador</ColMobile>
												<ColMobile>{username}</ColMobile>
											</RowMobile>
											<RowMobile>
												<ColMobile>Medio</ColMobile>
												<ColMobile>{row.paymentMethod || "-"}</ColMobile>
											</RowMobile>
											<RowMobile>
												<ColMobile>Terminos</ColMobile>
												<ColMobile>{row.paymentTerms || "-"}</ColMobile>
											</RowMobile>
											<RowMobile>
												<div style={{ display: 'flex', alignItems: 'center', justifyContent: 'flex-end', gap: '8px', width: '100%' }}>
													{row.type == "Contraoferta" && mode === "received" && <MsgContainer>
														<Msg src="/img/settings.svg" onClick={() => openSettings(row)} />
													</MsgContainer>}
													{row.variants.length > 1 && <MsgContainer onClick={() => openModal(row)}>
														<Msg src="/img/formatlist2.svg"></Msg>
													</MsgContainer>}
													<MsgContainer onClick={() => router.push(`/offer/${row._id}`)}>
														<Msg src="/img/preview.svg"></Msg>
													</MsgContainer>
													{mode === "received" && <MsgContainer onClick={() => sendMessage(row)}>
														<Msg src="/img/chat.svg"></Msg>
													</MsgContainer>}
													{row.type === "Contraoferta" && mode === "received" && <>
														<MsgContainer>
															<RespondButton accept title="Aceptar contraoferta" onClick={() => respondCounterOffer(false, row)} />
														</MsgContainer>
														<MsgContainer>
															<RespondButton reject title="Aceptar contraoferta" onClick={() => respondCounterOffer(true, row)}/>
														</MsgContainer>
													</>}
												</div>
											</RowMobile>
										</TableMobile>}
									</React.Fragment>
								})}
						</TableMobileContainer>
					</>
				) : (
					<p>No tienes interacciones en este momento</p>
				)}
			</Container>
		</>
	)
}

export default OffersSection
