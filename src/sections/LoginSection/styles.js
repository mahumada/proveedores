import styled from 'styled-components'

export const Container = styled.div`
	width: 100vw;
	height: 100vh;
	overflow: hidden;
	display: flex;
	align-items: center;
	background: #fff;
`

export const FormContainer = styled.div`
	display: flex;
	flex-direction: column;
	align-items: center;
	justify-content: flex-start;
	max-height: 90%;
	overflow-y: scroll;
	width: min(500px, 100vw);
	background: #fff;
	z-index: 99;
	@media(max-width: 900px){
		width: 100vw;
	}
	@media(min-width: 2000px){
		width: 700px;
	}
	@media(min-width: 3000px){
		width: 33vw;
	}
`

export const Slides = styled.div`
	width: calc(100vw - 500px);
	height: 100%;
	background: #476ade;
	overflow: hidden;
	display: flex;
	align-items: center;
	justify-content: center;
	position: relative;
	
`

export const LogoContainer = styled.div`
	display: flex;
	gap: 20px;
	align-items: center;
	position: absolute;
	top: 0;
	left: 0;
	margin: 20px 0 0 20px;
`

export const Square = styled.div`
	width: 37px;
	height: 37px;
	background: #ffffff;
	border-radius: 8px;
`

export const Text = styled.p`
	font-weight: 700;
	font-size: 20px;
	color: #ffffff;
`

export const SlideButtons = styled.div`
	display: flex;
	width: 100px;
	justify-content: space-between;
	align-items: center;
	position: absolute;
	bottom: 10%;
`

export const SlideBtn = styled.button`
	background: #000;
	width: 24px;
	height: 24px;
	border: none;
	border-radius: 12px;
`

export const MiluxPreview = styled.div`
	width: 100%;
	height: 100%;
	display: flex;
	align-items: center;
	justify-content: center;
	position: relative;
`

export const Image = styled.img`
	width: 100%;
	height: 100%;
	top: 0;
	right: 0;
	position: absolute;
	z-index: 1;
`

export const Logo = styled.img`
	width: min(95%, 400px);
	z-index: 2;
	@media(max-width: 900px){
		display: none;
	}
	@media(min-width: 2000px){
		width: 500px;
	}
`