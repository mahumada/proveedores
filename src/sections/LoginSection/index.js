import Form from '../../components/Login/Form'
import Info from '../../components/Login/Info'
import Switch from '../../components/Login/Switch'
import {
	Container,
	FormContainer,
	Slides,
	SlideButtons,
	SlideBtn,
	LogoContainer,
	Square,
	Text,
	Image,
	MiluxPreview,
	Logo,
} from './styles'
import React from 'react'

const LoginSection = () => {
	return (
		<Container>
			<Slides>
				<MiluxPreview>
					<Logo src="/img/loginPreviewLogo.png" />
					<Image src="/img/loginPreview.png" />
				</MiluxPreview>
			</Slides>
			<FormContainer>
				<Info title='Bienvenido' subtitle='Nos alegra verte de nuevo' />
				<Form mode='login' />
				<Switch mode='login' />
			</FormContainer>
		</Container>
	)
}

export default LoginSection
