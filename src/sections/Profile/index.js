import React, { useState, useEffect } from "react";
import Link from "next/link";
import {
  Wrapper,
  ProfilePic,
  Name,
  Info,
  InfoText,
  Hr,
  PicContainer,
  Hover,
  InputContainer,
  ButtonSave,
	DescriptionText,
  InputDescription,
  MyPostsContainer,
  MyPostsLabel,
  MyPostsButton
} from "./styles";

import Layout from "../../components/common/Layout";
import axios from "axios";


export const ProfileSection = ({ edit }) => {
  const [seller, setSeller] = useState(null);
  const [userId, setUser] = useState("");
  const [body, setBody] = useState(undefined);
  const [modal, setModal] = useState(false);
  const [usuario, setUsuario] = useState({});
  
  let date= '';
  let fecha = new Date();
  if(seller?.createdAt){
    date = new Date(seller.createdAt)
    fecha =Math.floor( Math.floor((fecha - date) / (1000*60*60*24))/ 31);
  }

  let mes = fecha === 1 ? 'mes' : 'meses';
     
  

  useEffect(() => {
    if (JSON.parse(localStorage.getItem("loggedUser")) === null) {
      window.location = "/login";
    }
  }, []);

  const [interactions, setInteractions] = useState('Sin ');

  useEffect(() => {
    const getSellerById = async () => {
      try {
        const user = localStorage.getItem("loggedUser");
        const userparsed = JSON.parse(user);
        setUser(userparsed.id);
        const response = await axios.get(
          `/api/user/getseller?_id=${userparsed.id}`
        );
        const newOffers = await axios.get(`/api/offer/own?id=${response.data._id}`);
				setInteractions(newOffers.data.length);
        setSeller(response.data);
      } catch (error) {
        console.log(error);
      }
    };

    getSellerById();
  }, []);

  // useEffect(() => {
  //   const getPostBySeller = async () => {
  //     try {
  //       const response = await axios.get(
  //         `/api/user/posts?seller_id=${seller._id}`
  //       );
  //       setData(response.data);
  //        date= new Date(seller.createdAt)
  //     } catch (error) {
  //       console.log(error);
  //     }
  //   };

  //   getPostBySeller();
  // }, [seller]);
  const [ownPosts, setOwnPosts] = useState("")
  useEffect(() => {
    const getUserInfo = async () => {
      try {
        if (!userId) return;
        const response = await axios.get(
          `/api/user/${userId}`
        );
        setUsuario(response.data.data);
      } catch (error) {
        console.log(error);
      }
      if(userId){
        try {
          const posts = await fetch(`/api/post/from/${userId}`).then(res => res.json()).catch(err => console.log(err));
          setOwnPosts(posts.data.length.toString());
        }catch(err){
          console.log(err)
        }
      }
    };

    getUserInfo();
  }, [userId]);
  async function uploadImage(files) {
    const formData = new FormData();
    formData.append("file", files[0]);
    formData.append("upload_preset", "azihkt5g");
    axios
      .post(
        "https://api.cloudinary.com/v1_1/proveedores/image/upload",
        formData
      )
      .then(async (response) => {
        setBody(response.data.secure_url);
      });
  }

  async function update() {
    if(body !== undefined){
      let tomas = await axios.put(`/api/user?id=${userId}`, {
        image: body,
      });
      // window.location.reload();
    }
  }

  return (
    
      <Layout>
        <Wrapper>
            <Hr />
          <PicContainer style={{ marginBottom: "30px" }}>
            <ProfilePic src={usuario?.image ? usuario.image : ""} />
            <Hover onClick={() => setModal(!modal)}> </Hover>
          </PicContainer>
          {modal && (
            <>
              <InputContainer
                onChange={(e) => uploadImage(e.target.files)}
                type={"file"}
              ></InputContainer>
              <ButtonSave onClick={() => update()}>Actualizar</ButtonSave>
            </>
          )}
          <Name style={{ marginBottom: "20px" }}>
            {seller?.businessName ? seller.businessName : usuario.username}
          </Name>
					{seller !== null ? 
          <InputDescription>
					<DescriptionText>
            {!date ? '' : 'Se unió el ' + date.getDay() + '/' + date.getMonth() + '/' + date.getFullYear()}
					</DescriptionText>
          <DescriptionText><img style={{marginRight : '5px'}}  src={'/img/location.png'}/>{seller.province ?? ''}, {seller.city ?? ''}</DescriptionText>
          </InputDescription>
					:
					<>
          <DescriptionText>Se unió el: {usuario?.createdAt?.slice(0, 10)} </DescriptionText>
          
          </>	
				}
          {seller !== null ? (
            <Info>
              <InfoText>{ownPosts} Publicaciones</InfoText>
              <InfoText>{fecha ? `${fecha} ${mes} de antigüedad` : ''}</InfoText>
							<InfoText>{interactions} Interacciones</InfoText>
            </Info>
          ) : (
            <Info>
              <InfoText> {usuario?.email ? usuario.email : ""}</InfoText>
              <InfoText style={{color: '#476ADE'}}><Link href='/signup'>Hazte Vendedor</Link></InfoText>
              <InfoText>
                Telefono: {usuario?.phone ? usuario.phone : ""}
              </InfoText>
            </Info>
          )}
          <MyPostsContainer>
            {/* <MyPostsLabel>Ir a</MyPostsLabel> */}
            <Link href="/publicaciones">
              <MyPostsButton>Mis Publicaciones</MyPostsButton>
            </Link>
          </MyPostsContainer>
        </Wrapper>
      </Layout>
    
  );
};
