import { useEffect } from 'react';
import Layout from '../../components/common/Layout'
import CreatePost from '../../components/common/Posts/CreatePost';
import axios from 'axios';

import useFetch from '../../hooks/useFetch';

import { Container } from './style'

const ToOfferSection = (props) => {
	const { data, loading } = useFetch(`/api/category` , 'get');
	
	useEffect(()=>{
		let userId = JSON.parse(localStorage.getItem('loggedUser'));	
		if(!userId){
			window.location = '/login';
		}
	},[])

	useEffect(()=>{
		const a = async() => {
			const user = await (await axios.get(`/api/user/${JSON.parse(localStorage.getItem('loggedUser')).id}`)).data.data;
			if(user.blocked){
				window.location = '/';
			}
		}
		a()
	}, [])

	return (
		<Layout heightContainer={'auto'}>
			<Container>
				{loading === false? 
					<CreatePost categories={data}></CreatePost>	
				: null
			}
			</Container>
		</Layout>
	)
}


export default ToOfferSection
