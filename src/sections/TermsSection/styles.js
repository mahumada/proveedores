import styled from "styled-components";

export const Wrapper = styled.div`
  width: 100%;
  height: calc(100vh - 77px);
  display: flex;
  flex-direction: column;
  align-items: center;
`

export const Text = styled.p`
  font-size: 16px;
  max-width: 90%;
  text-align: center;
`

export const Title = styled.h1`
  font-size: 24px;
  font-weight: 700;
`

export const MobileDiv = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  height: 100%;
  width: 100%;
`

export const Button = styled.p`
  font-size: 20px;
  font-weight: 600;
  color: white;
  width: 90%;
  padding: 12px 0;
  margin-top: 12px;
  border-radius: 48px;
  text-align: center;
  background: #476ade;
  cursor: pointer;
`