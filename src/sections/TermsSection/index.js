import React from 'react';
import Layout from '../../components/common/Layout';
import { Button, MobileDiv, Text, Title, Wrapper } from './styles';
import {BrowserView, MobileView} from 'react-device-detect';
import Link from 'next/link';

const TermsSection = () => {
  return (
    <Layout>
      <Wrapper>
        <BrowserView style={{width: '100%', height: '100%'}}>
          <object data="/terms.pdf" type="application/pdf" width="100%" height="100%">
            <embed src="https://drive.google.com/file/d/1vWX53f7J2F7p3Iq0xzeSA0n02ogJkF_c/preview" width="100%" height="100%" />
          </object>
        </BrowserView>
        <MobileView style={{width: '100%', height: '100%'}}>
          <MobileDiv>
            <Title>Terminos y Condiciones</Title>
            <Text>Haz click en el siguiente boton para leer los terminos y condiciones de Milux</Text>
            <Link href="/terms.pdf"><Button>DESCARGAR</Button></Link>
          </MobileDiv>
        </MobileView>
      </Wrapper>
    </Layout>
  )
}

export default TermsSection