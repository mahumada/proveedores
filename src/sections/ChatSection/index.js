import { useState, useEffect, useRef, useContext } from 'react'
import { io } from "socket.io-client";
import AuthContext from '../../context/AuthContext'
import axios from 'axios';
import Modal from '../../components/common/Modal'

import { IoMdArrowBack, IoImagesOutline } from "react-icons/io";

import dynamic from 'next/dynamic'

const Picker = dynamic(import('emoji-picker-react'), { ssr: false })

import {
	Container,
	Chats,
	Chat,
	Header,
	Avatar,
	Name,
	NameAndOnline,
	Online,
	SendContainer,
	Sticker,
	Input,
	File,
	Conversation,
	General,
	Archivados,
	Back,
	GeneralOrArchivados,
	Box,
	CompraTodo,
	ModalOpen,
	InputFile,
	LabelFile,
	Image,
	Confirm
} from './styles'

import ChatBox from './components/ChatBox'

import Message from './components/Message'
import { set } from 'react-hook-form';

const ChatSection = ({ socket }) => {
	const [selected, setSelected] = useState(null)
	const [dimensions, setDimensions] = useState({})
	const [emoji, setEmoji] = useState(false)
	const [msg, setMsg] = useState('')
	const [mensajes, setMensajes] = useState([]);
	const [chats, setChats] = useState([])
	const [sellerData, setSellerData] = useState(undefined);
	const [file, setFile] = useState(false)
	const [body, setBody] = useState(null)
	const [users, setUsers] = useState({});

	const [modal, setModal] = useState(false)
	const [body2, setBody2] = useState(null)

	// useEffect(() => {
	// 	const hisId = data?.members[0]?.id === userId ? data?.members[1]?.id : data?.members[0]?.id;
	// 	const a = async () => {
	// 		const response = await axios.get(`/api/user/${hisId}`);
	// 		setImg(response.data.data?.image);
	// 	}
	// 	a();
	// }, [])

	useEffect(() => {
		const a = async() => {
			const newUsers = await axios.get('/api/user');
			setUsers(newUsers.data);
		}
		a();
	}, [])

	const userImg = (chat) => {
		if(users?.length){
			const hisId = chat?.members[0]?.id === JSON.parse(localStorage.getItem('loggedUser')).id ? chat?.members[1]?.id : chat?.members[0]?.id;
			return users.filter(u => u._id === hisId)[0].image;
		}
	}

	const handleEmoji = () => {
		setEmoji(!emoji)
	}

	useEffect(()=>{
		const a = async() => {
			const user = await (await axios.get(`/api/user/${JSON.parse(localStorage.getItem('loggedUser')).id}`)).data.data;
			if(user.blocked){
				window.location = '/';
			}
		}
		a()
	}, [])

	const handleChatSelect = async (index) => {
		if (index !== undefined) {
			setMensajes(chats[index]?.messages)
			const newSellerData = await axios.get(`/api/user/${chats[index].members[0].id === userId.id ? chats[index].members[1].id : chats[index].members[0].id}`)
			setSellerData(newSellerData);
			if(chats[index].messages[0].sender !== userId.id){
				socket.emit("read", { chatId: chats[index]._id })
			}
		}
	}

	const [chosenEmoji, setChosenEmoji] = useState(null)
	const refInput = useRef(null)
	const [userId, setUserId] = useState(undefined);

	useEffect(() => {
		setUserId(JSON.parse(localStorage.getItem('loggedUser')));
	}, [])

	const onEmojiClick = (event, emojiObject) => {
		setChosenEmoji(emojiObject)
		refInput.current.focus()
	}

	const handleMsg = e => {
		if(e.target.value){
			setMsg(e.target.value[0].toUpperCase() + e.target.value.slice(1));
		}else{
			setMsg('');
		}
	}

	useEffect(() => {
		if (chosenEmoji) {
			setMsg(prev => prev + chosenEmoji.emoji)
		}
	}, [chosenEmoji])

	useEffect(() => {
		if (body === '') return
		setMsg(body)
	}, [body])

/* 	useEffect(() => {
		if (body2 === '') return
		refInput?.current?.value = body2

	}, [body2]) */

	useEffect(() => {
		const handleResize = () => {
			const height = window.innerHeight
			const width = window.innerWidth
			setDimensions({
				height,
				width,
			})
		}

		window.addEventListener('resize', handleResize);

		return _ => window.removeEventListener('resize', handleResize)
	}, [dimensions])

	useEffect(() => {
		setDimensions({
			height: window.innerHeight,
			width: window.innerWidth
		})
	}, [])


	async function uploadImage(files) {
		const formData = new FormData();
		formData.append("file", files[0]);
		formData.append("upload_preset", "uw1r0jwd");
		axios
			.post(
				"https://api.cloudinary.com/v1_1/mahumada/image/upload",
				formData,
			)
			.then(async (response) => {
				setFileMsg(response.data.secure_url);
				setFileConfirm(true);
			});
		}

	async function uploadFile(files) {
		const formData = new FormData();
		formData.append("file", files[0]);
		formData.append("upload_preset", "uw1r0jwd");
		axios
			.post(
				"https://api.cloudinary.com/v1_1/mahumada/raw/upload",
				formData,
			)
			.then(async (response) => {
				setFileMsg(response.data.secure_url);
				setFileConfirm(true);
			});
	}

	useEffect(() => {
		if(socket){
			socket?.on("chats", (chats) => {
				setChats(chats.reverse());
			})
			socket?.on("new chat", (chat) => {
				setChats([...chats, chat])
			})
			socket?.on("read", (message) => {
				let newChats = chats.map((chat) => {
					if(chat._id === message.chatId){
						chat.messages[0] = message;
					}
					return chat;
				})
				setChats(newChats);
			})
			socket?.on("private message", (message) => {
				const newChats = chats.map(chat => {
					if(message.chatId === chat._id){
						chat.messages.unshift(message);
					}
					return chat;
				})
				setChats(newChats);
				if(selected !== null){
					setMensajes(newChats[selected].messages);
					if(message.sender !== userId.id){
						socket.emit("read", { chatId: chats[selected]._id })
					}
				}
			})
			return () => {
				socket.off("private message");
				socket.off("chats");
				socket.off("new chat");
				socket.off("read");
			}
		}
	}, [socket, selected, chats])

	const sendMessage = async () => {
		if (msg) {
			socket.emit("private message", { receiver: { id: sellerData.data.data._id, name: sellerData.data.data.username }, text: msg });
			setMsg('');
		}
	}
	const sendInputMessage = async (message) => {
		socket.emit("private message", { receiver: { id: sellerData.data.data._id, name: sellerData.data.data.username }, text: message });
		setMsg('');
	}

	const confirmFile = () => {
		sendInputMessage(fileMsg);
		setFile(false);
	}

	const [fileMsg, setFileMsg] = useState();
	const [fileConfirm, setFileConfirm] = useState(false);

	return (
		<Container height={`${dimensions.height - 70}px`}>

			{file ? !fileConfirm ? <Modal>
				<InputFile onChange={(e) => uploadImage(e.target.files)} name='file' id='file' type={'file'} />
				<LabelFile for='file'>Subir imagen</LabelFile>
				<InputFile onChange={(e) => uploadFile(e.target.files)} name='docs' id='docs' type={'file'} />
				<LabelFile for='docs'>Subir archivo</LabelFile>
				<p onClick={() => setFile(false)} style={{ position: 'absolute', top: '0', right: '10px', fontSize: '30px', cursor: 'pointer', color: '#000000' }}>x</p>
			</Modal> : <Modal>
				{/* <a style={{fontSize: '1vw'}} href={fileMsg} target="_blank">{fileMsg}</a> */}
				<img src={fileMsg} style={{maxHeight: "60vh", maxWidth: "60vw"}} />
				<Confirm onClick={() => confirmFile()}>ENVIAR</Confirm>
				<p onClick={() => setFile(false)} style={{ position: 'absolute', top: '0', right: '10px', fontSize: '30px', cursor: 'pointer', color: '#000000' }}>x</p>
			</Modal> : ''}

			{file && <ModalOpen></ModalOpen>}


			{modal && <Modal>
				<InputFile onChange={(e) => uploadFile(e.target.files)} name='file' id='file' type={'file'} />
				<LabelFile for='file'>Agregar archivo</LabelFile>
				<p onClick={() => setModal(false)} style={{ position: 'absolute', top: '0', right: '10px', fontSize: '30px', cursor: 'pointer', color: '#000000' }}>x</p>
			</Modal>}

			{modal && <ModalOpen></ModalOpen>}

			{selected === null ? (
				<Chats>

					<GeneralOrArchivados >

					</GeneralOrArchivados>
					{chats.map((chat, index) => {
						return <ChatBox
							data={chat}
							read={chats[index].messages[0] === undefined || chats[index].messages[0].seen || chats[index].messages[0].sender === userId.id}
							setChats={setChats}
							chats={chats}
							index={index}
							setSelected={setSelected}
							execute={handleChatSelect}
							selected={selected}
							userId={userId.id}
							key={index}
							img={userImg(chat)}
						/>
					})}
				</Chats>
			) : selected !== null && dimensions.width <= 980 ? (
				''
			) : (
				<Chats>
					<GeneralOrArchivados>
					</GeneralOrArchivados>
					{chats.map((chat, index) => {
						return <ChatBox
							data={chat}
							read={chats[index].messages[0].seen || chats[index].messages[0].sender === userId.id}
							setChats={setChats}
							chats={chats}
							index={index}
							setSelected={setSelected}
							execute={handleChatSelect}
							selected={selected}
							userId={userId.id}
							img={userImg(chat)}
							key={index}
						/>
					})}
				</Chats>
			)}

			{selected !== null && (
				<Chat>

					
					<Header>
						<NameAndOnline>
							{dimensions.width <= 980 && <Back onClick={() => setSelected(null)} />}
							{chats[selected].online && <Online />}
							<Name>{chats[selected].members[0].id === userId.id ? chats[selected].members[1].name : chats[selected].members[0].name}</Name>
						</NameAndOnline>
						<Avatar src={userImg(chats[selected]) || 'img/user.png'} />
					</Header>
					<Conversation>
						{mensajes.map((msg, index) => {
							const state = msg.sender !== userId.id;
							return <Message
								key={index}
								bool={state}
								direction={state ? 'flex-start' : 'flex-end'}
								msg={msg.text}
								id={msg.id}
							/>
						})}
					</Conversation>
					{emoji && (
						<Picker
							pickerStyle={{
								width: '100%',
								height: '670px'
							}}
							onEmojiClick={onEmojiClick}
						/>
					)}

					<SendContainer>
						<Sticker
							onClick={() => handleEmoji()}
							src='/img/sticker.png'
						/>
						<Input
							value={msg || ''}
							onClick={() => setEmoji(false)}
							onChange={e => handleMsg(e)}
							type='input'
							placeholder='Enviar mensaje...'
							ref={refInput}
							onKeyPress={(e) => {
								if (e.key === 'Enter') {
									sendMessage()
								}
							}
							}
						/>
						<File onClick={() => {setFile(true); setFileConfirm(false);}} src='/img/file.png'></File>
						<button style={{ background: 'none', border: 'none' }} type="button" onClick={sendMessage}><File src='/img/send.svg' /></button>
						{/* 					<File onClick={() => setModal(!modal)} src='as'></File> */}


					</SendContainer>
				</Chat>
			)}

			{selected === null && (
				<Box>
					<Image src="/img/loginPreview.png" />
				</Box>
			)}
		</Container>
	)
}
export default ChatSection
