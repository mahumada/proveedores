import styled, { keyframes } from 'styled-components'


import { IoMdArrowBack, IoImagesOutline } from "react-icons/io";


export const ModalOpen = styled.div`
  position: absolute;
  width: 100%;
  height: 100%;
  background-color: rgba(220,220,220,0.7);
  z-index: 8;
`

export const Back = styled(IoMdArrowBack)` 
	width: 22px;
	height: 22px;
	cursor: pointer;
`

export const Container = styled.div`
	width: min(1620px, 95%);
	height: ${props => props.height || '100%'};
	display: flex;
	gap: 18px;
	justify-content: space-evenly;
	align-items: center;
	margin: 0 auto;
	padding: 30px 0;
	@media(max-width: 980px){
		padding: 0;
		width: 100%;
	}
`

export const InputFile = styled.input`
	width: 0.1px;
	height: 0.1px;
	opacity: 0;
	overflow: hidden;
	position: absolute;
	z-index: -1;
	font-size: 1.125em;
  font-weight: 700;
  color: white;
  background-color: black;
  display: inline-block;

	&:focus{
		background-color: red;
	}
`
export const LabelFile = styled.label`
  display: flex;
	justify-content: center;
	align-items: center;
	padding: 4px 14px;
	height: 27px;
	background: #ffffff;
	border: 1px solid #476ade;
	border-radius: 13px;
	color: #476ade;
	font-weight: normal;
	font-size: 13.5px;
	width: 60%;
	margin: auto;
	margin-top: 3%;
		cursor: pointer;
		&:hover {
			background-color: #476ade;
			color: white;
		}

`

export const GeneralOrArchivados = styled.div`
	display: flex;
	justify-content: flex-end;
	gap: 5px;
`
export const General = styled.p`
	cursor: pointer;
	font-weight: 400;
	font-size: 13.5px;
	letter-spacing: -0.408px;
	color: #000000;
`
export const Archivados = styled.p`
	cursor: pointer;
	font-weight: 400;
	font-size: 13.5px;
	letter-spacing: -0.408px;
	color: #828282;
`

export const Chats = styled.div`
	height: 100%;
	width: 360px;
	min-width: 315px;
	display: flex;
	flex-direction: column;
	overflow-y: auto;
	padding: 1px;
	@media(max-width: 980px){
		padding-top: 24px;
	}

	&::-webkit-scrollbar {
		-webkit-appearance: none;
		appearance: none;
	}

	&::-webkit-scrollbar:vertical {
		width: 2px;
	}

	&::-webkit-scrollbar-button:increment {
		display: none;
	}

	&::-webkit-scrollbar-button {
		display: none;
	}

	&::-webkit-scrollbar-thumb {
		background: #797979;
		border-radius: 20px;
	}

	&::-webkit-scrollbar-track {
		margin-top: 27px;
	}

	@media screen and (max-width: 980px) {
		width: 80%;
		margin: auto;
	}
`

export const Chat = styled.div`
	width: 1170px;
	position: relative;
	height: 100%;
	display: flex;
	flex-direction: column;
	justify-content: flex-start;
	background: #ffffff;
	box-shadow: 0px 0px 5px rgba(0, 0, 0, 0.13);
	border-radius: 11px;
	overflow: hidden;
`

export const Header = styled.header`
	width: 100%;
	min-height: 72px;
	display: flex;
	justify-content: space-between;
	align-items: center;
	padding: 0 15px;
	border-bottom: 1px solid #e1e1e1;
`

export const Avatar = styled.img`
	width: 40px;
	height: 40px;
	border-radius: 6px;
`

export const Name = styled.p`
	font-weight: 400;
	font-size: 15px;
	letter-spacing: -0.408px;
	color: #000000;
`

export const NameAndOnline = styled.div`
	display: flex;
	align-items: center;
	gap: 9px;
`

export const Online = styled.div`
	width: 13px;
	height: 13px;
	border-radius: 50%;
	background: #28b446;
`

export const SendContainer = styled.div`
	width: 100%;
	min-height: 67px;
	display: flex;
	justify-content: space-between;
	align-items: center;
	padding: 0 22px;
	border: 1px solid #e1e1e1;
`

export const Sticker = styled.img`
	cursor: pointer;
`

export const Input = styled.input`
	width: calc(100% - 90px);
	height: 18px;
	border: 2px solid black;
	outline: none;
	border: none;
	font-weight: 400;
	font-size: 15px;
	color: #000000;
	padding-left: 5px;
	background: #EEE;
	padding: 14px 0px 14px 12px;
	border-radius: 100px;

	&::placeholder {
		font-weight: 400;
		font-size: 15px;
		color: #000000;
	}
`

export const File = styled.img`
	cursor: pointer;
`

export const animation = keyframes`
0% { margin-bottom: -47px }
100% { margin-bottom: 0 }
`

export const Conversation = styled.div`
	width: 100%;
	height: 100%;

	padding: 9px 15px;
	display: flex;
	gap: 4px;
	flex-direction: column-reverse;
	overflow-y: auto;
	transition: .7s;
	overflow-x: hidden;

	&::-webkit-scrollbar {
		-webkit-appearance: none;
		appearance: none;
	}

	&::-webkit-scrollbar:vertical {
		width: 3px;
	}

	&::-webkit-scrollbar-button:increment {
		display: none;
	}

	&::-webkit-scrollbar-button {
		display: none;
	}

	&::-webkit-scrollbar-thumb {
		background: #797979;
		border-radius: 20px;
	}

	.emoji-scroll-wrapper {
		background-color: red;
	}

	.content-wrapper {
		background-color: red;
	}

	/*class="content-wrapper" class="emoji-scroll-wrapper">*/
`

export const Box = styled.div`
	width: 1170px;
	height: 100%;
	background: #476ade;
	box-shadow: 0px 0px 5px rgba(0, 0, 0, 0.13);
	border-radius: 9px;
	background-image: url('/img/slide1.png'), url('/img/blackCircle.png');
	background-repeat: no-repeat;
	background-position: center;
	position: relative;
	overflow: hidden;
	display: flex;
	justify-content: center;
	align-items: center;

	@media screen and (max-width: 980px) {
		display: none;
	}
`

export const CompraTodo = styled.p`
	font-weight: 800;
	font-size: 36px;
	color: #ffffff;
	margin-top: 235px;
`

export const Image = styled.img`
	width: 100%;
	height: 100%;
`

export const Confirm = styled.p`
	font-size: 2vw;
	font-weight: 600;
	width: 100%;
	text-align: center;
	transition: .3s;
	cursor: pointer;

	&:hover {
		color: #476ade;
	}
`