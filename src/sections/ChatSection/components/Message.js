import styled, { keyframes } from 'styled-components'

const Container = styled.div`
	width: 100%;
	display: flex;
	justify-content: ${({ direction }) => direction};
	align-items: center;
`

export const animationL = keyframes`
	0% { margin-left: -100px }
	100% { margin-left: 0 }
`

export const animationR = keyframes`
	0% { margin-right: -100px }
	100% { margin-right: 0 }
`

export const Msg = styled.p`
	max-width: 60%;
	padding: 9px 22px;
	display: flex;
	align-items: center;
	letter-spacing: -0.408px;
	color: ${props => props.clickable ? '#476ade' : '#000000'};
	text-decoration: ${props => props.clickable ? 'underline' : 'none'};
	cursor: ${props => props.clickable ? 'pointer' : 'auto'};
	background: ${({ bool }) => (bool ? '#F5F5F5' : '#FFFFFF')};
	border: 1px solid #f5f5f5;
	border-radius: 18px;
	font-weight: 400;
	font-size: 15px;
	/* animation-name: ${props => props.bool ? animationL : animationR}; */
	animation-duration: .8s;
	animation-fill-mode: forwards;
	animation-iteration-count: 1;
	animation-timing-function: ease-out;
	border: none;
	box-shadow: 0 0 3px rgba(0, 0, 0, .25);
	margin-top: 3px;
	word-break: break-all;

	@media screen and (max-width: 663px) {
		max-width: 80%;
	}
`

const Img = styled.img` 
	max-width: min(60%, 450px);
`

const Message = ({ direction, msg, bool }) => {
	return (
		<Container direction={direction}>
			{msg.includes('/image') ? <Img src={msg} /> : msg.includes('/raw') ? <Msg bool={bool} clickable={true} onClick={() => window.open(msg)}>{msg}</Msg> : <Msg bool={bool}>{msg}</Msg>}
		</Container>
	)
}
export default Message
