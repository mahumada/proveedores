import { useEffect, useState } from 'react'
import styled from 'styled-components'
import axios from 'axios'

const Box = styled.div`
	width: 100%;
	min-height: 65px;
	display: flex;
	align-items: center;
	gap: 18px;
	margin-bottom: 9px;
	position: relative;
	padding: 0 18px 0 18px;
	background: #ffffff;
	box-shadow: 0px 0px 5px rgba(0, 0, 0, 0.13);
	border-radius: 9px;
	cursor: pointer;
	transition: 300ms background-color ease-out;
	border: ${({ focus }) => (focus ? '1px solid #476ADE' : 'none')};
	overflow: hidden;
	&:hover {
		background-color: rgba(208, 211, 213, 0.1);
	}
`

const Avatar = styled.img`
	width: 40px;
	height: 40px;
	border-radius: 50%;
`

const Read = styled.div`
	position: absolute;
	top: 9px;
	right: 9px;
	width: 9px;
	height: 9px;
	border-radius: 50%;
	background: #FF3636;
`

const NameAndMsg = styled.div`
	display: flex;
	flex-direction: column;
	width: 100%;
`
const Name = styled.p`
	color: #000000;
	font-weight: 400;
	font-size: 15px;
	letter-spacing: -0.408px;
`

const Msg = styled.p`
	display: flex;
	align-items: center;
	width: 225px;
	font-weight: 400;
	font-size: 13.5px;
	letter-spacing: -0.408px;
	color: #828282;
	white-space: nowrap;
	overflow: hidden;
	text-overflow: ellipsis;
`

const ChatBox = ({ index, data, selected, setSelected, setChats, chats, execute, userId, read, img }) => {
	/* 	const handleChats = index => {
			if (!chats[index].read) {
				const copy = [...chats]
				copy[index].read = true
				setChats(copy)
			}
		} */
	return (
		<Box
			onClick={() => {
				setSelected(index)
				/* handleChats(index) */
				execute(index)
			}}
			focus={selected === index}
		>
			<Avatar src={img || '/img/user.png'}></Avatar>
			<NameAndMsg>
				{data && data.messages.length && <>
					<Name>{data?.members[0]?.id === userId ? data?.members[1]?.name : data?.members[0]?.name}</Name>
					<Msg>{data?.messages[0].text.includes('/image') ? 'Imagen' : data?.messages[0].text.includes('/raw') ? 'Documento' : data?.messages[0].text.length > 30 ? `${data?.messages[0].text.slice(0, 30)}...` : data?.messages[0].text}</Msg>
				</>}
			</NameAndMsg>
			{!read && <Read />}
		</Box>
	)
}
export default ChatBox
