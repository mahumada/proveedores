import styled from "styled-components"

const Container = styled.section`
	width: 100%;
	min-height: 100vh;
	padding-top: 81px;
	display: flex;
	align-items: center;
	justify-content: space-evenly;
	gap: 27px;
	scroll-snap-align: start;
	@media screen and (max-width: 1200px) {
		flex-direction: column;
		justify-content: center;
	}
`

const Title = styled.p`
	text-align: center;
	letter-spacing: -0.408px;
	color: #476ade;
	font-weight: 700;
	font-size: min(36px, 8vw);
	max-width: 90%;
`

const Img = styled.img`
	width: min(900px, 95%);
	@media(max-height: 800px){
		width: 70%;
	}
`

const Search = () => {
	return (
		<Container>
			<Title data-aos="fade-right" data-aos-duration="1000">
				Busca el producto que necesitas
			</Title>
			<Img data-aos="zoom-in" data-aos-duration="1000" src="/img/home.png" alt="milux" />
		</Container>
	)
}
export default Search
