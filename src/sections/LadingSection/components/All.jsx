import styled from "styled-components"
import { keyframes } from "styled-components"

const Container = styled.section`
	width: 100%;
	height: 100vh;
	display: flex;
	align-items: center;
	justify-content: space-evenly;
	gap: 11px;
	padding-top: 81px;
	scroll-snap-align: start;
	@media screen and (max-width: 1200px) {
		flex-direction: column-reverse;
		justify-content: center;
	}
`

const animate = keyframes`
  from {
    opacity: 0;
	/* transform: translateX(-300px); */
	transform: scale(0);
  }

  to {
    opacity: 1;
	/* transform: translateX(0); */
	transform: scale(1);

  }
`

const animate2 = keyframes`
  from {
    opacity: 0;
	transform: translateY(300px); 

	
  }

  to {
    opacity: 1;
	transform: translateY(0);

  }
`

const Right = styled.div`
	width: min(540px, 100%);
	display: flex;
	flex-direction: column;
	align-items: center;
	justify-content: center;
	gap: 9px;
	&.lazy2 {
		animation-name: ${animate2};
		animation-duration: 1s;
	}
`

const Title = styled.p`
	text-align: center;
	letter-spacing: -0.408px;
	color: #476ade;
	font-weight: 700;
	font-size: min(36px, 7vw);
`

const Description = styled.p`
	text-align: center;
	font-weight: 400;
	font-size: min(18px, 3vw);
	letter-spacing: -0.408px;
	color: #000000;
	max-width: 90%;
`

const Img = styled.img`
	width: min(875px, 90%);
	&.lazy {
		animation-name: ${animate};
		animation-duration: 1s;
	}
	@media(max-height: 800px){
		width: 95%;
	}
`

const All = () => {
	return (
		<Container>
			<Img id="img" /* data-aos="fade-right" data-aos-duration="1000" */ src="/img/chat.png" alt="milux" />
			<Right id="box" /* data-aos="fade-left" data-aos-duration="1000" */>
				<Title>Todo en el mismo lugar!</Title>
				<Description>
					Con nuestra app de mensajeria integrada, podes contactarte con cualquier miembro de{" "}
					<span style={{ fontWeight: "700" }}>Milux</span>, al aceptar su oferta o consultar una duda.
				</Description>
			</Right>
		</Container>
	)
}

export default All
