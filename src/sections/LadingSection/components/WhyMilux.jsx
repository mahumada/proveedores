import styled from "styled-components"

const Container = styled.section`
	width: 95%;
	min-height: 100vh;
	display: flex;
	flex-direction: column;
	align-items: center;
	justify-content: center;
	margin: 0 auto;
	scroll-snap-align: start;
`

const Title = styled.p`
	display: flex;
	align-items: center;
	text-align: center;
	letter-spacing: -0.408px;
	color: #476ade;
	font-weight: 700;
	font-size: min(38px, 6vw);
`

const Description = styled.p`
	width: min(660px, 90%);
	text-align: center;
	font-weight: 400;
	font-size: min(20px, 4vw);
	letter-spacing: -0.408px;
	color: #000000;
`

const WhyMilux = () => {
	return (
		<Container>
			<Title data-aos="fade-up" data-aos-duration="1000">
				¿Por que usar <img style={{ marginLeft: "9px", width: 'min(120px, 24vw)' }} src="/img/logo2.0.png" />?
			</Title>
			<Description data-aos="fade-up" data-aos-duration="1000">
			Porque nuestra plataforma ofrece la oportunidad de conectarse con un público más amplio y aumentar la visibilidad tanto para proveedores como clientes. Los proveedores pueden publicar sus ofertas en un lugar centralizado, mientras que los clientes pueden acceder a una amplia variedad de proveedores y productos en un solo lugar, comparar y evaluar diferentes opciones para encontrar la mejor oferta.
			</Description>
		</Container>
	)
}

export default WhyMilux
