import styled from "styled-components"

const Container = styled.section`
	width: 95%;
	min-height: calc(100vh - 81px);
	display: flex;
	flex-direction: column;
	align-items: center;
	justify-content: center;
	margin: 0 auto;
	scroll-snap-align: end;
`

const Title = styled.p`
	display: flex;
	align-items: center;
	text-align: center;
	letter-spacing: -0.408px;
	color: #476ade;
	font-weight: 700;
	font-size: min(38px, 11vw);
	max-width: 90%;
`

const Description = styled.p`
	text-align: center;
	width: min(660px, 90%);
	text-align: center;
	font-weight: 400;
	font-size: min(20px, 4vw);
	letter-spacing: -0.408px;
	color: #000000;
`

const WhatIsMilux = () => {
	return (
		<Container>
			<Title data-aos="fade-up" data-aos-duration="1000">
				¿Que es <img style={{ marginLeft: "9px", width: 'min(120px, 24vw)' }} src="/img/logo2.0.png" />?
			</Title>
			<Description data-aos="fade-up" data-aos-duration="1000">
			Es una web diseñada para conectar proveedores de insumos con sus clientes en un solo lugar. Los proveedores pueden publicar sus ofertas de productos y servicios, y los clientes pueden buscar y encontrar fácilmente lo que necesitan con una experiencia de usuario intuitiva y opciones de búsqueda avanzadas.
			</Description>
		</Container>
	)
}

export default WhatIsMilux
