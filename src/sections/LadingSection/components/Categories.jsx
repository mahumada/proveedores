import styled from "styled-components"

const Container = styled.section`
	width: 100%;
	min-height: 100vh;
	display: flex;
	flex-direction: column;
	align-items: center;
	justify-content: center;
	scroll-snap-align: start;
	padding-top: 81px;
	gap: 12px;
`

const Title = styled.p`
	text-align: center;
	letter-spacing: -0.408px;
	color: #476ade;
	font-weight: 700;
	font-size: min(38px, 7vw);
	max-width: 90%;
`

const Description = styled.p`
	text-align: center;
	font-weight: 400;
	font-size:  min(20px, 3vw);
	letter-spacing: -0.408px;
	color: #000000;
	max-width: 90%;
`

const Img = styled.img`
	width: min(720px, 95%, 100vh);
	margin-top: 16px;
`

const Categories = () => {
	return (
		<Container>
			<Title data-aos="fade-up" data-aos-duration="1000">
				Echa un vistazo a nuestra plataforma
			</Title>
			<Description data-aos="fade-up" data-aos-duration="1000">
				Selecciona las categorias que vendes o las que consumis
			</Description>
			<Img data-aos="zoom-in" data-aos-duration="1000" src="/img/milux6.png" alt="milux" />
		</Container>
	)
}

export default Categories
