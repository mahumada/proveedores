import styled from "styled-components"
import { useRouter } from "next/router"

const Container = styled.section`
	width: 100%;
	height: 100vh;
	padding-top: 81px;
	display: flex;
	align-items: center;
	justify-content: center;
	gap: 9px;
	scroll-snap-align: end;
	@media screen and (max-width: 1200px) {
		flex-direction: column;
		scroll-snap-align: start;
	}
`
const Img = styled.img`
	position: absolute;
	bottom: -12px;
	right: 0;
	width: 20%;
	height: 65%;
	@media (max-height: 800px) {
		width: 16%;
		right: 10%;
		height: auto;
	}
`

const Img2 = styled.img`
	width: 100%;
	@media (max-height: 800px) {
		width: 80%;
		margin-left: 5%;
	}
`

const Right = styled.div`
	width: min(90%, 720px);
	display: flex;
	align-items: center;
	position: relative;
`

const Left = styled.div`
	width: min(540px, 90%);
	display: flex;
	flex-direction: column;
	align-items: center;
`
const Title = styled.p`
	font-weight: 400;
	font-size: min(18px, 4vw);
	line-height: 32px;
	text-align: center;
	letter-spacing: -0.408px;
	color: #000000;
	max-width: 90%;
`

const Description = styled.p`
	font-weight: 400;
	font-size: min(18px, 4vw);
	letter-spacing: -0.408px;
	color: #000000;
	text-align: center;
	width: min(505px, 90%);
`

const Button = styled.button`
	width: 50%;
	height: 32px;
	display: flex;
	justify-content: center;
	align-items: center;
	background-color: ${({ bg }) => bg};
	color: ${({ color }) => color};
	border-radius: 27px;
	border: none;
	outline: none;
	border: 1px solid #476ade;
	cursor: pointer;
	font-weight: 700;
	font-size: min(14px, 3vw);
`

const Logo = styled.img`
	width: min(120px, 30vw);
`

const Buttons = styled.div`
	margin-top: 18px;
	display: flex;
	align-items: center;
	width: 90%;
	gap: 9px;
`

const GrowYourBusiness = () => {
	const router = useRouter()
	return (
		<Container>
			<Left data-aos="fade-right" data-aos-duration="1000">
				<Title>HACE CRECER TU COMERCIO CON</Title>
				<Logo src="/img/logo3.0.png" alt="milux" />
				<Description>
				Bienvenido a nuestra plataforma web para conectar proveedores de insumos con sus respectivos clientes. Aquí encontrará una solución innovadora y eficiente para conectarse con proveedores y clientes confiables y encontrar exactamente lo que necesita.
				</Description>
				<Buttons>
					<Button onClick={() => router.push("/signup")} color="#FFF" bg="#476ADE">
						Registrarse
					</Button>
					<Button as="a" href="#precios" color="#476ADE" bg="#FFF">
						Ver planes
					</Button>
				</Buttons>
			</Left>
			{/* <img src="/img/all.png" data-aos="fade-right" data-aos-duration="1000" /> */}
			<Right data-aos="fade-left" data-aos-duration="1000">
				<Img2 src="/img/mac.png" alt="milux" />
				<Img src="/img/iphone.png" alt="milux" />
			</Right>
		</Container>
	)
}

export default GrowYourBusiness
