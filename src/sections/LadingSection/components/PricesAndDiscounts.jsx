import styled from "styled-components"

const Container = styled.section`
	width: 95%;
	min-height: 100vh;
	display: flex;
	flex-direction: column;
	align-items: center;
	justify-content: center;
	gap: 36px;
	margin: 0 auto;
	scroll-snap-align: start;
	padding-top: 81px;
`

const Top = styled.div`
	display: flex;
	justify-content: center;
	flex-direction: column;
	align-items: center;
	gap: 9px;
`

const Title = styled.p`
	font-weight: 700;
	font-size: min(38px, 10vw);
	line-height: min(26px, 2vw);
	letter-spacing: -0.408px;
	color: #476ade;
	text-align: center;
`

const Precios = styled.div`
	display: flex;
	flex-direction: column;
	align-items: center;
	gap: 12px;
`

const Img = styled.img`
	width: min(900px, 95%);
`

const Terms = styled.p`
	width: min(900px, 95%);
	font-size: 12px;
	font-weight: 600;
	color: black;
	text-align: center;
`

const PricesAndDiscounts = () => {
	return (
		<Container id="precios">
			<Top data-aos="fade-up" data-aos-duration="1000">
				<Title>Planes</Title>
			</Top>
			<Precios>
				<Img data-aos="zoom-in" data-aos-duration="1000" src="/img/precios.png" alt="milux" />
				<Terms>*Actualmente, el uso de nuestra plataforma es gratuito para todos nuestros usuarios. Sin embargo, nos reservamos el derecho de implementar una tarifa en el futuro y notificaremos a todos nuestros usuarios con al menos un mes de antelación.</Terms>
			</Precios>
		</Container>
	)
}

export default PricesAndDiscounts
