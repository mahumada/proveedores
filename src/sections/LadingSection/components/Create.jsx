import styled from "styled-components"

const Container = styled.section`
	width: 100%;
	margin: 0 auto;
	min-height: 100vh;
	display: flex;
	align-items: center;
	justify-content: space-evenly;
	padding: 0 45px 0 90px;
	scroll-snap-align: start;
	padding-top: 81px;
	@media screen and (max-width: 1400px) {
		flex-direction: column-reverse;
		justify-content: center;
		padding: 0;
		gap: 48px;
	}
`

const Title = styled.p`
	width: min(745px, 90%);
	text-align: center;
	letter-spacing: -0.408px;
	color: #476ade;
	font-weight: 700;
	font-size: min(32px, 6vw);
	padding: 0 3%;
`

const Gif = styled.img`
	width: min(1000px, 90%);
	border-radius: 12px;
	box-shadow: 0 0 4px rgba(0, 0, 0, .4);
`

const Create = () => {
	return (
		<Container>
			<Gif src="/img/gif.gif" alt="GIF" />
			<Title data-aos="zoom-in" data-aos-duration="1000">
				Crea una publicacion cargando los diferentes productos que ofreces o que estas buscando
			</Title>
		</Container>
	)
}

export default Create
