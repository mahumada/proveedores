import { useRouter } from "next/router"

import styled from "styled-components"
import Link from "next/link"

const Container = styled.header`
	width: 100%;
	min-height: 81px;
	position: fixed;
	top: 0;
	z-index: 1;
	display: flex;
	justify-content: space-between;
	align-items: center;
	padding: 0 24px;
	background-color: #ffffff;
	box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.25);
	z-index: 99999;
`

const Nav = styled.nav`
	height: 100%;
	display: flex;
	gap: max(18px, 2.5vw);
	justify-content: center;
	align-items: center;
	color: #000000;
	font-weight: 400;
	font-size: 14px;

	@media(max-width: 600px) {
		flex-direction: column;
		align-items: center;
		gap: 7.5px;
		width: 30%;
	}
`

const Sign = styled.div`
	display: flex;
	justify-content: center;
	align-items: center;
	gap: 9px;
	@media (max-width: 600px){
		flex-direction: column;
		align-items: center;
		gap: 7.5px;
		width: 30%;
	}
`

const Button = styled.button`
	width: 100px;
	height: 24px;
	display: flex;
	justify-content: center;
	align-items: center;
	background-color: ${({ bg }) => bg};
	color: ${({ color }) => color};
	border-radius: 7.5px;
	border: none;
	outline: none;
	cursor: pointer;
	border: 1px solid #476ade;
`

const Img = styled.img`
	width: min(120px, 20vw);
	cursor: pointer;
	position: absolute;
	left: calc(50% - 60px);
	@media(max-width: 650px) {
		position: static;
	}
`

const Bloqueado = styled.p`
	cursor: pointer;
	color: #476ade;
	text-decoration: underline;
`

const Enlace = styled(Link)`
`

const TextoEnlace = styled.a`
	font-weight: 700;
	font-size: 16px;
	cursor: pointer;
`

const NavBar2 = ({noLinks, blocked}) => {
	const router = useRouter()
	return (
		<Container>
			<Nav>
				{!noLinks && <Enlace href="/#precios"><TextoEnlace>Planes</TextoEnlace></Enlace>}
				<Enlace href="/ayuda"><TextoEnlace>Contacto</TextoEnlace></Enlace>
			</Nav>
			<Img src="/img/logo3.0.png" onClick={() => window.location = '/'} />
			{blocked ? <Sign><Link href="/"><Bloqueado>Usuario Bloqueado</Bloqueado></Link></Sign> : <Sign>
				<Button onClick={() => router.push("/signup")} bg="#FFFFFF" color="#476ADE">
					Registrarse
				</Button>
				<Button onClick={() => router.push("/login")} bg="#476ADE" color="#FFFFFF">
					Iniciar sesion
				</Button>
			</Sign>}
		</Container>
	)
}
export default NavBar2
