const getPosts = ( body ) => {
  return fetch(`/api/post`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body,
    //credentials: true
  })
  .then(response => response.json())
  .then(response => console.log(response));
}

export default getPosts;