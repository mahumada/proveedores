const userLogin = async( body ) => {
  return await fetch(`api/login`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(body),
    credentials: "include"
  })
  .then(response => response.json())
}

export default userLogin;