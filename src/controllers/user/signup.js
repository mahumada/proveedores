const userSignup = async( body ) => {
  return await fetch(`api/user`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(body),
    credentials: "include"
  })
  .then(response => response.json())
}

export default userSignup;