const blockUser = async( id, block ) => {
    return await fetch(`/api/user/block`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        id, block
      }),
    })
    .then(response => response.json())
    .then(response => console.log('blocked user', response));
  }
  
  export default blockUser;