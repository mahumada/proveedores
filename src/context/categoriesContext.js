import { createContext, useState, useMemo, useContext, useCallback } from 'react'

export const CategoriesContext = createContext()

export const useCategoriesContext = () => {
	return useContext(CategoriesContext)
}

const CategoriesContextProvider = ({ children }) => {
	const [category, setCategory] = useState({ id: null, name: '' })

	const handleCategory = useCallback((name, id) => {
		setCategory({ ...category, id, name })
	}, [category, setCategory])


	const value = useMemo(
		function () {
			return {
				category,
				handleCategory,
			}
		},
		[category, handleCategory]
	)

	return (
		<CategoriesContext.Provider value={value}>
			{children}
		</CategoriesContext.Provider>
	)
}
export default CategoriesContextProvider
