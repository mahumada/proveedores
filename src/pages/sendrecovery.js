import Head from 'next/head'
import Layout from '../components/common/Layout';
import SendRecoverySection from '../sections/SendRecoverySection';
export default function Offers() {

	return (
		<>
			<Head>
				<title>Milux - Ayuda</title>
			</Head>
			<Layout>
      	<SendRecoverySection />
			</Layout>
		</>
	)
}
