import OffersDetailsSection from '../../sections/OffersDetailsSection/OffersDetailsSection';
import { useRouter } from 'next/router';
import Head from 'next/head'
import Layout from '../../components/common/Layout';
import { useState, useEffect } from 'react';

export default function Offer() {
    const router = useRouter();
    const [dimensions, setDimensions] = useState({});

    useEffect(() => {
		const handleResize = () => {
			const height = window.innerHeight
			const width = window.innerWidth
			setDimensions({
				height,
				width,
			})
		}

		window.addEventListener('resize', handleResize);

		return _ => window.removeEventListener('resize', handleResize)
	}, [dimensions])

	useEffect(() => {
		setDimensions({
			height: window.innerHeight,
			width: window.innerWidth
		})
    setTimeout(() => {
      setDimensions({
        height: window.innerHeight,
			  width: window.innerWidth
      })
    }, 1000)
	}, [])

    console.log('id', router.query.id);

    return (<>
        <Head><title>Offer</title></Head>
        <Layout heightContainer={`${dimensions.height}px`}>
            <OffersDetailsSection height={dimensions.height} id={router.query.id} />
        </Layout>

    </>)
}
