import Head from 'next/head'
import LoginSection from '../sections/LoginSection'

export default function Login() {
	return (
		<>
			<Head>
				<title>Milux - Login</title>
			</Head>
			<LoginSection />
		</>
	)
}
