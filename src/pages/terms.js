import React from 'react';
import TermsSection from '../sections/TermsSection';
import Head from 'next/head';

const Terms = () => {
  return (
    <>
      <Head>
        <title>Milux - Terminos y condiciones</title>
      </Head>
        <TermsSection />
    </>
  )
}

export default Terms