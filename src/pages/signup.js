import Head from 'next/head';
import SignupSection from '../sections/SignupSection';

export default function Login() {
  return (
    <>
      <Head>
        <title>Milux - Registro</title>
      </Head>
        <SignupSection />
    </>
  )
}
