import { ProfileSection } from '../sections/Profile'

export default function Profile() {
	return <ProfileSection edit={true} />
}
