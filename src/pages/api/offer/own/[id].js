import connectDB from "../../../../utils/database";
import Offer from "../../../../utils/models/offer/offerModel";

const offer = async (req, res) => {
  const { id } = req.query;
  try{
    const response = await Offer.find({ buyer_id: id }).sort({createdAt: -1});
    return res.status(200).json(response);
  }catch(err){
    return res.status(400).json({error: err});
  }
}

export default connectDB(offer);