import connectDB from "../../../utils/database";
import Offer from "../../../utils/models/offer/offerModel";

const bySellerId = async (req, res) => {
  const { id, status } = req.body;
  switch (req.method) {
    case "POST":
      try{
        let offer = await Offer.findOneAndUpdate({ _id: id }, { status });
        res.status(200).json(offer);
      }catch(err){
        res.json({error: err})
      }
    break;
    
    default:
    break;
  }
}

export default connectDB(bySellerId);