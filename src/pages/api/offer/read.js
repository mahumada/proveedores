import connectDB from "../../../utils/database";
import Offer from "../../../utils/models/offer/offerModel";

const bySellerId = async (req, res) => {
  const { id } = req.query;
  switch (req.method) {
    case "GET":
      try{
        let offer = await Offer.findOneAndUpdate({ _id: id }, { seen: true });
        console.log(offer)
        res.status(200).json(offer);
      }catch(err){
        res.json({error: err})
      }
    break;
    
    default:
    break;
  }
}

export default connectDB(bySellerId);