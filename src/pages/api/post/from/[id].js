import Post from '../../../../utils/models/post/postModel';
import Seller from '../../../../utils/models/seller/sellerModel';
import connectDB from '../../../../utils/database';

const getPostById = async (req, res) => {
  const {id} = req.query;
  if(!id) return res.status(400).json({error: "Bad Request"});
  try{
    const sellerList = await Seller.find({ user_id: id });
    const seller_id = sellerList[0]._id.toString();
    const post = await Post.find({ seller_id });
    return res.status(200).json({data: post});
  }catch(err){
    return res.status(404).json({error: err.message});
  }
}


export default connectDB(getPostById);