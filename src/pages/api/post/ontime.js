import connectDB from "../../../utils/database";
import Post from "../../../utils/models/post/postModel.js";

const post = async (req, res) => {
  switch (req.method) {
    case "GET":
      try {
        let posts = await Post.find({}).sort({createdAt: -1});
        const currentDate = new Date();
        const response = posts.filter(post => {
          const postDate = new Date(post.proposal.endDate);
          if(currentDate - postDate <= 0){
            return true;
          }else{
            return false;
          }
        })
        return res.status(200).send(response)
      } catch (err) {
        res.json({ error: err });
      }
      break;
    default:
      break;
  }
};

export default connectDB(post);
