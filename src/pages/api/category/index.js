import connectDB from "../../../utils/database";
import Category from "../../../utils/models/categoryModel";

const category = async (req, res) => {
  const { _id } = req.query;
  switch (req.method) {
    case "GET":
      try {
        const categories = await Category.find({});
        res.status(200).send(categories)
      } catch (err) {
        res.status(500).json({ error: err.message })
      }
      break;
    case "POST":
      const { name } = req.body;
      if (!name) return res.status(400).json({ error: "Bad Request" })
      const category = new Category(req.body);
      const categoryDB = await category.save();
      return res.status(200).send(categoryDB);
      break;
    case "DELETE":
      if (!_id) res.status(400).json({ error: "Bad Request" });
      Category.findOneAndDelete({ _id }, function (err, docs) {
        if (err) {
          res.status(404).json({ "error": err });
        } else {
          res.status(200).json({ "Deleted Category": docs });
        }
      });
      break;
    case "PUT":
      const updateData = req.body;
      try {
        if (_id && updateData) {
          const updated = await Category.findOneAndUpdate({ _id }, updateData);
          return res.status(200).json({ status: "updated" });
        }
        return res.status(400).json({ error: "bad request" });
      } catch (err) {
        return res.status(400).json({ error: err.message });
      }
      break;
    default:
      break;
  }
}

export default connectDB(category);