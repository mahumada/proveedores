import User from '../../../utils/models/user/userModel';
import connectDB from '../../../utils/database';

const getUserCount = async (req, res) => {
  const users = await User.find({});
  const userCount = users.length;

  res.json({userCount: userCount})
}

export default connectDB(getUserCount);