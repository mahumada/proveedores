import Seller from '../../../utils/models/seller/sellerModel'
import connectDB from '../../../utils/database';

const addInteraction = async (req, res) => {
  const { _id } = req.query;
  if (!_id) return res.status(400).send("Bad request");
  const seller = await Seller.findOne({ _id });
  let interactions = seller.interactions + 1
  const updated = await Seller.findOneAndUpdate({ _id }, { interactions: interactions });
  if (!updated) return res.status(500).send("bad request");
  return res.send("Updated");

}

export default connectDB(addInteraction);