import Seller from '../../../utils/models/seller/sellerModel'
import connectDB from '../../../utils/database'

const getSellerByUserId = async (req, res) => {
  const { _id } = req.query
  try {
    const seller = await Seller.findOne({ user_id: _id })
    if(seller) return res.send(seller);
    return res.status(404).json({error: "Not found"})
  } catch (err) {
    res.status(500).json({ error: err })
  }
}

export default connectDB(getSellerByUserId);