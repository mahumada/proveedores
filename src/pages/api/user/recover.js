import connectDB from '../../../utils/database';
import User from '../../../utils/models/user/userModel';
import bcrypt from 'bcrypt'

const user = async (req, res) => {
  
  switch(req.method){
    case "POST":
      console.log(req.body)
      let update = await User.findOne({ _id: req.body.id }).catch(err => {return res.status(400).json(err)});
      if(req.body.password !== req.body.repeatPassword){ return res.status(400).json({error: "Passwords do not match."})}
      const passwordhash = await bcrypt.hash(req.body.password, 10);
      update.password = passwordhash;
      await update.save();
      return res.status(200).json({ success: true, msg: 'New password succesfully set!', update });

    default:
      return res.status(404).json({error: "REQ METHOD NOT VALID."})
  
  }

}

export default connectDB(user);