import connectDB from '../../../utils/database';
import User from '../../../utils/models/user/userModel';

const user = async (req, res) => {
  
  switch(req.method){
    case "POST":
      let update = await User.findOne({ _id: req.body.id }).catch(err => {return res.status(400).json(err)});
      update.blocked = req.body.block;
      await update.save();
      return res.status(200).json({ success: true, msg: 'blocked status succesfully changed!', update });

    default:
      return res.status(404).json({error: "REQ METHOD NOT VALID."})
  
  }

}

export default connectDB(user);