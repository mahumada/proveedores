import connectDB from '../../../utils/database';
import User from '../../../utils/models/user/userModel';
import bcrypt from 'bcrypt'

const user = async (req, res) => {
  
  switch(req.method){
    case "POST":
      let user = await User.findOne({ email: req.body.email }).catch(err => {return res.status(400).json(err)});
      return res.status(200).json({ id: user._id });

    default:
      return res.status(404).json({error: "REQ METHOD NOT VALID."})
  
  }

}

export default connectDB(user);