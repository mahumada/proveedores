import Post from '../../../utils/models/post/postModel';
import connectDB from '../../../utils/database';

const getPostOfUser = async (req, res) => {
  const { seller_id } = req.query;
  if(!seller_id) return res.status(400).json({error: "Bad request"});

  try{
    const post = await Post.find({ seller_id: seller_id })
    return res.send(post);
  }catch(err){
    res.status(500).json({error: err.message});
  }

}
export default connectDB(getPostOfUser);