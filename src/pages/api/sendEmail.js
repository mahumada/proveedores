export default function (req, res) {  
  let nodemailer = require('nodemailer');
  let hbs = require('nodemailer-express-handlebars');
  const transporter = nodemailer.createTransport({
    port: 465,
    host: "c2541031.ferozo.com",
    auth: {
      user: 'no-responder@milux.com.ar',
      pass: 'Administradoresmati99',
    },
    secure: true,
  })

  transporter.use('compile', hbs({
    viewEngine: {
      extName: ".hbs",
      partialsDir: "public/html",
      defaultLayout: false
    },
    viewPath: 'public/html', //Path to email template folder
    extName: '.hbs' //extendtion of email template
  }));

  const mailData = {
    from: 'no-responder@milux.com.ar',
    to: req.body.email,
    subject: req.body.subject,
    template: req.body.template,
    context: req.body.context
  }

  transporter.sendMail(mailData, (err, info) => {
    if(err)
      return res.status(400).json({ err })
    else
      return res.status(200).json({ info })
  })

}