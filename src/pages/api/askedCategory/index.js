import connectDB from '../../../utils/database';
import AskedCategory from '../../../utils/models/askedCategory/askedCategoryModel';

const askedCategory = async (req, res) => {
  
  switch(req.method){
    case "GET":
      const result = await AskedCategory.find({});
      return res.status(200).json(result);

    case "POST": 
    console.log(req.body)
      const { name } = req.body;
      if (name) {
        try {
          let newCat = await AskedCategory.create(req.body);
          return res.status(200).json(newCat);
        } catch (error) {
          return res.status(400).json(error);
        }
      } else {
        return res.status(400).send("No name received.")
      }
      
    case "DELETE":
      const { _id } = req.query;
      await AskedCategory.findOneAndDelete({ _id }, function (err, docs) {
        if (err) {
          return res.status(404).json({err});
        }
        else {
          return res.status(200).json(docs);
        }
      })

    default:
      return res.status(404).json({error: "REQ METHOD NOT VALID."})
  
  }

}

export default connectDB(askedCategory);