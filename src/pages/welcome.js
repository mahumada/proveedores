import Head from 'next/head'
import WelcomeSection from '../sections/WelcomeSection'

export default function Welcome() {
	return (
		<>
			<Head>
				<title>Milux - Bienvenida</title>
			</Head>

			<WelcomeSection welcome />
		</>
	)
}
