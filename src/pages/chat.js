import Head from 'next/head'
import ChatSection from '../sections/ChatSection'
import Layout from '../components/common/Layout'

export default function Chat() {
	return (
		<>
			<Head>
			<title>Milux - Chat</title>
			</Head>
			<Layout heightContainer={'auto'}>
				<ChatSection />
			</Layout>
		</>
	)
}
