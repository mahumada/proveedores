import Head from 'next/head';
import SellerSignupSection from '../sections/SellerSignupSection';

export default function Login() {
  return (
    <>
      <Head>
        <title>Milux - Vendedor</title>
      </Head>
        <SellerSignupSection />
    </>
  )
}
