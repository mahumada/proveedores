import Head from 'next/head'
import OffersSection from '../sections/OffersSection';
import Layout from '../components/common/Layout';
export default function Offers() {

	return (
		<>
			<Head>
				<title>Milux - Ofertas</title>
			</Head>
			<Layout heightContainer={"100vh"}>
				<OffersSection />
			</Layout>
		</>
	)
}
