import Head from 'next/head'
import RecoverySection from '../sections/RecoverySection'
import Layout from '../components/common/Layout';
export default function Offers() {

	return (
		<>
			<Head>
				<title>Milux - Ayuda</title>
			</Head>
			<Layout>
      	<RecoverySection />
			</Layout>
		</>
	)
}
