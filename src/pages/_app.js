import '../styles/globals.css'
import CategoriesContextProvider from '../context/categoriesContext'
import { AuthProvider } from '../context/AuthContext'

function MyApp({ Component, pageProps: { session, ...pageProps } }) {
	return (
		<AuthProvider>
			<CategoriesContextProvider>
				<Component {...pageProps} />
			</CategoriesContextProvider>
		</AuthProvider>
	)
}

export default MyApp