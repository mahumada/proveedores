import Head from 'next/head'
import AdminSection from '../sections/AdminSection'

export default function Help() {
	return (
		<>
			<Head>
				<title>Administracion Milux</title>
			</Head>
			<AdminSection></AdminSection>
		</>
	)
}
