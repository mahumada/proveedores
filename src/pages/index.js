import Head from 'next/head';
import Feed from '../sections/Feed';
import Layout from '../components/common/Layout';
import LandingSection from '../sections/LadingSection/LandingSection';
import { useState, useEffect } from 'react';
import axios from 'axios';
import NavBar2 from '../sections/LadingSection/components/NavBar';
import Filter from '../components/common/Filter';
import { useRouter } from 'next/router'

export default function Home({ socket }) {
  const [login, setLogin] = useState(null);
  const router = useRouter();
  const { search } = router.query;
  const [blocked, setBlocked] = useState();
  const [orderBy, setOrderBy] = useState();
  const [dimensions, setDimensions] = useState({});
  const [category, setCategory] = useState();
  useEffect(() => {
    setLogin(localStorage.getItem('loggedUser'));
    let response;
    if(login){
      const a = async() => {
        response = await axios.get(
          `/api/user/${JSON.parse(login).id}`
          );
        axios.get(
          `/api/user/getseller?_id=${JSON.parse(login).id}`
          ).catch(err => window.location = '/sellerSignup');
          setBlocked(response.data.data.blocked);
        }
        a()
      }
  }, [login]);

  useEffect(() => {
		const handleResize = () => {
			const height = window.innerHeight
			const width = window.innerWidth
			setDimensions({
				height,
				width,
			})
		}

		window.addEventListener('resize', handleResize);

		return _ => window.removeEventListener('resize', handleResize)
	}, [dimensions])

	useEffect(() => {
		setDimensions({
			height: window.innerHeight,
			width: window.innerWidth
		})
    setTimeout(() => {
      setDimensions({
        height: window.innerHeight,
			  width: window.innerWidth
      })
    }, 1000)
	}, [])

  if (!login) {
    return <LandingSection></LandingSection>;
  }

  function logOut(){
		if (typeof window !== 'undefined') {
			// Perform localStorage action
			localStorage.clear();
			window.location = '/';
		}
	}

  if(login && blocked){
    return <>
    <NavBar2 noLinks={true} blocked={true} />
      <div style={{width: '100vw', height: '100vh', background: 'white', display: 'flex', justifyContent: 'center', alignItems: 'center', flexDirection: 'column', gap: '16px'}}>
        <h1 style={{fontSize: '24px', fontWeight: 700, }}>USUARIO BLOQUEADO <img src="/img/block.png" /></h1>
        <p style={{maxWidth: '60%', textAlign: 'center' }}>Este mensaje puede ser mostrado en caso de que se haya registrado y no hayan pasado más de 24hs hasta que nuestro equipo verifique los datos ingresados. O en el caso de que haya incumplido con nuestros <a href="/ayuda" style={{ color: '#476ade', textDecoration: 'underline' }}>términos y condiciones</a>.</p>
        <p style={{maxWidth: '60%', textAlign: 'center' }}>Para más ayuda visite nuestra página de <a href="/ayuda" style={{ color: '#476ade', textDecoration: 'underline' }}>ayuda</a> o contacte al soporte al mail: <b style={{fontWeight: 600}}>soporte@milux.com.ar</b></p>
        <p>Muchas gracias!</p>
        <button onClick={logOut} style={{cursor: 'pointer', padding: '3px 14px', fontSize: '16px', borderRadius: '11px', border: 'none', marginTop: '14px'}}>Cerrar sesion</button>
      </div>
    </>
  }

  
  if(login && !blocked){ return (
    <>
      <Head>
        <title>Milux - Feed</title>
        <meta name="apple-mobile-web-app-capable" content="yes" />
        <meta name="apple-mobile-web-app-capable" content="yes" />
        <meta name="mobile-web-app-capable" content="yes" />
      </Head>
      <Layout heightContainer={`${dimensions.height}px`} setCategory={setCategory}>
        <Filter category={category} setOrderBy={setOrderBy} />
        <Feed
          title='Publicaciones Recientes'
          blob='NUEVO'
          socket={socket}
          orderBy={orderBy}
          search={search}
          setCategory={category}
          height={dimensions.height}
        />
      </Layout>
    </>
  )};
}
