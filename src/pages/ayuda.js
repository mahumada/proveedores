import Head from 'next/head'
import HelpSection from '../sections/HelpSection'
import Layout from '../components/common/Layout'

export default function Help() {
	return (
		<>
			<Head>
				<title>Milux - Ayuda</title>
			</Head>
			<Layout heightContainer={'auto'} >
				<HelpSection />
			</Layout>
		</>
	)
}
