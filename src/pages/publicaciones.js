import Head from 'next/head'
import Filter from '../components/common/Filter'
import Layout from '../components/common/Layout'
import MyPosts from '../sections/MyPostsSection'
import { useState } from 'react'

export default function Publicaciones() {

  const [orderBy, setOrderBy] = useState();

	return (
		<>
			<Head>
				<title>Milux - Mis Publicaciones</title>
			</Head>
			<Layout heightContainer={'auto'}>
				<Filter setOrderBy={setOrderBy}  />
        <MyPosts orderBy={orderBy} />
			</Layout>
		</>
	)
}
