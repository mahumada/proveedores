import Head from 'next/head'
import WelcomeSection from '../sections/WelcomeSection'
import Layout from '../components/common/Layout'

export default function Settings() {
	return (
		<>
			<Head>
				<title>Milux - Opciones</title>
			</Head>
			<Layout heightContainer={'100vh'} noLinks={true} >
				<WelcomeSection />
			</Layout>
		</>
	)
}
