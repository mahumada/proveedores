import EditSection from '../../sections/EditSection/index.js';
import { useRouter } from 'next/router';

export default function Edit() {
	const router = useRouter();
	const { postId } = router.query;

	return (
		<>
			<EditSection id={postId} />
		</>
	)
}
