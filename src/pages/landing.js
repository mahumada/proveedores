import LandingSection from "../sections/LadingSection/LandingSection"
import Head from "next/head"
const Landing = () => {
    return (
        <>
            <Head>
                <title>Bienvenido!</title>
            </Head>
            <LandingSection />
        </>
    )

}

export default Landing
