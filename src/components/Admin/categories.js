import { useEffect, useState } from "react";
import axios from "axios";
import {
  ButtonSaveModal,
  Edit,
  Email,
  ModalOpen,
  User,
  Username,
  Users,
  Wrapper,
  ModalParagraph,
  Span,
  ModalContainer,
  BlockBtn,
  Filter,
  FilterBtn
} from "./styles.jsx";
import Modal from "../../components/common/Modal/index";
import blockUser from "../../controllers/user/block.js";

const Categories = ({ filters }) => {
  const [users, setUsers] = useState([]);
  const [user, setUser] = useState({});
  const [open, setOpen] = useState(false);
  const [sellers, setSellers] = useState([]);
  useEffect(() => {
    async function getUsers() {
      try {
        let data = await axios.get("/api/user");
        if(filters === 'down'){
          return setUsers(data.data.reverse());
        }else{
          return setUsers(data.data);
        }
      } catch (error) {
        console.log(error);
      }
    }
    getUsers();
  }, [filters]);

  useEffect(() => {
    async function getUsers() {
      try {
        let data = await axios.get("/api/seller");
        return setSellers(data.data);
      } catch (error) {
        console.log(error);
      }
    }
    getUsers();
  }, [])

  const block = async(id, block) => {
    const response = await blockUser(id, block);
    console.log(response)
    try {
      let data = await axios.get("/api/user");
      console.log("users data", data);
      setUsers(data.data);
    } catch (error) {
      console.log(error);
    }
    closeModal();
    return;
  }

  function openModal(user) {
    setOpen(true);
    setUser(user);
  }

  function closeModal() {
    setOpen(false);
    setUser({});
  }
  return (
    <>
      {open && <ModalOpen></ModalOpen>}
      <Wrapper>
        <Users>
          <User style={{ marginBottom: "40px" }}>
            <Username style={{ fontWeight: "bold" }}>USUARIO</Username>
            <Username style={{ fontWeight: "bold", width: "10%" }}>
              CATEGORIA
            </Username>
          </User>
          {users.length &&
            users.map((user, index) => {
              switch(filters){
                case 'blocked':
                  if(!user.blocked){ return };
                  break;
                case 'unblocked':
                  if(user.blocked){ return };
                  break;
                default:
                  break;
              }
              let UserCategories = [];
              user?.interestedCategories && user.interestedCategories.map(i => UserCategories += i.name + ' ');
              console.log(UserCategories.length && UserCategories)
              return <User key={index}>
                <Username style={ user.blocked ? {color: 'red'} : {}}>{user.username}</Username>
                <Username style={{textAlign: 'end'}}>{UserCategories.length ? UserCategories : '-'}</Username>
              </User>
            })}
        </Users>
      </Wrapper>
      {open && (
        <Modal>
          <ModalContainer>
            <img
              style={{ width: "60px", height: "60px", borderRadius: "50%" }}
              src={user.image}
            />
            <ModalParagraph>{user.username}</ModalParagraph>
            <ModalParagraph style={{ fontWeight: 'bold' }}>{user.code}</ModalParagraph>
            <ModalParagraph>
              <Span>Email: </Span> {user.email}
            </ModalParagraph>
            <ModalParagraph>
              <Span>Telefono: </Span>
              {user.phone}
            </ModalParagraph>
            <ModalParagraph>
              <Span>Bloqueado: </Span>
              {user.blocked ? 'SI' : 'NO'}
            </ModalParagraph>
            <ModalParagraph>
              <Span>Tipo:  </Span>
              {user.code && user.code[0] === 'P' ? 'Vendedor' : 'Comprador'}
            </ModalParagraph>
            <ModalParagraph>
              <Span>Referido:  </Span>
              {user.referredBy || 'Sin referido'}
            </ModalParagraph>
            {sellers && sellers.map(seller => {
              if(seller.user_id === user._id){
                return Object.keys(seller).map((key, i) => {
                if(key !== '_id' && key !== 'user_id' && key !== '__v' && key !== 'createdAt' && key !== 'updatedAt'){
                return <ModalParagraph>
                  <Span>{key}:  </Span>
                  {seller[key]}
                </ModalParagraph>
              }})
              }else{
                return;
              }
            })}
              <div>
              <BlockBtn>
                <img src={"/img/warning.png"} />
              </BlockBtn>
              <BlockBtn onClick={async() => await block(user._id, !user.blocked)}>
                <img src={"/img/block.png"} />
              </BlockBtn>
            </div>
            <ButtonSaveModal onClick={() => closeModal()}>
              Cerrar
            </ButtonSaveModal>
          </ModalContainer>
        </Modal>
      )}
    </>
  );
};

export default Categories;
