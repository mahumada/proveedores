import { useRouter } from 'next/router';
import Link from 'next/link';
import styled from 'styled-components';

const Enlace = styled.a` 
    border-bottom: ${({ isHere }) => isHere ? '1px solid #000000' : 'none'};
    color: #000;
	transition: all 300ms ease-out;
    cursor: pointer;
    height: 100%;
    position: relative;
	&:hover {
		transform: scale(1.1);
	}
`

const Dot = styled.div`
    background: red;
    width: 9px;
    height: 9px;
    border-radius: 16px;
    position: absolute;
    top: 0px;
    right: -10px;
`

function ActiveLink({ href, children, unread }) {
    const router = useRouter();

    const isHere = router.asPath.includes("?") ? router.asPath.split("?")[0] === href : router.asPath === href;

    const handleClick = (e) => {
        e.preventDefault();
        router.push(href);
    };

    return (
        <Link href={href}>
            <Enlace isHere={isHere} onClick={handleClick}>
                {children}
                {unread && <Dot />}
            </Enlace>
        </Link>

    );
};
export default ActiveLink;