import React from 'react';
import { Container, Input, Label } from './styles';

const ContactForm = () => {
  return (
    <Container>
      <Label>Nombre</Label>
      <Input type="text" />
      <Label>Email</Label>
      <Input type="text" />
      <Label>Mensaje</Label>
      <Input type="text" />
    </Container>
  )
}

export default ContactForm;