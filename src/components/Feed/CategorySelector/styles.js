import styled from "styled-components";

export const Slide = styled.p`
  font-size: 17px;
  background: #FFF;
  padding: 10px 0 10px 0;
  text-align: center;
  margin-right: 8px;
  position: relative;
  width: 200px;
  display: flex;
  justify-content: center;
  border-radius: 20px;
  flex: 0 0 auto;
`