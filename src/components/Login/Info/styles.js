import styled from 'styled-components'

export const Wrapper = styled.div`
	display: flex;
	flex-direction: column;
	align-items: center;
	justify-content: center;
`

export const Title = styled.h1`
	font-size: 22px;
	font-weight: 700;
	color: #343434;
	margin-bottom: 0;
	@media(min-width: 2000px){
		font-size: 26px;
	}
`

export const Subtitle = styled.p`
	font-size: 14px;
	color: #828282;
	@media(min-width: 2000px){
		font-size: 18px;
	}
`
