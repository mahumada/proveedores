import React from "react";
import { SlideBG, SlideItem, Wrapper, SlideTitle, SlideSubtitle } from "./styles";

const Slide = ({ title, subtitle, imgUrl}) => {
  return (
    <Wrapper>
      <SlideItem src={imgUrl} />
      <SlideBG src="/img/blackCircle.png" />
      <SlideTitle>{title}</SlideTitle>
      <SlideSubtitle>{subtitle}</SlideSubtitle>
    </Wrapper>
  );
};

export default Slide;
