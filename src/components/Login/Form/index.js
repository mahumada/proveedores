import React, { useState, useEffect,useContext } from 'react';
import { FormWrapper, Small, Input, Recovery, Icon, InputWrapper, SubmitBtn, ExternalLoginBtn, ExternalLogo, ExternalText, ExternalButtons, Checkbox, CheckboxContainer, CheckboxLabel, TermsLink } from './styles';
import { useForm } from "react-hook-form";
import Switch from '../../common/switchSeller';
import userLogin from '../../../controllers/user/login';
import userSignup from '../../../controllers/user/signup';
import { Router, useRouter } from 'next/router';
import jwt from 'jsonwebtoken'
import { useAuthContext } from '../../../context/AuthContext'
import ErrorAlert from '../../common/ErrorAlert';
import Link from 'next/link'

const Form = ({ mode }) => {
  let router = useRouter();
  const { setUser, user } = useAuthContext()
  const [isToggledUserType, setToggledUserType] = useState(false);
  const [userType, setUserType] = useState("Proveedor")
  const { register, handleSubmit, watch, formState: { errors } } = useForm({mode:"all"});


  const checkError = (response) => {
    if(!response.token || response && response.error){
      setError(response.error);
      return true;
    }else{
      return false;
    }
  }

  const onSubmit = async (data) => {
    let inputs = {
      ...data,
      email: data.email.toLowerCase(),
      type: userType
    }
    if(mode === 'signup' && inputs.repeatpassword !== inputs.password){
      setError('Contrasenas distintas');
      return;
    }
    if (mode === "login"){
      const res = await userLogin(inputs);
      if(checkError(res)){ return; }
      const response = res.token;
      if (response) {
        localStorage.setItem('IdToken', response);
        const decoded = jwt.decode(response);
        setUser(decoded);
        await localStorage.setItem('loggedUser', JSON.stringify(decoded))
        router.replace('/');
      }
    } else {
      const signupresponse = await userSignup(inputs);
      const emailresponse = await fetch('/api/sendEmail', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({
          subject: `El usuario ${inputs.email} se ha registrado`,
          template: `newuser`,
          email: "miluxmati@gmail.com"
        })
      }).then(response => response.json());
      if(signupresponse){
        const { email, password } = inputs;
        const response = await userLogin({email, password});
        if (response) {
          localStorage.setItem('IdToken', response);
          const decoded = jwt.decode(response.token);
          setUser(decoded);
          await localStorage.setItem('loggedUser', JSON.stringify(decoded))
          router.replace("/sellerSignup");
        }
      }
    }
  }
  function onToggleUserType() {
    setToggledUserType(!isToggledUserType);
    if(isToggledUserType === true);
    setUserType("Proveedor");
    if(isToggledUserType ===false){
      setUserType("Comprador");
    }
  }

  const [error, setError] = useState();
  const [checked, setChecked] = useState(false);

  return (
    <>
      <ErrorAlert error={error} closeNotification={() => setError()}/>
      <FormWrapper onSubmit={handleSubmit(onSubmit)}>
        {mode === 'signup' && <>
          <InputWrapper>
            <Icon src="/img/nameIcon.svg" />
            <Input placeholder="Nombre" name="username"
              {...register("username",
                {
                  required: {
                    value: true,
                    message: "Ingresar un nombre de usuario es obligatorio"
                  },
                  minLength: {
                    value: 2,
                    message: "El nombre debe tener al menos 2 caracteres"
                  },
                  maxLength: {
                    value: 60,
                    message: "El nombre no puede superar los 60 caracteres"
                  }
                })
              }
            />
          </InputWrapper>
          <Small>
            {errors?.username?.message}
          </Small>
          <InputWrapper >
            <Icon src="/img/phoneIcon.svg" />
            <Input placeholder="Phone" name="phone" {...register("phone", {
              required: {
                value: true,
                message: "El numero de teléfono es obligatorio"
              },
              pattern: {
                value: /^[0-9\s]*$/,
                message: "El telefono ingresado no cumple con un formato valido"
              }
            })} />
          </InputWrapper>
          <Small>
            {errors?.phone?.message}
          </Small>


        </>}
        <InputWrapper  >
          <Icon src="/img/emailIcon.png" />
          <Input name="email" type='text' placeholder="Email" {...register("email", {
            required: {
              value: true,
              message: "Ingresar un email es obligatorio"
            },
            pattern: {
              value: /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i,
              message: "Ingrese un email valido"
            },
          }
          )}
          />
        </InputWrapper>
        <Small>
          {errors?.email?.message}
        </Small>

        <InputWrapper>
          <Icon src="/img/passwordIcon.png" />
          <Input placeholder="Contraseña" name="password" type="password" {...register("password",{
            required:{
            value:true,
            message:"Ingresar una contraseña es obligatorio"
          },
          // pattern:{
          //   value:/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z]{8,}$/,
          //   message:"La contaseña debe tener al menos 8 caracteres, 1 mayúscula, 1 minúscula y un número"
          // }
          }
          )} />
        </InputWrapper>
        {mode === 'signup' && <>
          <InputWrapper>
            <Icon src="/img/passwordIcon.png" />
            <Input placeholder="Repetir Contraseña" name="repeatpassword" type="password" {...register("repeatpassword",{
              required:{
              value:true,
              message:"Repetir contraseña es obligatorio"
            },
            // pattern:{
            //   value:/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z]{8,}$/,
            //   message:"La contaseña debe tener al menos 8 caracteres, 1 mayúscula, 1 minúscula y un número"
            // }
            }
            )} />
          </InputWrapper>
        </>}
        <Small>
          {errors?.password?.message}
        </Small>
        { mode === 'signup' && <InputWrapper>
          <Icon src="/img/referred.svg" />
          <Input placeholder="Codigo de Referido" name="referredBy" type="text" {...register("referredBy")} />
        </InputWrapper>}
        {
          mode === 'signup' && <Switch isToggled={isToggledUserType} onToggle={() => onToggleUserType()} on={'Proveedor'} off={'Comprador'} />
        }
        { mode === 'signup' && <CheckboxContainer type="checkbox">
          <Checkbox type="checkbox" onClick={() => setChecked(!checked)} value={checked}/>
          <CheckboxLabel>He leido y acepto los <TermsLink href="/terms" target="_blank">terminos y condiciones</TermsLink></CheckboxLabel>
        </CheckboxContainer>}
        {mode === 'login' && <Recovery href="/sendrecovery">¿Olvidaste tu contraseña?</Recovery>}
        <SubmitBtn type="submit" disabled={mode === 'signup' && !checked}>{mode === 'login' ? 'Ingresar' : 'Registrarse'}</SubmitBtn>
      </FormWrapper>
    </>
  )
}

export default Form;