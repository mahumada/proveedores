import styled from 'styled-components'

export const FormWrapper = styled.form`
	display: flex;
	flex-direction: column;
	align-items: flex-end;
	justify-content: space-evenly;
	width: 80%;
	margin-top: 20px;
`

export const Label = styled.label``

export const Small = styled.small`
	color:red;
	width: 100%;
    position: relative;
	bottom: 12px;
`

export const Input = styled.input`
	font-size: 12.6px;
	border: none;
	outline: none;
	width: 100%;
	cursor: pointer;
	@media(min-width: 2000px){
		font-size: 18px;
	}
`

export const InputWrapper = styled.div`
	background: #fff;
	width: 100%;
	height: 36px;
	margin-bottom: 18px;
	border-radius: 8px;
	box-shadow: 0 0px 2px rgba(0, 0, 0, 0.8);
	overflow: hidden;
	display: flex;
	justify-content: center;
	align-items: center;
	@media(min-width: 2000px){
		box-shadow: 0 0px .125vw rgba(0, 0, 0, 0.8);
		height: 46px;
		border-radius: 12px;
	}
`

export const Recovery = styled.a`
	color: #4182bd;
	font-size: 13px;
	width: 100%;
	text-align: center;
	margin-bottom: 12px;
	@media(min-width: 2000px){
		font-size: 18px;
	}
`

export const Icon = styled.img`
	width: 22px;
	height: 22px;
	margin: 9px;
	filter: invert(57%) sepia(11%) saturate(6%) hue-rotate(319deg) brightness(93%) contrast(86%);
	@media(min-width: 2000px){
		width: 24px;
		height: 24px;
	}
`

export const SubmitBtn = styled.button`
	border: none;
	font-size: 13px;
	background: ${props => props.disabled ? '#777' : '#476ade'};
	width: 100%;
	height: 36px;
	color: #fff;
	border-radius: 20px;
	cursor: ${props => props.disabled ? '' : 'pointer'};
	@media(min-width: 2000px){
		font-size: 18px;
		height: 42px;
	}
`

export const ExternalButtons = styled.div`
	display: flex;
	justify-content: space-between;
	width: 80%;
	margin-top: 0px;
	cursor: pointer;
	display: none;
`

export const ExternalLoginBtn = styled.div`
	display: flex;
	align-items: center;
	justify-content: center;
	height: 36px;
	width: 48%;
	border: 1px solid #000;
	border-radius: 36px;
	cursor: pointer;
`

export const ExternalLogo = styled.img`
	margin-right: 10px;
	width: 22px;
`

export const ExternalText = styled.p`
	font-size: 13px;
	color: #000;
`

export const CheckboxContainer = styled.div`
	display: flex;
	align-items: center;
	width: 100%;
	margin-top: 6px;
	margin-bottom: 8px;
	gap: 8px;
`

export const Checkbox = styled.input`
	@media(min-width: 2000px){
		width: 16px;
		height: 16px;
	}
`

export const CheckboxLabel = styled.div`
	display: flex;
	gap: 5px;
	font-size: 15px;
	@media(max-width: 500px){
		font-size: 13px;
	}
	@media(min-width: 2000px){
		font-size: 16px;
	}
`

export const TermsLink = styled.a`
	color: #476ade;
	text-decoration: underline;
	cursor: pointer;
`