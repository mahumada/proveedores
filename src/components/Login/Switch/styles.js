import styled from "styled-components";

export const Wrapper = styled.div`
  margin-top: 18px;
  width: 80%;
`

export const Text = styled.p`
  font-size: 13px;
  color: #828282;
  width: 100%;
  text-align: center;
  margin-bottom: 11px;
  @media(min-width: 2000px){
		font-size: 18px;
	}
`

export const Button = styled.button`
  font-size: 13px;
  color: #4182BD;
  border-radius: 36px;
  border: 2px solid #4182BD;
  height: 36px;
  width: 100%;
  cursor: pointer;
  @media(min-width: 2000px){
		font-size: 18px;
    height: 42px;
	}
`