import styled from 'styled-components'

export const FormWrapper = styled.form`
	display: flex;
	flex-direction: column;
	align-items: center;
	justify-content: flex-start;
	width: 82%;
	margin-top: 40px;
	padding:0 1%;
	padding-top: 1%;
	height:55%;
`
export const Inputs = styled.div`
	display: flex;
	flex-direction: column;
	align-items: flex-end;
	justify-content: flex-start;
	width: 100%;
	padding:0 1%;
	padding-top: .5%;
	overflow-y:auto;
	height:100%;
`

export const Label = styled.label`
	width: 100%;
	font-weight:600;
	margin-bottom:14px;
`

export const Small = styled.small`
	color:red;
	width: 100%;
    position: relative;
	bottom: 16px;
`

export const Input = styled.input`
	font-size: 13px;
	border: none;
	outline: none;
	width: 100%;
	padding: 8px;
	border-radius: 10px;
	@media(min-width: 2000px){
		font-size: 18px;
	}
`
export const Option = styled.option`
	font-size: 13px;
	border: none;
	outline: none;
	width: 100%;
	padding: 8px;
`
export const Select = styled.select`
	width: 100%;
	padding: 8px;
	outline: none;
	border-radius:8px;
	background:#fff;
	margin-bottom: 18px;
	height: 36px;
`

export const InputWrapper = styled.div`
	background: #fff;
	width: 100%;
	height: 36px;
	margin-bottom: 18px;
	border-radius: 10px;
	box-shadow: 0 0px 2px rgba(0, 0, 0, 0.8);
	display: flex;
	justify-content: center;
	align-items: center;
	@media(min-width: 2000px){
		box-shadow: 0 0px .125vw rgba(0, 0, 0, 0.8);
		height: 46px;
		border-radius: 12px;
	}
`

export const Recovery = styled.a`
	color: #4182bd;
	font-size: 13px;
`

export const Icon = styled.img`
	width: 22px;
	height: 22px;
	margin: 10px;
	@media(min-width: 2000px){
		width: 24px;
		height: 24px;
	}
`

export const SubmitBtn = styled.button`
	margin-top: 18px;
	border: none;
	font-size: 13px;
	background: #476ade;
	width: 100%;
	height: 36px;
	color: #fff;
	border-radius: 20px;
	cursor: pointer;
	@media(min-width: 2000px){
		font-size: 18px;
		height: 42px;
	}
`

export const ExternalButtons = styled.div`
	display: flex;
	justify-content: space-between;
	width: 80%;
	margin-top: 18px;
`

export const ExternalLoginBtn = styled.div`
	display: flex;
	align-items: center;
	justify-content: center;
	height: 36px;
	width: 48%;
	border: 1px solid #000;
	border-radius: 40px;
`

export const ExternalLogo = styled.img`
	margin-right: 10px;
	width: 22px;
`

export const ExternalText = styled.p`
	font-size: 13px;
	color: #000;
`
