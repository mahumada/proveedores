import React, { useState, useEffect } from 'react';
import { FormWrapper, Select,Inputs,Option,Small, Input, Recovery, Icon, InputWrapper, SubmitBtn, ExternalLoginBtn, ExternalLogo, ExternalText, ExternalButtons, Label } from './styles';
import { useForm } from "react-hook-form";
import userLogin from '../../../controllers/user/login';
import userSignup from '../../../controllers/user/signup';
import Switch from '../../common/switchSeller';
import { Router, useRouter } from 'next/router';
import axios from "axios";
//import { password, password } from 'pg/lib/defaults';
const Form = ({ mode }) => {
  let router = useRouter();

  const { register, handleSubmit, watch, formState: { errors } } = useForm({mode:"all"});
  const [isToggledValidate, setToggledValidate] =useState(false);
  const [userId, setUserId] =useState(undefined);
  const [user, setUser] = useState();
  const onSubmit = async(data) => {
    let inputs={
      ...data,
      username: user.username,
      email: user.email,
      phone: user.phone,
      //userType:user,
      user_id:userId,
    }
    
    if(userId){
     const response = await axios.post("/api/seller",inputs)
      router.replace('/settings');
    }
    
  };
  
  useEffect(()=> {
    async function getId(){
      let local = await localStorage.getItem("loggedUser");
      let localParse = local ? JSON.parse(local) : false;
      if(!localParse){
        window.location = '/signup';
      }
      const userData = await fetch(`/api/user/${localParse.id}`).then(res => res.json());
      setUser(userData.data);
      return setUserId(localParse?.id); 
      }   
    getId();
  },[])
  
  const Password=watch("password")
  const checkPassword= input=>{
    return input==Password
  }
  return (
    <>
    
    <FormWrapper onSubmit={handleSubmit(onSubmit)}>
      <Inputs>
      {/* <InputWrapper>
            <Input placeholder="Nombre" name="username"
             {...register("username",
             {
               required:{
                 value:true,
                 message:"Ingresar un nombre de usuario es obligatorio"
               },
               minLength:{
                 value:2,
                 message:"El nombre debe tener al menos 2 caracteres"
               },
               maxLength:{
                 value:60,
                 message:"El nombre no puede superar los 60 caracteres"
               }
             })
             } 
            />
        </InputWrapper> */}
        <Small>
          {errors?.username?.message}
        </Small>
      {/* <InputWrapper>
          <Input name="email" type='text' placeholder="Email" {...register("email",{
							required:{
								value:true,
								message:"Ingresar un email es obligatorio"
							},
							pattern:{
								value:/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i,
								message:"Ingrese un email valido"
							},
							}
						)}     
            />       
        </InputWrapper> */}
        <Small>
          {errors?.email?.message}
        </Small>
        <InputWrapper>
        <Input name="socialReason" placeholder='Razón Social'
          {...register("socialReason",{
            required:{
              value:true,
              message:"Ingresar una razón social es obligatorio"
            }}
          )}
        />
      </InputWrapper>
      <Small>
            {errors?.socialReason?.message}
      </Small>
      <InputWrapper>
        <Input name="fantasyName" placeholder='Nombre de fantasía'
          {...register("fantasyName",{
            required:{
              value:true,
              message:"Ingresar un nombre de fantasía es obligatorio"
            }}
          )}
        />
      </InputWrapper>
      <Small>
            {errors?.fantasyName?.message}
      </Small>
      <InputWrapper>
        <Input name="cuit" placeholder='CUIT' maxLength={11}
          {...register("cuit",{
            required:{
              value:true,
              message:"Ingresar un CUIT es obligatorio"
            },
            pattern:{
            value:/^[0-9]{11}$/g,
            message:"Debe ingresar un CUIT válido de 11 digitos",
            }
          },
          
          )}
        />
      </InputWrapper>
      <Small>
            {errors?.cuit?.message}
      </Small>
      <InputWrapper>
        <Input name="address" placeholder='Dirección'
          {...register("address",{
            required:{
              value:true,
              message:"Ingresar una dirección es obligatorio"
            }}
          )}
        />
      </InputWrapper>
      <Small>
            {errors?.address?.message}
      </Small>
      <InputWrapper>
        <Input name="postalCode" placeholder='Código postal' maxLength={4}
          {...register("postalCode",{
            required:{
              value:true,
              message:"Ingresar un código postal es obligatorio"
            },
            pattern:{
              value:/^[0-9]{4}$/g,
              message:"Ingresar un código postal valido de 4 digitos"
            }
          }
          )}
        />
      </InputWrapper>
      <Small>
            {errors?.postalCode?.message}
      </Small>
      <InputWrapper>
        <Input name="city" placeholder='Ciudad'
          {...register("city",{
            required:{
              value:true,
              message:"Ingresar una ciudad es obligatorio"
            }}
          )}
        />
      </InputWrapper>
      <Small>
            {errors?.city?.message}
      </Small>
      <InputWrapper>
        <Input name="province" placeholder='Provincia'
          {...register("province",{
            required:{
              value:true,
              message:"Ingresar una provincia es obligatorio"
            }}
          )}
        />
      </InputWrapper>
      <Small>
            {errors?.province?.message}
      </Small>
      <InputWrapper>
        <Input name="category" placeholder='Categoria (Actividad principal)'
          {...register("category",{
            required:{
              value:true,
              message:"Ingresar una categoría es obligatorio"
            }}
          )}
        />
      </InputWrapper>
      <Small>
            {errors?.category?.message}
      </Small>
      {/* <InputWrapper >
            <Input placeholder="Teléfono" name="phone" {...register("phone",{
              required:{
                value:true,
                message:"El numero de teléfono es obligatorio"
              },
              pattern:{
                // /^((\+54\s?)?(\s?9\s?)?\d{2,3}[\s-]?\d{3,4}-?\d{3,4}|\d{10,11}|(\d{3,4}[\s-]){1,2}\d{3,4})$/g este es para tel de ARG
                value:/^[0-9\s]*$/,
                message:"El teléfono ingresado no cumple con un formato valido"
              }
            })} />
      </InputWrapper> */}
        <Small>
          {errors?.phone?.message}
        </Small>
          {/* <Switch isToggled={isToggledValidate} onToggle={()=>onToggleValidate()} on={'Proveedor'} off={'Comprador'}/> */}
      </Inputs>
      

      

        {mode === 'login' && <Recovery href="/recovery">Olvidaste tu contraseña?</Recovery>}
        <SubmitBtn type="submit">{mode === 'login' ? 'Ingresar' : 'Registrar'}</SubmitBtn>
      </FormWrapper>


      {/* <ExternalButtons>
        <ExternalLoginBtn>
          <ExternalLogo src="/img/googleLogo.png" />
          <ExternalText>Google</ExternalText>
        </ExternalLoginBtn>
        <ExternalLoginBtn>
          <ExternalLogo src="/img/facebookLogo.png" />
          <ExternalText>Facebook</ExternalText>
        </ExternalLoginBtn>
      </ExternalButtons> */}
    </>
  )
}

export default Form;