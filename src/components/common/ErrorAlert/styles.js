import styled, { keyframes } from "styled-components";

const slideIn = keyframes`
  0% { right: -400px }
  100% { right: 12px }
`

export const Wrapper = styled.div`
  background: darkred;
  width: 325px;
  height: 60px;
  border-radius: 14px;
  box-shadow: 0 0 4 rgba(0, 0, 0, .4);
  display: flex;
  align-items: center;
  justify-content: center;
  position: fixed;
  bottom: 12px;
  right: 12px;
  animation: ${slideIn} .6s ease-out;
  animation-iteration-count: 1;
  animation-fill-mode: forwards;
`

export const Image = styled.img`
  border-radius: 50%;
  width: 44px;
  height: 44px;
  margin-right: 12px;
`

export const TextContainer = styled.div`
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  justify-content: space-evenly;
  height: 80%;
  width: 70%;
`

export const TextTitle = styled.p`
  font-size: 17px;
  font-weight: 500;
  color: #FFF;
`

export const TextMessage = styled.p`
  font-size: 15px;
  font-weight: 400;
  color: #FFF;
  white-space: nowrap;
  max-width: 95%;
  overflow: hidden;
`

export const Close = styled.button`
  font-size: 16px;
  padding: 4px;
  border: none;
  background: none;
  color: #FFF;
  position: absolute;
  top: 0;
  right: 8px;
  display: flex;
  justify-content: center;
  align-items: center;
`