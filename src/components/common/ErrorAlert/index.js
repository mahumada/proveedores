import React, { useState } from 'react'
import { useEffect } from 'react'
import { Close, Image, TextContainer, TextMessage, TextTitle, Wrapper } from './styles'
import axios from 'axios'

const Notification = ({ error, closeNotification }) => {
  useEffect(()=>{
    const timer = setTimeout(()=>{
        closeNotification()
    }, 5000)
    return () => clearTimeout(timer);
  }, [error])

  return (
    <>
    {error && <Wrapper>
      <TextContainer onClick={closeNotification}>
        <TextTitle>Error</TextTitle>
        <TextMessage>{error}</TextMessage>
      </TextContainer>
      <Close onClick={closeNotification}>x</Close>
    </Wrapper>}
    </>
  )
}

export default Notification