import Navbar from "../Navbar"
import { NavMobile } from "../NavMobile"
import { Container, Main } from "./styles"
import { useState, useEffect } from "react"
import React from "react"
import { io } from "socket.io-client"
import Notification from "../Notification"
import LandingSection from "../../../sections/LadingSection/LandingSection"
import NavBar2 from "../../../sections/LadingSection/components/NavBar"
import { useRouter } from "next/router"
import Loading from "../../Loading"
import axios from "axios"

const Layout = ({ children, heightContainer, setCategory, noLinks }) => {
	const [mobile, setMobile] = useState(false)
	const [msg, setMsg] = useState()
	const [read, setRead] = useState(true);
	const [interacted, setInteracted] = useState(true);
	const [search, setNewSearch] = useState('');
	/* 	const [login, setLogin] = useState(null)
	
		useEffect(() => {
			setLogin(localStorage.getItem('loggedUser'))
		}, []) */

	const handleMobile = () => {
		setMobile(!mobile)
	}

	// Notificaciones
	const [token, setToken] = useState();
	const [socket, setSocket] = useState();
	const [user, setUser] = useState({});

	if(!read && window.location.pathname === '/chat' ){
		setRead(true);
	}

	useEffect(() => {
		setToken(localStorage.getItem("IdToken"))
	}, [])
	useEffect(() => {
		if (token) {
			const newSocket = io("https://chat.milux.com.ar", {
				auth: { token },
				autoConnect: true,
			})
			setSocket(newSocket)
			return () => newSocket.close()
		}
	}, [token])
	useEffect(() => {
		socket?.on("private message", message => {
			console.log("private message event + message", message)
			setMsg(message)
			setRead(false);
		})
		socket?.on("chats", (chats) => {
			let unread = false;
			chats.forEach(chat => {
				if(chat.messages.length && !chat.messages[0].seen && chat.messages[0].sender !== JSON.parse(localStorage.getItem('loggedUser')).id){
					unread = true;
				}
			})
			setRead(!unread);
		})
	}, [socket])

	const [blocked, setBlocked] = useState(false);
	const [offers, setOffers] = useState([]);

	useEffect(() => {
		if(token){
			const a = async() => {
				const offers = await fetch('/api/offer').then(res => res.json());
				setOffers(offers);
				const newuser = await fetch(`/api/user/${JSON.parse(localStorage.getItem('loggedUser')).id}`).then(res => res.json());
				setUser(newuser.data);
				setBlocked(newuser.data.blocked);
			}
			a();
		}
	}, [token])

	const childrenWithProps = React.Children.map(children, child => {
		// Checking isValidElement is the safe way and avoids a typescript
		// error too.
		if (React.isValidElement(child)) {
			return React.cloneElement(child, { socket, search, setInteracted, offers });
		}
		return child
	})


	useEffect(() => {
		async function a() {
			try {
				const seller = await axios.get(
					`/api/user/getseller?_id=${JSON.parse(localStorage.getItem("loggedUser")).id}`
				)
				const newOffers = await axios.get(`/api/offer/own?id=${seller.data._id}`)
				if(!newOffers.data[0].seen){
					setInteracted(false);
				}
			} catch (error) {
				console.log(error)
			}
		}
		a()
	}, [])

	const closeNotification = () => {
		setMsg(undefined)
	}

	const setSearch = (newSearch) => {
		if(window.location.href.split('/').at(-1) === "" || /\?search=[^&]*$/.test(window.location.href.split('/').at(-1))){
			window.location = `${window.location.pathname}?search=${newSearch}`;
		}else if(/\?category=[^&]*$/.test(window.location.href.split('/').at(-1))){
			window.location = `${window.location.href}&search=${newSearch}`;
		}else if(/\?search=[^&]*&category=[^&]*$/.test(window.location.href.split('/').at(-1))){
			window.location = `${window.location.pathname}?search=${newSearch}&${window.location.href.split('&')[1]}`;
		}else if(/\?category=[^&]*&search=[^&]*$/.test(window.location.href.split('/').at(-1))){
			window.location = `${window.location.href.split('&')[0]}&search=${newSearch}`;
		}else{
			window.location = `${window.location.pathname}?search=${newSearch}`;
		}
	}
	
	const router = useRouter();
	useEffect(() => {
		const newSearch = router.query.search;
		setNewSearch(newSearch);
	}, [])
  const admins = ['6448351ca79eb560f25d923f', '64487c1a344a37e512faf2ee'];
	return (<Container heightContainer={heightContainer}>
		<NavMobile isAdmin={admins.indexOf(user._id) !== -1} interacted={interacted} read={read} open={mobile} handleMobile={handleMobile}></NavMobile>
		{token && !blocked && <Navbar isAdmin={admins.indexOf(user._id) !== -1} interacted={interacted} read={read} open={mobile} handleMobile={handleMobile} setSearch={setSearch} setCategory={setCategory} search={search} />}
		<Main>{(!token || blocked) && <NavBar2 blocked={blocked} noLinks={noLinks} />}{childrenWithProps}</Main>
		<Notification msg={msg} closeNotification={closeNotification} />
	</Container>)
}

export default Layout;