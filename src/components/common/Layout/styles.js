import styled from 'styled-components'

export const Container = styled.div`
	display: flex;
	flex-direction: column;
	width: 100%;
	height: ${({ heightContainer }) => heightContainer};
	position: relative;
	overflow-x: hidden;
`

export const Main = styled.main`
	margin-top: 70px;
	width: 100%;
	height: 100%;
	background: #f8f8f8;
	@media(max-width: 1100px){
		background: white;
	}
`