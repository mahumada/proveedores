import { Wrapper, Categories, Category,LinkContainer, Arrow, Enlace, Read, CategoriesText } from './styles'
import { useState } from 'react'
import useFetch from '../../../../../hooks/useFetch'
import ActiveLink from '../../../../ActiveLink'

import Link from 'next/link'
import React from 'react'
import { useRouter } from 'next/router'

import { useCategoriesContext } from '../../../../../context/categoriesContext'
const Links = ({ user, isSeller, setCategory, read, interacted, isAdmin }) => {
	const [renderCategories, setRenderCategories] = useState(false)

	const { handleCategory, category } = useCategoriesContext()

	const { data } = useFetch('/api/category', 'get')
	const router = useRouter()

	const [categoryOffset, setCategoryOffset] = useState();

	const setCategoryLink = (newSearch) => {
		if(window.location.href.split('/').at(-1) === "" || /\?category=[^&]*$/.test(window.location.href.split('/').at(-1))){
			window.location = `${window.location.pathname}?category=${newSearch}`;
		}else if(/\?search=[^&]*$/.test(window.location.href.split('/').at(-1))){
			window.location = `${window.location.href}&category=${newSearch}`;
		}else if(/\?category=[^&]*&search=[^&]*$/.test(window.location.href.split('/').at(-1))){
			window.location = `${window.location.pathname}?category=${newSearch}&${window.location.href.split('&')[1]}`;
		}else if(/\?search=[^&]*&category=[^&]*$/.test(window.location.href.split('/').at(-1))){
			window.location = `${window.location.href.split('&')[0]}&category=${newSearch}`;
		}else{
			window.location = `${window.location.pathname}?category=${newSearch}`;
		}
	}
	return (
		<>
			<Wrapper>

				<LinkContainer>
				<ActiveLink href={'/'} children={'Inicio'}></ActiveLink>
				<CategoriesText onClick={() => { setRenderCategories(!renderCategories) }} ref={el => {
        // el can be null - see https://reactjs.org/docs/refs-and-the-dom.html#caveats-with-callback-refs
        if (!el) return;
				const offset = el.getBoundingClientRect().left;
				if(categoryOffset !== offset){
					setCategoryOffset(offset);
				}
      }}>
				

					Categorias
					{renderCategories ? (
						<Arrow src='/img/arrowUp.png' />
						) : (
							<Arrow src='/img/arrowDown.png' />
							)}
				</CategoriesText>
					<ActiveLink href={isSeller ? `/tooffer` : '/login'} children={'Crear publicación'}></ActiveLink>
					<ActiveLink unread={!read} href='/chat' children='Mensajes'></ActiveLink>
					<ActiveLink href='/ayuda' children='Ayuda'></ActiveLink>
					<ActiveLink unread={!interacted} href='/offers' children='Interacciones'></ActiveLink>
					{isAdmin && !window.location.href.split('/').at(-1).includes("publicaciones") && !window.location.href.split('/').at(-1).includes("settings") && !window.location.href.split('/').at(-1).includes("profile") && <ActiveLink href='/admin' children='Administrador'></ActiveLink>}
					{window.location.href.split('/').at(-1).includes("profile") && <ActiveLink href='/profile' children='Mi perfil'></ActiveLink>}
					{window.location.href.split('/').at(-1).includes("settings") && <ActiveLink href='/settings' children='Ajustes'></ActiveLink>}
					{window.location.href.includes("publicaciones") && <ActiveLink href='/publicaciones' children='Mis publicaciones'></ActiveLink>}
				</LinkContainer>
				<Categories renderCategories={renderCategories} offset={categoryOffset}>
				{data.map((ca, i) => (
					<div key={i}
						onClick={() => {
							// setCategory(ca.name)
							if(ca.name === category.name){
								window.location = `/`;
							}else{
								setCategoryLink(ca._id);
							}
							// handleCategory(ca.name, ca._id)
						}}
					>
						<Link href={'/'}>
							<Category color={category.name === ca.name ? '#476ADE' : '#000000'}>{ca.name}</Category>
						</Link>
					</div>
				))}
				</Categories>
			</Wrapper>

			

		</>
	)
}

export default Links
