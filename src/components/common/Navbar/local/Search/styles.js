import styled from 'styled-components'

export const Wrapper = styled.div`
	/*width: 300px;*/
	display: flex;
	gap: 9px;
	height: 32px;
	border-radius: 9px;
	border: 1px solid #828282;
	display: flex;
	align-items: center;
	background: #fafafa;
	padding-left: 9px;
	position: relative;
	@media(max-width: 600px){
		width: 30vw;
		overflow: hidden;
	}
`

export const Icon = styled.img`
	width: 18px;
	height: 18px;
	cursor: pointer;
`

export const SearchBar = styled.input`
	width: 230px;
	height: 20px;
	background: #fafafa;
	border: none;
	font-size: 15px;
	outline: none;
	@media(max-width: 600px) {
		width: 100%;
		margin-right: 4px;
	}
`

export const Result = styled.img`
	position: absolute;
	width: 24px;
	height: 24px;
	top: 3px;
	right: 4px;
	filter: invert(22%) sepia(94%) saturate(3877%) hue-rotate(112deg) brightness(92%) contrast(102%);
	opacity: ${props => props.loading ? 0 : 1};
	transition: .3s;
`