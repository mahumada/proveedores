import { Wrapper, SearchBar, Icon, Result } from './styles'
import { useEffect, useState } from 'react';

const Search = ({ setSearch, width, search }) => {
	const [searchHold, setSearchHold] = useState();
	useEffect(() => {
		setSearchHold(search);
	}, [search])
	const [loading, setLoading] = useState(true);

	return (
		<Wrapper width={width} onKeyDown={e => {if(e.code === 'Enter'){ setSearch(searchHold); setLoading(false) }}}>
			<Icon src='img/searchIcon.png' onClick={() => { setSearch(searchHold); setLoading(false); }} />
			<SearchBar
				placeholder={window.innerWidth > 450 && 'Buscar'}
				onChange={e => { setSearchHold(e.target.value); setLoading(true); }}
				value={searchHold}
			/>
			<Result src="img/done.svg" loading={loading ? 1 : 0}/>
		</Wrapper>
	)
}

export default Search
