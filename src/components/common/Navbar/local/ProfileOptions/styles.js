import styled from 'styled-components'

export const Wrapper = styled.div`
	position: fixed;
	top: 70px;
	right: 0;
	background: #ffffff;
	display: flex;
	flex-direction: column;
	justify-content: space-evenly;
	align-items: center;
	width: 180px;
	border-bottom-right-radius: 18px;
	border-bottom-left-radius: 18px;
	box-shadow: 0 0 3px rgba(0, 0, 0, 0.3);
	z-index: 999999999999;
	overflow: hidden;
`

export const Enlace = styled.a`
	font-size: 14px;
	color: #222;
	text-decoration: none;
	text-align: center;
	padding: 9px;
	width: 100%;
	cursor: pointer;

	&:hover {
		color: #000;
		background: #fbfbfb;
	}
`

export const Button = styled.button`
	font-size: 14px;
	color: #222;
	background: none;
	text-decoration: none;
	text-align: center;
	padding: 9px;
	width: 100%;
	border: none;
	cursor: pointer;

	&:hover {
		color: #000;
		background: #fbfbfb;
	}
`