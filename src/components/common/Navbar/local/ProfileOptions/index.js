import { Enlace, Wrapper, Button } from './styles'
import Link from 'next/link'

const ProfileOptions = () => {

	function logOut(){
		if (typeof window !== 'undefined') {
			// Perform localStorage action
			localStorage.clear();
		}
		window.location = '/';
		window.location.reload();
	}

	return (
		<Wrapper>
			<Link href={'/profile'}>
				<Enlace>Mi perfil</Enlace>
			</Link>
			<Link href={'/settings'}>
				<Enlace>Ajustes</Enlace>
			</Link>
			<Link href={'/'}>
				<Button onClick={()=>logOut()}>Cerrar sesion</Button>
			</Link>
		</Wrapper>
	)
}

export default ProfileOptions
