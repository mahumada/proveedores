import styled from 'styled-components'
import { GiHamburgerMenu } from 'react-icons/gi'

export const Wrapper = styled.div`
	width: 100%;
	padding: 0 24px 0 24px;
	height: 70px;
	box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.25);
	display: flex;
	align-items: center;
	justify-content: space-between;
	position: fixed;
	z-index: 99;
	background: white;
	overflow: hidden;
`

export const LeftContainer = styled.div`
	display: flex;
	align-items: center;
	gap: 20px;
	@media (max-width: 1275px){
		justify-content: space-between;
		width: calc(50% + 135px);
	}
	@media (max-width: 600px){
		width: calc(50% + 135px);
		justify-content: flex-start;
		gap: 5.5px;
	}
`

export const Icon = styled.img`
	width: 40px;
	height: 40px;
	border-radius: 50%;
	cursor: pointer;
`

export const Burger = styled(GiHamburgerMenu)`
	width: 35px;
	height: 35px;
	margin-right: 18px;

	margin-left: auto;
	fill: #000000;
	display: none;

 	@media screen and (max-width: 1275px) {
		display: flex;
	} 
`

export const Logo = styled.div`
	width: 33px;
	height: 33px;
	background: #476ade;
	border-radius: 7px;
`

export const Img = styled.img`
	width: 100px;
	cursor: pointer;
`