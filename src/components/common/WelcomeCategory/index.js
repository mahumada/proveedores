import axios from 'axios';
import { useState,useEffect,useRef } from 'react'

import { useForm } from "react-hook-form";
import useFetch from '../../../hooks/useFetch'
import {
	Container,
	Welcome,
	Title,
	Description,
	TableCategory,
	TheadCategory,
	TbodyCategory,
	ColHead,
	TdCategory,
	Text,
	TrCategory,
	Radius,
	Send,
	Form,
	Other,
	OtherDiv,
	Sent,
	SentImg,
	SentText
} from './styles'

const WelcomeCategory = ({ welcome }) => {

	const { register, handleSubmit, watch, formState: { errors } } = useForm({mode:"all"});
	const [provee, setProvee] = useState([])
	const [consume, setConsume] = useState([])
	const [user, setUser] = useState('');

	useEffect(()=> {
		async function getId(){
		  let local = localStorage.getItem('loggedUser');
		  if(local){
		  let localParse = JSON.parse(local);
			const userdata = await fetch(`/api/user/${localParse.id}`).then(res => res.json())
		  setUser(JSON.stringify(userdata.data)); 
			const newprovee = userdata.data.interestedCategories.filter(c => c.type === 'provee');
			const newconsume = userdata.data.interestedCategories.filter(c => c.type === 'consume');
			setProvee(newprovee);
			setConsume(newconsume);
		  }else{
				window.location = '/';
			}
	  }
		getId();
	  },[])

		let { data } = useFetch('/api/category', 'get')

		let handleClick = (value,type) => {
		if(type === "provee"){
			let newProvee = provee;
			let index = newProvee.findIndex(object => {
				return object.name === value;
			});
			index === -1 ? newProvee = [...newProvee, {name: value, type: "provee"}] : newProvee = newProvee.filter((n, i) => i !== index);
			setProvee(newProvee);
		}
		else if(type === "consume"){
			let newConsume = consume;
			let index = newConsume.findIndex(object => {
				return object.name === value
			})
			index === -1 ? newConsume = [...newConsume, {name: value, type: "consume"}] : newConsume = newConsume.filter((n, i) => i !== index);
			setConsume(newConsume);
		}
	}
	let onSubmit = async(input) => {
		let interestedCategories={interestedCategories:provee.concat(consume)}
		const response = await axios.put(`/api/user?id=${JSON.parse(user)._id}`,interestedCategories)
		sendInput && await axios.post(`/api/askedCategory`, { name: input.other, userId: JSON.parse(user)._id })
		setSent(true);
		setTimeout(()=>{
			setSent(false);
			window.location = '/';
		}, 3000)
	}

	const [sendInput, setSendInput] = useState(false);
	let input = watch("other")

	const [sent, setSent] = useState(false);

	return (
		<Container height={welcome ? '100vh' : '100%'}>
			{sent && <Sent>
        <SentImg src="/img/sent.png"/>
        <SentText>Categorias enviadas!</SentText>
      </Sent>}
			<Welcome>
				<Title>
					{welcome
						? 'Bienvenido a Compratodo'
						: 'Editar categorias preferidas'}
				</Title>
				<Description>
					Seleccione las categorias de productos que mejor se adapten
					al perfil de su empresa
				</Description>
			</Welcome>
			<Form onSubmit={handleSubmit(onSubmit)}>
			<TableCategory>
				<TheadCategory>
					{['Categoria', 'Provee', 'Consume'].map((col, key) => (
						<ColHead key={key}>{col}</ColHead>
					))}
				</TheadCategory>
				
					<TbodyCategory>
					{data.map((categoria, index) => {
						return (
						<TrCategory key={index}>
							<TdCategory>
								<Text>{categoria.name}</Text>
							</TdCategory>

							<TdCategory>
								<Radius
									checked={provee.map(c => c.name === categoria.name).includes(true)}
									type='checkbox'
									value={categoria.name}
									onChange={() => {
										handleClick(categoria.name, "provee")
									}}
								/>
							</TdCategory>
							<TdCategory>
								<Radius
									checked={consume.map(c => c.name === categoria.name).includes(true)}
									type='checkbox'
									value={categoria.name}
									onChange={() => {
										handleClick(categoria.name, 'consume')
									}}
								/>
							</TdCategory>
						</TrCategory>
					)}
					)}
				</TbodyCategory>
				<TbodyCategory>
					<TrCategory>
						<TdCategory>
							<Other type="text" placeholder="Otra categoria"name="other" {...register("other")}/>
						</TdCategory>
						<TdCategory>
									<Radius
										type='checkbox'
										value={input}
										onChange={() => {
											setSendInput(true)
										}}
									/>
								</TdCategory>
								<TdCategory>
									<Radius
										//checked={data[length-1]['consume']}
										type='checkbox'
										value={input}
										onChange={() => {
											setSendInput(true)
										}}
									/>
							</TdCategory>
					</TrCategory>
				</TbodyCategory>					
			</TableCategory>
			<Send type='submit'>Enviar</Send>
			</Form>	
		</Container>
	)
}
export default WelcomeCategory