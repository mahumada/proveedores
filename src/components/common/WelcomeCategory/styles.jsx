import styled from 'styled-components'

export const Container = styled.div`
	width: 100%;
	height: ${({ height }) => height};
	display: flex;
	flex-direction: column;
	align-items: center;
	justify-content: flex-start;
	gap: 16px;
	margin: 0 auto;
`

export const Welcome = styled.div`
	display: flex;
	flex-direction: column;
	gap: 9px;
	margin-left: 18px;
	margin-top: 18px;
`

export const Title = styled.p`
	font-weight: 500;
	font-size: 27px;
	color: #476ade;
	letter-spacing: -0.408px;
	text-align: center;
	
`

export const Description = styled.p`
	font-weight: 500;
	font-size: 13.5px;
	letter-spacing: -0.408px;
	color: #000000;
	text-align: center;
`

export const TableCategory = styled.div`
	width: min(90%, 810px);
	height: fit-content;
	font-size: 16px;
	display: flex;
	flex-direction: column;
	margin: 0;
	@media screen and (max-width: 1000px) {
		font-size: 15px;
	}
`

export const TheadCategory = styled.div`
	width: 100%;
	height: 40px;
	font-size: 0.9em;
	display: flex;
	border-top: 2px solid #ccc;
	border-bottom: 2px solid #ccc;
`

export const ColHead = styled.p`
	height: 40px;
	display: flex;
	justify-content: center;
	align-items: center;
	font-weight: normal;
	font-size: 0.9em;
	color: #000000;
	letter-spacing: -0.408px;
	&:nth-child(1) {
		width: 40%;
	}
	&:nth-child(2) {
		width: 20%;
	}
	&:nth-child(3) {
		width: 40%;
	}
`

export const TbodyCategory = styled.div`
	width: 100%;
	height: max-content;
	font-size: 0.9em;
	display: flex;
	flex-direction: column;
	overflow-y: auto;
	margin-top: 11px;

	&::-webkit-scrollbar {
		-webkit-appearance: none;
		appearance: none;
	}

	&::-webkit-scrollbar:vertical {
		width: 3px;
	}

	&::-webkit-scrollbar-button:increment {
		display: none;
	}

	&::-webkit-scrollbar-button {
		display: none;
	}

	&::-webkit-scrollbar-thumb {
		background: #797979;
		border-radius: 18px;
	}
`

export const TrCategory = styled.div`
	width: 100%;
	min-height: 40px;
	display: flex;
	font-weight: normal;
	font-size: 1em; /*16px*/
	letter-spacing: -0.408px;
	color: #828282;
`

export const TdCategory = styled.div`
	height: 100%;
	font-size: 0.9em;
	display: flex;
	justify-content: center;
	align-items: center;
	text-align: center;

	&:nth-child(1) {
		width: 40%;
	}
	&:nth-child(2) {
		width: 20%;
	}
	&:nth-child(3) {
		width: 40%;
	}
`
export const Form=styled.form`
	width: 100%;
	justify-content: center;
	align-items: center;
	display: flex;
	flex-direction: column;
`
export const OtherDiv=styled.div`
	font-weight: normal;
	font-size: 0.9em; /*16px*/
	letter-spacing: -0.408px;
	color: #828282;
	position: relative;
	left: 115px;
	width: max-content;
	display: flex;
	margin-bottom: 3px;
	@media (max-width:1024px){
		left: 27px;
	}
`
export const Text = styled.p``
export const Other = styled.input`
	height: 24px;
	background: #ffffff;
	border: 1px solid #ffffff;
	box-shadow: 0px 0px 5px rgba(0, 0, 0, 0.13);
	border-radius: 12.5px;
	outline: none;
	text-align: center;
	max-width: 95%;
`
export const Radius = styled.input`
	font-size: 1.2em;
	-webkit-appearance: none;
	appearance: none;
	background: #ffffff;
	border: 1px solid #828282;
	border-radius: 6px;
	width: 0.9em;
	height: 0.9em;
	cursor: pointer;

	&:checked {
		border: 5px solid #476ade;
	}
`

export const Send = styled.button`
	width: 100px;
	height: 36px;
	background: #476ade;
	border: 1px solid #476ade;
	border-radius: 100px;
	font-weight: 500;
	font-size: 14px;
	letter-spacing: -0.408px;
	align-self: center;
	color: #ffffff;
	margin-top: 18px;
	cursor: pointer;
`

export const Sent = styled.div`
	display: flex;
	flex-direction: column;
	align-items: center;
	justify-content: space-evenly;
	width: min(450px, 90%);
	height: min(360px, 90%);
	border-radius: 11px;
	background-color: #FFF;
	box-shadow: 0 0 4px rgba(0, 0, 0, .2);
	position: absolute;
	z-index: 99999999;
	top: calc(50% - 225px);
	right: calc(50% - 225px);
`

export const SentImg = styled.img`
	width: 60%;
`

export const SentText = styled.p`
	margin: 0;
	font-size: 16px;
	font-weight: 500;
`