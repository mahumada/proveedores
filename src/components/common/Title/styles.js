import styled from "styled-components";

export const TitleText = styled.h2`
  color: black;
  font-size: 18px;
  font-weight: 500;
`

export const Blob = styled.h3`
  background: #476ade;
  padding: 5px 10px 5px 10px;
  border-radius: 10px;
  color: white;
  font-size: 16px;
  font-weight: 700px;
  width: fit-content;
`

export const TitleContainer = styled.div`
  display: flex;
  align-items: center;
  margin: 0;
  gap: 8px;
`