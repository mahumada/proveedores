import styled from 'styled-components';

export const Container = styled.div`
	display: flex;
	width: 100%;
	height: 100%;
	scroll-snap-align: start;
	flex-direction: column;
	justify-content: center;
	align-items: center;
	position: relative;
	padding: 0;
	@media (max-width: 1100px){
		justify-content: flex-start;
		height: 100%;
		background: #FFF;
	}
`
export const Wrapper = styled.div`
	width: min(100%, 1000px);
	padding-bottom: 15px;
	height: min(405px, 65%);
	overflow: hidden;
	background-color: white;
	box-shadow: 0px 0px 5px rgba(0, 0, 0, 0.13);
	border-radius: 12px;
	@media (max-width: 1100px) {
		height: 100%;
		padding: 0;
		margin: 0;
		box-shadow: none;
	}
	@media (min-width: 1500px) and (min-height: 820px){
		width: 1300px;
		height: 550px;
		border-radius: 16px;
	}
`

export const Main = styled.div`
	display: flex;
	justify-content: center;
	align-items: center;
	flex-direction: column;
	width: 100%;
	height: 65%;
	position: relative;
	@media (max-width: 1100px) {
		height: 55%;
	}
	@media (min-width: 1500px) and (min-height: 820px){
		height: 70%;
	}
`

export const SubtitleWrapper = styled.div`
	display: flex;
	justify-content: center;
	align-items: center;
	flex-direction: row;
	width: 100%;
`

export const Subtitle = styled.p`
  font-style: normal;
  font-weight: bold;
  font-size: 20px;
  line-height: 20px;
  color: #476ade;
	@media (min-width: 1500px) and (min-height: 820px){
		font-size: 24px;
		line-height: 24px;	
	}
`;

export const Info = styled.div`
	display: flex;
	justify-content: center;
	align-items: flex-start;
	flex-direction: row;
	width: 100%;
	height: fit-content;
	@media (max-width: 1100px) {
		flex-direction: column;
		justify-content: flex-start;
		overflow-y: auto;
		width: 95%;
		border-radius: 8px;
		box-shadow: 0 0 4px rgba(0, 0, 0, 0.4);
		padding: 9px;
	}
`

export const BoxContainer = styled.div`
	display: flex;
	flex-direction: column;
	align-items: center;
	width: 38%;
	height: 100%;
	@media (max-width: 1100px) {
		width: 100%;
		height: 1000px;
	}
`
export const BoxContainer2 = styled.div`
	display: flex;
	align-items: center;
	flex-direction: column;
	width: 38%;
	height: 100%;
	@media (max-width: 1100px) {
		width: 100%;
		height: 1000px;
	}
`
export const BoxContainer3 = styled.div`
	display: flex;
	align-items: center;
	flex-direction: column;
	width: 24%;
	height: 100%;
	@media (max-width: 1100px) {
		width: 100%;
		height: 300px;
	}
`

export const Box1 = styled.div`
	height: 225px;
	padding: 14px;
	@media (max-width: 1100px) {
		height: max-content;
		min-height: auto;
		margin-bottom: 22px;
	}
	@media (min-width: 1500px) and (min-height: 820px){
		height: 300px;
	}
`
export const Box2 = styled.div`
	width: 100%;
	border-right: 1px solid #666;
	border-left: 1px solid #666;
	padding: 14px;
	height: 225px;
	@media (max-width: 1100px) {
		height: max-content;
		min-height: auto;
		margin-bottom: 40px;
		border: none;
	}
	@media (min-width: 1500px) and (min-height: 820px){
		height: 300px;	
	}
`
export const Box3 = styled.div`
	height: 225px;
	height: max-content;
	display: flex;
	justify-content: flex-start;
	align-items: center;
	flex-direction: column;
	padding: 14px;
	width: 100%;
	@media (min-width: 1500px) and (min-height: 820px){
		height: 300px;
	}
`

export const BoxContent = styled.div`
	display: flex;
	justify-content: space-evenly;
	align-items: flex-start;
	flex-direction: row;
	width: 100%;
	height: 100%;
	@media (max-width: 1100px) {
		margin: auto;
		align-items: center;
		flex-direction: column;
	}
`
export const BoxColumnContainer = styled.div`
	display: flex;
	flex-direction: column;
	align-items: flex-start;
	justify-content: flex-start;
`
export const BoxColumn = styled.div`
	display: flex;
	align-items: center;
	flex-direction: column;
	text-align: left;
	justify-content: flex-start;
	height: 100%;
	width: 100%;
	padding: 0 4px;
	@media (max-width: 1100px) {
		margin: auto;
	}
`
export const BoxColumnTitle = styled.p`
	font-style: normal;
	font-weight: 500;
	font-size: 15px;
	line-height: 20px;
	margin-bottom: 0px;
	height: 45px;
	width: 170px;
	color: black;
	display: flex;
	flex-direction: column;
	justify-content: center;
	@media (max-width: 1100px) {
		text-align: center;
	}
	@media (min-width: 1500px) and (min-height: 820px){
		font-size: 18px;
		line-height: 20px;	
		width: 215px;
	}
`

export const DoubleBoxColumnTitle = styled.p`
	font-style: normal;
	font-weight: 500;
	font-size: 15px;
	line-height: 20px;
	margin-bottom: 0px;
	height: 45px;
	width: 200px;
	color: black;
	display: flex;
	flex-direction: column;
	justify-content: center;
	@media (max-width: 1100px) {
		text-align: center;
	}
	@media (min-width: 1500px) and (min-height: 820px){
		font-size: 18px;
		line-height: 20px;	
		width: 280px;
	}
`

export const BoxColumnText = styled.p`
	font-style: normal;
	font-weight: 500;
	font-size: 13.5px;
	margin-bottom: 16px;
	width: 170px;

	color: rgba(0, 0, 0, 0.6);
	@media (max-width: 1100px) {
		text-align: center;
	}
	@media (min-width: 1500px) and (min-height: 820px){
		font-size: 15px;
		width: 215px;
	}
`

export const DoubleBoxColumnText = styled.p`
	font-style: normal;
	font-weight: 500;
	font-size: 13.5px;
	margin-bottom: 16px;
	width: 200px;

	color: rgba(0, 0, 0, 0.6);
	@media (max-width: 1100px) {
		text-align: center;
	}
	@media (min-width: 1500px) and (min-height: 820px){
		font-size: 15px;
		width: 280px;
	}
`

export const Date = styled.p`
	font-style: normal;
	font-weight: 500;
	font-size: 14px;
	color: rgba(0, 0, 0, 0.6);
	margin-right: 3%;
	@media (max-width: 1100px) {
		margin-right: 0%;
	}
	@media (min-width: 1500px) and (min-height: 820px){
		font-size: 15px;
	}
`

export const Title = styled.div`
	display: flex;
	justify-content: space-between;
	align-items: center;
	flex-direction: row;
	padding: 11px;
	margin-bottom: 32px;
	@media (max-width: 1100px) {
		flex-direction: column;
		padding-top: 36px;
	}
`

export const NameCompany = styled.div`
  display: flex;
  align-items: center;
  color: black;
  font-size: 18px;
  gap: 7.2px;
	@media (min-width: 1500px) and (min-height: 820px){
		font-size: 22px;
	}
`;

export const Category = styled.span`
  color: white;
  font-size: 0.9rem;
	font-weight: 500;
  background-color: #476ade;
	padding: 2px 11px;
	border-radius: 8px;
	@media (min-width: 1500px) and (min-height: 820px){
		font-size: 18px;
	}
`;

export const Footer = styled.div`
	display: flex;
	justify-content: flex-start;
	align-items: center;
	flex-direction: row;
	position: absolute;
	bottom: ${props => (props.open ? "160px" : "22px")};
	padding-top: 22px;
	transition: 0.7s;
	transition-timing-function: ease-in-out;
	border-radius: 12px;
	@media (max-height: 700px){
		bottom: ${props => (props.open ? "175px" : "16px")};
	}
	@media (max-width: 1100px) {
		flex-direction: column-reverse;
		margin-top: 30px;
		width: 100%;
		display: none;
	}
	@media (min-width: 1500px) and (min-height: 820px){
		bottom: ${props => (props.open ? "260px" : "20px")};
		@media(max-height: 750px){
			bottom: ${props => (props.open ? "260px" : "6px")};
		}
		@media(max-height: 700px){
			bottom: ${props => (props.open ? "260px" : "42px")};
		}
	}
`
export const ButtonsContainer = styled.div`
	display: flex;
	margin-top: 13.5px;
	width: 100%;
	justify-content: flex-start;
	padding-left: 18px;
	@media (max-width: 1100px) {
		display: none;
	}
`
export const ButtonsContainerMobile = styled.div`
  display: flex;
  flex-direction: column;
  width: 100%;
  background-color: white;
`;
export const ButtonsSubContainer = styled.div`
	display: flex;
	width: 100%;
	justify-content: space-around;
	padding: 0 6px;
	gap: 8px;
`

export const Arrows = styled.div`
	width: 80%;
	display: flex;
	align-items: center;
	justify-content: space-between;
	align-self: center;
	position: absolute;
	bottom: 0;
	padding: 4px 0;
	@media (min-width: 1100px){
		display: none;
	}
`

export const Arrow = styled.img`
	width: 16px;
	height: 11px;
	@media (min-width: 1500px) and (min-height: 820px){
		width: 18px;
		height: 12px;
	}
`

export const Buttons = styled.p`
	font-style: normal;
	font-weight: 500;
	font-size: 12.5px;
	color: #333333;
	border: 1px solid #476ade;
	box-sizing: border-box;
	border-radius: 8px;
	width: max-content;
	margin-right: 20px;
	text-align: center;
	cursor: pointer;
	padding: 2px 8px;
	background: #FFF;
	display: flex;
	align-items: center;
	justify-content: center;
	cursor: pointer;
	
	@media (max-width: 1100px) {
		margin: 0;
		margin-bottom: 4px;
		width: 168px;
		font-size: 13px;
	}

	@media (min-width: 1500px) and (min-height: 820px){
		font-size: 16px;
	}
`

export const Products = styled.p`
	width: 130px;
	height: 100%;
	font-style: normal;
	font-weight: 500;
	font-size: 12px;
	color: #333333;
	text-align: center;
	padding: 5px;
	transition: 0.7s;
	cursor: pointer;
	@media (max-width: 1100px) {
		margin: 0;
		margin-bottom: 20px;
	}
	@media (min-width: 1500px) and (min-height: 820px){
		font-size: 16px;
		width: 180px;
	}
`

export const Options = styled.div`
	display: flex;
	justify-content: flex-start;
	align-items: center;
	flex-direction: column;
	position: absolute;
	bottom: 0px;
	width: min(100%, 1080px);
	height: ${props => (props.active ? "150px" : "0px")};
	padding: ${props => (props.active ? "11px 0" : "0px")};
	background-color: ${props => (props.active ? "white" : "transparent")};
	transition: 0.7s;
	transition-timing-function: ease-in-out;
	border-radius: 12px;
	overflow-y: scroll;
	@media (max-height: 700px){
		bottom: 15px;
	}
	@media (max-width: 1100px) {
		height: ${props => (props.active ? "80%" : "70px")};
		width: 100%;
		background-color: ${props => (props.active ? "white" : "none")};
		overflow: ${props => props.active ? 'auto' : 'hidden'};
		border-top-left-radius: 12px;
		border-top-right-radius: 12px;
		box-shadow: ${props => props.active ? '0 0px 8px rgba(0, 0, 0, .5)' : 'none'};
		bottom: 40px;
	}
	@media (min-width: 1500px) and (min-height: 820px){
		height: ${props => (props.active ? "250px" : "0px")};
		width: 1300px;
	}
`
export const OptionBox = styled.div`
  display: flex;
  padding: 9px;
  align-items: center;
  justify-content: space-evenly;
  flex-direction: row;
  background: #ffffff;
  box-shadow: 0px 0px 5px rgba(0, 0, 0, 0.13);
  border-radius: 12.5px;
  width: 97%;
  height: 60px;
  margin: 4px 0;

	@media (max-width: 1100px) {
		flex-wrap: nowrap;
		justify-content: center;
		align-items: flex-start;
		flex-direction: column;
		height: max-content;
		width: 90%;
		margin-bottom: 10px;
		margin-top: 10px;
		padding: 50px;
	}
	@media (max-width: 412px) {
		flex-wrap: nowrap;
		justify-content: center;
		align-items: flex-start;
		flex-direction: column;
		height: max-content;
		width: 90%;
		margin-bottom: 10px;
		margin-top: 10px;
		padding: 10px;
	}
	@media (min-width: 1500px) and (min-height: 820px){
		height: 70px;
	}
`

export const OptionText = styled.div`
  display: flex;
  justify-content: flex-start;
  align-items: center;
  flex-direction: row;
  margin-right: 18px;
  width: 15%;
	cursor: default;

	@media (max-width: 1100px) {
		margin-bottom: 15px;
		margin-left: 20%;
		&:nth-child(2) {
			width: 50%;
		}
		&:nth-child(5) {
			width: 90%;
		}
		&:nth-child(3) {
			width: 70%;
		}
		&:nth-child(4) {
			width: 70%;
		}
		&:nth-child(6) {
			width: 70%;
		}
		&:nth-child(7) {
			width: 70%;
		}
	}
`
export const OptionP = styled.p`
  font-style: normal;
  font-weight: 500;
  font-size: 14px;
  width: auto;
  color: rgba(0, 0, 0, 0.6);
  padding: 5px;
  margin: 0px;
	max-height: 40px;
	overflow: ${props => props.description ? "auto" : "hidden"};
	cursor: ${props => props.pointer ? 'pointer' : 'default'};
	@media (min-width: 1500px) and (min-height: 820px){
		font-size: 16px;
	}
`;
export const OptionTitle = styled.p`
  font-style: normal;
  font-weight: 500;
  font-size: 14px;
  line-height: 22.5px;
  color: black;
  padding: 5px;
	@media (min-width: 1500px) and (min-height: 820px){
		font-size: 16px;
	}
`;

export const Input = styled.input`
  -webkit-appearance: none;
  appearance: none;
  background: #ffffff;
  border: 1px solid #828282;
  border-radius: 6px;
  width: 14px;
  height: 14px;
	font-size: 14px;

  cursor: pointer;
  margin-right: 13.5px;

	&:checked {
		border: 5px solid #476ade;
	}
	@media (max-width: 1100px) {
		margin-right: 90%;
		margin-bottom: 15px;
	}
	@media (min-width: 1500px) and (min-height: 820px){
		width: 16px;
		height: 16px;
	}
`

export const OptionImg = styled.img`
	margin-top: 9px;
	@media (max-width: 1100px) {
		margin-top: 20px;
		margin-left: 45%;
	}
`

export const ModalImageContainer = styled.div`
  max-width: 270px;
  max-height: 540px;
  margin: auto;
`;

export const ModalImage = styled.img`
  max-width: 100%;
  max-height: 100%;
`;
export const ModalTitle = styled.p`
  font-size: 21px;
  color: #476ade;
  text-align: center;
  margin-bottom: 60px;
`;

export const ButtonSaveModal = styled.button`
  background: #ffffff;
  border: 1px solid #476ade;
  box-sizing: border-box;
  border-radius: 8px;
  width: 45%;
  height: 25px;

  color: #476ade;
  cursor: pointer;
  &:hover {
    background-color: ${(props) => (props.props ? 'transparent' : '#476ADE')};
    color: ${(props) => (props.props ? '#476ADE' : '#FFFFFF')};
  }
`;
export const ModalButtonsContainer = styled.div`
  width: 80%;
  display: flex;
  align-items: center;

  justify-content: center;
  margin: auto;
`;
export const ModalOpen = styled.div`
  position: absolute;
  width: 100%;
  height: 100%;
  background-color: rgba(220, 220, 220, 0.7);
`;

export const InputContra = styled.input`
  font-style: normal;
  font-weight: 500;
  font-size: 14px;
  text-align: center;
  width: auto;
  color: rgba(0, 0, 0, 0.6);
  width: 80px;
  border: none;
  background-color: transparent;
  border-bottom: 1px solid #476ade;
  margin: 0px;
	outline: none;
	@media (min-width: 1500px) and (min-height: 820px){
		font-size: 16px;
	}
`;
export const selectAll = styled.button`
  background: #ffffff;
  border: 1px solid #476ade;
  box-sizing: border-box;
  border-radius: 8px;
  width: 45%;
  height: 25px;
  color: #476ade;
  cursor: pointer;
  &:hover {
    background-color: #476ade;
    color: #ffffff;
  }
`;
export const Selector = styled.div`
  margin: 2%;
`;
export const ModalImg = styled.img`
	filter: ${props => props.enabled ? 'none' : 'invert(91%) sepia(8%) saturate(0%) hue-rotate(181deg) brightness(82%) contrast(91%)'};
`
export const Sent = styled.div`
	display: flex;
	flex-direction: column;
	align-items: center;
	justify-content: space-evenly;
	width: min(450px, 90%);
	height: min(360px, 90%);
	border-radius: 12px;
	background-color: #FFF;
	box-shadow: 0 0 4px rgba(0, 0, 0, .2);
	position: absolute;
	z-index: 99999999;
`

export const SentImg = styled.img`
	width: 60%;
`

export const SentText = styled.p`
	margin: 0;
	font-size: 16px;
	font-weight: 500;
`

export const CounterOptions = styled.div`
	display: flex;
  padding: 9px;
  align-items: center;
  justify-content: space-evenly;
  flex-direction: row;
  background: #ffffff;
  box-shadow: 0px 0px 5px rgba(0, 0, 0, 0.13);
  border-radius: 12px;
  width: 97%;
  height: 60px;
  margin: 4px 0;
	flex-direction: row;

	@media (max-width: 1100px){
		flex-direction: column;
		align-items: center;
		height: auto;
		gap: 24px;
		padding: 12px 0;
		width: 90%;
	}
	@media (min-width: 1500px) and (min-height: 820px){
		height: 70px;
	}
`

export const CounterOptionText = styled.p`
	font-weight: 500;
	color: black;
	margin-right: 21px;
	width: 180px;
	text-align: left;

	@media (max-width: 1100px){
		text-decoration: underline;
		text-align: center;
	}
`

export const CounterOption = styled.div`
	width: 45%;
	display: flex;
	align-items: center;
	gap: 8px;
	justify-content: flex-start;
	@media (max-width: 1100px){
		justify-content: center;
		width: 95%;
	}
`

export const CounterOptionLabel = styled.label`
	font-weight: 500;
	color: black;
	cursor: pointer;
`

export const CounterOptionInput = styled.input`
  font-weight: 500;
  font-size: 14px;
  text-align: center;
  color: rgba(0, 0, 0, 0.6);
  border: none;
  background-color: transparent;
  border-bottom: 1px solid #476ade;
  margin: 0px;
	outline: none;
`

export const CounterOptionModalButtonContainer = styled.div`
	position: relative;
`

export const CounterOptionModalButton = styled.button`
	padding: 2px 11px;
	font-size: 14px;
	border: none;
	border-radius: 8px;
	background: #476ade;
	color: #FFF;
	font-weight: 600;
	transition: .6s;
	width: 195px;
`

export const CounterOptionModal = styled.div`
	width: 195px;
	height: ${props => props.open ? '100px' : 0};
	background: #FFF;
	position: absolute;
	bottom: 0;
	left: 0;
	border-radius: 7px;
	box-shadow: 0px 0px 5px rgba(0, 0, 0, 0.13);
	z-index: 9999999999999;
	transition: .6s;
	display: flex;
	flex-direction: column;
	gap: 11px;
	overflow: hidden;
	padding: ${props => props.open ? '11px 0' : 0};
`

export const ModalClose = styled.button`
	border: none;
	border-radius: 90px;
	height: ${props => props.open ? '16px' : 0};
	width: ${props => props.open ? '16px' : 0};
	position: absolute;
	top: ${props => props.open ? '4px' : '12px'};
	right: ${props => props.open ? '4px' : '12px'};
	background: #476ade;
	display: flex;
	align-items: center;
	justify-content: center;
	overflow: hidden;
	transition: .6s;
`

export const ModalOption = styled.div`
	display: flex;
	gap: 8px;
	padding: 0 11px;
	overflow: hidden;
	height: ${props => props.open ? 'auto' : 0};
`

export const ModalCheckbox = styled.input`
	border-radius: 4px;
`

export const ModalCheckboxLabel = styled.label`
	font-size: 14px;
`

export const ColumnSpacer = styled.div`
	height: ${props => props.height};

	@media (max-width: 1100px){
		display: none;
	}
`