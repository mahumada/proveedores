import React, { useState, useEffect } from 'react';
import axios from 'axios';
import Modal from '../Modal/index';
import { io } from 'socket.io-client';
import Link from 'next/link';
import {
  Date,
  Wrapper,
  Box1,
  Box2,
  Box3,
  NameCompany,
  Container,
  OptionP,
  Category,
  OptionText,
  OptionTitle,
  BoxContainer2,
  BoxContainer3,
  Info,
  Main,
  Input,
  OptionBox,
  Options,
  OptionImg,
  Subtitle,
  BoxContent,
  BoxColumn,
  BoxColumnTitle,
  BoxContainer,
  BoxColumnText,
  Title,
  Footer,
  Buttons,
  Products,
  ModalTitle,
  ModalImage,
  ModalImageContainer,
  ButtonSaveModal,
  ModalButtonsContainer,
  ModalOpen,
  InputContra,
	ButtonsContainerMobile,
	ButtonsSubContainer,
  selectAll,
  DoubleBoxColumnTitle,
  DoubleBoxColumnText,
  ModalImg,
  Sent,
  SentImg,
  SentText,
  Arrows,
  Arrow,
  NotesContainer,
  CounterOptions,
  CounterOptionLabel,
  CounterOptionInput,
  CounterOption,
  CounterOptionText,
  CounterOptionSelect,
  SelectOption,
  CounterOptionModalButton,
  CounterOptionModalButtonContainer,
  CounterOptionModal,
  ModalOption,
  ModalCheckbox,
  ModalCheckboxLabel,
  ModalClose,
  ColumnSpacer
} from "./styles";
import Image from 'next/image';

const Posts = ({ datos, edit, sellerId, categories, socket, id, setCategory, offered, seller, sellerData }) => {
  const [names, setNames] = useState([]);
  const [all, setall] = useState(false);
  const [modal, setModal] = useState({
    open: false,
    image: '',
  });
  const [description, setDescription] = useState(false);
  const [selected, setSelected] = useState({
    products: [],
  });
  const [sent, setSent] = useState(false);
  const [sentAccept, setSentAccept] = useState(false);
  

  const [active, setActive] = useState('');
  const [contraoferta, setcontraoferta] = useState(false);
  const [selectedcontra, setselectedcontra] = useState({
    products: [],
  });

  function useWindowSize() {
    const [size, setSize] = useState([window.innerHeight, window.innerWidth]);
    useEffect(() => {
      const handleResize = () => {
        setSize([window.innerHeight, window.innerWidth]);
      };
      window.addEventListener('resize', handleResize);
      return () => {
        window.removeEventListener('resize', handleResize);
      };
    }, []);
    return size;
  }
  const [height, width] = useWindowSize();

  useEffect(() => {
    let newNames = [];
    const getCategory = async() => {
      const cats = await axios.get(`/api/category/`);
      cats.data.map(cat => {if(categories.indexOf(cat._id) !== -1){newNames.push(cat.name)}});
      setNames(newNames);
    };
    getCategory();
  }, []);

  const [selectedProductsList, setSelectedProductsList] = useState([]);
  const handleSelect = (product) => {

    if(selectedProductsList.indexOf(product._id) === -1){
      setSelectedProductsList([...selectedProductsList, product._id]);
    }else{
      setSelectedProductsList(selectedProductsList.filter(p => p !== product._id));
    }

    handleSelectContraCopy(product);

    let filtered = selected.products.find((x) => x._id === product._id);
    if (filtered)
      setSelected({
        products: selected.products.filter((x) => x._id !== product._id),
      });
    else
      setSelected({
        products: [...selected.products, product],
      });
  };

  const handleSelectCopy = (product) => {
    let filtered = selected.products.find((x) => x._id === product._id);

    if (filtered)
      setSelected({
        products: selected.products.filter((x) => x._id !== product._id),
      });
    else
      setSelected({
        products: [...selected.products, product],
      });
  };

  const [selectedContrasList, setSelectedContrasList] = useState([]);

  const handleSelectContra = (e, product) => {
    
    if(selectedContrasList.indexOf(product._id) === -1){
        setSelectedContrasList([...selectedContrasList, product._id]);
    }else{
      setSelectedContrasList([...selectedContrasList.filter(c => c !== product._id )]);
    }

    handleSelectCopy(product);

    let producto = { ...product };
    let filtered = selectedcontra.products.filter(
      (x) => x._id === producto._id
    );

    if (filtered.length)
      setselectedcontra({
        ...selectedcontra,
        products: selectedcontra.products.filter((x) => x._id !== producto._id),
      });
    else{
      setselectedcontra({
        ...selectedcontra,
        products: [...selectedcontra.products, producto],
      });
    }
    
  };

  const handleSelectContraCopy = (product) => {
    let producto = { ...product };
    let filtered = selectedcontra.products.filter(
      (x) => x._id === producto._id
    );

    if (filtered.length)
      setselectedcontra({
        ...selectedcontra,
        products: selectedcontra.products.filter((x) => x._id !== producto._id),
      });
    else
      setselectedcontra({
        ...selectedcontra,
        products: [...selectedcontra.products, producto],
      });
  };
  function onClick() {
    if(active !== 'active'){
      setActive('active');
    }else{
      setActive('');
      setcontraoferta(false);
    }
  }

  function contraOferta() {
    setActive('active');
    setcontraoferta(!contraoferta);
    setContras(contras.map(contra => { return {...contra, price: '', quantity: ''} } ));
  }

  function onModal(e, image) {
    e.preventDefault();
    setModal({
      open: true,
      image: !image ? image : '',
    });
  }
  const accept = async () => {
    const userId = JSON.parse(localStorage.getItem('loggedUser'));
    if (userId && selected.products.length && seller._id) {
      const res = await axios.get(`/api/post/accept/${datos.id}`);
      console.log(res);
      const response = await axios.post('/api/offer', {
        variants: datos.variants.filter(p => selectedProductsList.indexOf(p._id) !== -1),
        type: 'Aceptado',
        post_id: datos.id,
        buyer_id: userId.id,
        seller_id: seller._id,
      });
      const response2 = await fetch('/api/sendEmail', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
				email: seller.email,
        subject: `Aceptaron tu producto en Milux!`,
        template: `accept`,
        context: {
          products: datos.variants.filter(v => selectedProductsList.indexOf(v._id) !== -1).map(v => { return v.variantType })
        }
      })
    }).then(response => response.json());
      setSentAccept(true);
      setTimeout(() => {
        setSentAccept(false);
      }, 3000)
    }
  };

  const createChat = () => {
    socket.emit('private message', {
      receiver: { id: sellerData._id, name: sellerData.username },
      text: 'Hola',
    });
    window.location = '/chat';
  };

  const [contras, setContras] = useState([]);
  useEffect(()=>{
    if(datos){
      let newcontras = [];
      datos.variants.map(v => newcontras.push(v));
      setContras(newcontras);
    }
  }, [datos])
  const onChangeContra = (e, property, index) => {
    e.preventDefault();
    let newcontras = contras.map((contra, i) => {
      if(i === index){
        contra[property] = e.target.value;
      }
      return contra;
    });
    setContras(newcontras);
  }

  const submitContra = async() => {
    let paymentMethod = "";
    paymentMethods.map((method, index) => {
      if(index !== 0){
        paymentMethod += `, ${method}`;
      }else{
        paymentMethod = method;
      }
    })
    let paymentTermsString = "";
    paymentTerms.map((method, index) => {
      if(index !== 0){
        paymentTermsString += `, ${method}`;
      }else{
        paymentTermsString = method;
      }
    })
    let obj = {
      variants: contras.filter(contra => selectedContrasList.indexOf(contra._id) !== -1),
      buyer_id: JSON.parse(localStorage.getItem('loggedUser')).id,
      seller_id: sellerId,
      post_id: datos.id,
      type: 'Contraoferta',
      paymentMethod,
      paymentTerms: paymentTermsString
    };
    let response = await axios.post('/api/offer/', obj);
    const response2 = await fetch('/api/sendEmail', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
				email: seller.email,
        subject: `Hicieron una contraoferta a tu producto en Milux!`,
        template: 'offer',
        context: {
          products: datos.variants.filter(v => selectedContrasList.indexOf(v._id) !== -1).map(v => { return v.variantType })
        }
      })
    }).then(response => response.json());
    setSent(true);
    setcontraoferta(false);
    setTimeout(() => {
      setSent(false);
    }, 3000)
  }

  function selectAll() {
    if (all === false || selected.products.length < datos.variants.length) {
      setSelected({
        products: datos.variants,
      });
      setselectedcontra({
        products: datos.variants,
      });
      datos.variants.map((box) => {
        let check = document.getElementById(`${box._id}`);
        check.checked = true;
      });
      setall(true);
    } else {
      setSelected({
        products: [],
      });
      setselectedcontra({
        products: [],
      });

      datos.variants.map((box) => {
        let check = document.getElementById(`${box._id}`);
        check.checked = false;
      });
      setall(false);
    }
  }

  if(setCategory && !names.includes(setCategory)){
    return <></>
  }

  const [modalMethod, setModalMethod] = useState(false)
  const methods = ["Efectivo", "Cheque", "Transferencia Bancaria"];
  const [modalCondition, setModalCondition] = useState(false)
  const conditions = ["Contado", "Financiado", "Contraentrega"];

  const [paymentMethods, setPaymentMethods] = useState([]);
  const handlePaymentCheckbox = e => {
    if(e.target.checked){
      if(!paymentMethods.includes(e.target.value)){
        setPaymentMethods([...paymentMethods, e.target.value]);
      }
    }else{
      if(paymentMethods.includes(e.target.value)){
        setPaymentMethods(paymentMethods.filter(p => p !== e.target.value));
      }
    }
  }

  const [paymentTerms, setPaymentTerms] = useState([]);
  const handleTermsCheckbox = e => {
    if(e.target.checked){
      if(!paymentTerms.includes(e.target.value)){
        setPaymentTerms([...paymentTerms, e.target.value]);
      }
    }else{
      if(paymentTerms.includes(e.target.value)){
        setPaymentTerms(paymentTerms.filter(p => p !== e.target.value));
      }
    }
  }

  const deletePost = async() => {
    try {
      const response = await fetch(`/api/post?_id=${datos.id}`, {
        method: 'DELETE'
      }).then(res => res.json());
      window.location.reload();
    }catch(err){
      console.error(err);
    }
  }

  return (
    <Container>
      {sent && <Sent>
        <SentImg src="/img/sent.png"/>
        <SentText>Oferta enviada con exito!</SentText>
      </Sent>}
      {sentAccept && <Sent>
        <SentImg src="/img/sent.png"/>
        <SentText>Productos aceptados exitosamente!</SentText>
      </Sent>}
      {modal.open && <ModalOpen></ModalOpen>}
      <Wrapper>
        <Title>
          <NameCompany>
            {seller && (
              <label style={{ fontWeight: 700}} >
                {seller?.fantasyName || seller?.businessName}
              </label>
            )}
            <div style={{ display: 'flex', flexDirection: 'row', flexWrap: 'wrap', gap: '6px'}}>
              {names.map((category, i) => (
                <Category key={i}>{category}</Category>
              ))}
            </div>
          </NameCompany>
          <Date>
            Valido desde: {datos.validationInit?.substring(0, 10)} hasta:{' '}
            {datos.validation.slice(0, 10)}{' '}
          </Date>
        </Title>

        <Main>
          <Arrows>
            <Arrow src="/img/arrowDownAccordion.png"/>
            <Arrow src="/img/arrowDownAccordion.png"/>
          </Arrows>
          <Info>
            <BoxContainer>
              <Subtitle>Cotización</Subtitle>
              <Box1>
                <BoxContent>
                  <BoxColumn>
                    <BoxColumnTitle>Tipo de publicación</BoxColumnTitle>
                    <BoxColumnText>{datos.pcd}</BoxColumnText>
                    <BoxColumnTitle>Notas</BoxColumnTitle>
                    <BoxColumnText>{datos.observations1}</BoxColumnText>
                  </BoxColumn>
                  <BoxColumn>
                    <BoxColumnTitle>Medio de Pago</BoxColumnTitle>
                    <BoxColumnText>{datos.payment && datos.payment.slice(0, datos.payment.length - 1).split(' ').join(', ')}</BoxColumnText>
                  </BoxColumn>
                </BoxContent>
              </Box1>
            </BoxContainer>
            <BoxContainer2>
              <Subtitle>Propuesta</Subtitle>
              <Box2>
                <BoxContent>
                  <BoxColumn>
                    <BoxColumnTitle>Monto mínimo</BoxColumnTitle>
                    <BoxColumnText>{datos.billing}</BoxColumnText>
                    {/* <BoxColumnTitle>Validez de la propuesta</BoxColumnTitle>
                    <BoxColumnText>
                      {datos.validation.split("T")[0]}
                    </BoxColumnText> */}
                    <BoxColumnTitle>Incluye flete</BoxColumnTitle>
                    <BoxColumnText>
                      {datos.includesTransportation ? 'Si' : 'No'}
                    </BoxColumnText>
                  </BoxColumn>
                  <BoxColumn>
                    <ColumnSpacer height="8px" />
                    <BoxColumnTitle>Plazo de entrega</BoxColumnTitle>
                    <BoxColumnText>{datos.deliveryDate}</BoxColumnText>
                    <BoxColumnTitle>Condiciones de pago</BoxColumnTitle>
                    <BoxColumnText>{datos.paymentTerms && datos.paymentTerms.split(' ').length > 1 ? datos.paymentTerms.slice(0, datos.paymentTerms.length - 1).split(' ').join(', ') : datos.paymentTerms}</BoxColumnText>
                    <BoxColumnTitle>Notas</BoxColumnTitle>
                    <BoxColumnText>{datos.observations2}</BoxColumnText>
                  </BoxColumn>
                </BoxContent>
              </Box2>
            </BoxContainer2>
            <BoxContainer3>
              <Subtitle>Contacto</Subtitle>
              <Box3>
                <BoxContent>
                  <BoxColumn style={{ width: "90%" }}>
                    <DoubleBoxColumnTitle>Dirección de entrega</DoubleBoxColumnTitle>
                    <DoubleBoxColumnText>{datos.deliveryAdress}</DoubleBoxColumnText>
                
                    <DoubleBoxColumnTitle>Contacto del responsable</DoubleBoxColumnTitle>
                    <DoubleBoxColumnText>{datos.contactName}</DoubleBoxColumnText>
                  </BoxColumn>
                </BoxContent>
              </Box3>
            </BoxContainer3>
          </Info>
        </Main>
        {edit === true ? (
          <Footer open={active}>
            <Products onClick={() => onClick()}>
              Ver productos
              <button
                onClick={() => onClick()}
                style={{
                  backgroundColor: 'transparent',
                  border: 'none',
                  cursor: 'pointer',
                }}>
                <img style={{marginLeft: '6px'}}
                  src={
                    active.length ? `${window.location.origin}/img/arrowUp.png` : `${window.location.origin}/img/arrowDown.png`
                  }></img>
              </button>
            </Products>
            <Link href={'/offers'}>
              <Buttons style={{ cursor: 'pointer' }}>Interacciones</Buttons>
            </Link>
            <Link style={{ cursor: 'pointer' }} href={`/edit/${datos.id}`}>
              <button
                onClick={() => (window.location.href = `/edit/${datos.id}`)}
                style={{
                  backgroundColor: 'transparent',
                  border: 'none',
                  cursor: 'pointer',
                }}>
                <img
                  style={{
                    cursor: 'pointer',
                  }}
                  src={'/img/pencilModal.png'}
                />
              </button>
            </Link>
            <div style={{ cursor: 'pointer', marginLeft: '12px' }}>
              <button
                onClick={() => deletePost()}
                style={{
                  backgroundColor: 'transparent',
                  border: 'none',
                  cursor: 'pointer',
                }}>
                <img
                  style={{
                    cursor: 'pointer',
                  }}
                  src={'/img/delete.png'}
                />
              </button>
            </div>
          </Footer>
        ) : (
          <Footer open={active}>
            <Products onClick={() => onClick()}>
              Ver productos
              <button
                onClick={() => onClick()}
                style={{
                  backgroundColor: 'transparent',
                  border: 'none',
                  cursor: 'pointer',
                }}>
                <img
                  src={active.length ? `${window.location.origin}/img/arrowUp.png` : `${window.location.origin}/img/arrowDown.png`}
                  style={{ marginLeft: '6px' }}></img>
              </button>
            </Products>

            {offered !== true && <Buttons onClick={accept}>Aceptar productos</Buttons>}

            {offered !== true && <Buttons
              onClick={contraOferta}
              style={
                contraoferta
                  ? { backgroundColor: 'transparent', color: 'black' }
                  : {}
              }>
              Realizar una contraoferta
            </Buttons>}
            <Buttons onClick={() => createChat()}>Contactar</Buttons>
            {contraoferta && (
              <Buttons
                onClick={() => submitContra()}
                style={{ background: '#476ade', color: 'white' }}>
                {' '}
                Enviar
              </Buttons>
            )}
          </Footer>
        )}
      </Wrapper>
      <Options active={active}>
			{
					width<1100?
					<ButtonsContainerMobile>
						<ButtonsSubContainer>
						{offered !== true && <Buttons onClick={accept}>Aceptar</Buttons> }
            {offered !== true && <Buttons
              onClick={contraOferta}
              style={
                contraoferta
                  ? { backgroundColor: "#476ADE", color: "white" }
                  : {}
              }
            >
              Contraoferta
            </Buttons>}
            <Buttons onClick={() => createChat()}>Contactar</Buttons>
						{contraoferta && <Buttons onClick={()=>submitContra()} style={{background: '#476ade', color: 'white'}}> Enviar</Buttons>}
						</ButtonsSubContainer>
						<ButtonsSubContainer>
						<Products active={active} onClick={() => onClick()}>
							Ver productos
							<button
								onClick={() => onClick()}
								style={{
									backgroundColor: 'transparent',
									border: 'none',
									cursor: 'pointer',
								}}
							>
								<img
									src={
										active.length ? `${window.location.origin}/img/arrowUp.png` : `${window.location.origin}/img/arrowDown.png`
									}
									style={{marginLeft: '6px'}}
								></img>
							</button>
						</Products>
						</ButtonsSubContainer>				
					</ButtonsContainerMobile>
					:null
				}
        {datos.variants && !contraoferta ? (
        <>
        {
           (active) && datos.variants.map((va) => (
              <OptionBox key={va._id}>
                <Input
                  id={va._id}
                  value={datos.variants._id}
                  onChange={() => handleSelect(va)}
                  checked={selectedProductsList.indexOf(va._id) !== -1}
                  type='checkbox'></Input>
                <OptionText>
                  <OptionTitle>Tipo:</OptionTitle>
                  <OptionP style={{ marginLeft: '7px' }}>
                    {va.variantType}
                  </OptionP>
                </OptionText>
                <OptionText>
                  <OptionTitle>Cantidad:</OptionTitle>
                  <OptionP style={{ marginLeft: '7px' }}>
                    {va.quantity} {va?.unit}
                  </OptionP>
                  {/* 	<button
									style={{
										backgroundColor: 'transparent',
										border: 'none',
										cursor: 'pointer',
									}}
								>
									<img src='img/arrowDown.png'></img>
								</button> */}
                </OptionText>
                <OptionText>
                  <OptionTitle>Precio: </OptionTitle>
                  <OptionP style={{ marginLeft: '2px' }}> $ {va.price}</OptionP>
                </OptionText>
                <OptionText onClick={() => setDescription(!description)}>
                  <OptionTitle>Descripción: </OptionTitle>
                  <OptionP description pointer={true}> {va.description && !description ? va.description.slice(0, 4) + '...' : va.description}</OptionP>
                </OptionText>
                <OptionText>
                  <OptionTitle>Marca: </OptionTitle>
                  <OptionP style={{ marginLeft: '0px' }}> {va.brand}</OptionP>
                </OptionText>
                <OptionText>
                  <OptionTitle>Color: </OptionTitle>
                  <OptionP style={{ marginLeft: '0px' }}> {va.color}</OptionP>
                </OptionText>
                <button
                  style={{
                    background: 'none',
                    border: 'none',
                    cursor: `${va.image ? 'pointer' : ''}`,
                  }}
                  onClick={(e) => va.image && onModal(e, va.image)}>
                  <ModalImg enabled={va.image} src={'/img/ModalImage.png'} alt='' />
                </button>
              </OptionBox>
            ))}
            <ButtonSaveModal
              props={active}
              style={
                active
                  ? {
                      display: 'block',
                      zIndex: '9',
                      width: '200px',
                      border: 'none',
                      fontWeight: '500',
                      fontSize: '16px',
                      alignSelf: 'flex-start',
                    }
                  : { display: 'none' }
              }
              onClick={() => selectAll()}>
              Seleccionar todos
            </ButtonSaveModal>
          </>
        ) : contraoferta && active ? (
          datos.variants.map((va, index) => (
            <OptionBox key={va._id}>
              <Input
                onChange={(e) => handleSelectContra(e, va)}
                type='checkbox'
                checked={selectedContrasList.indexOf(va._id) !== -1}></Input>
              <OptionText>
                <OptionTitle>Tipo:</OptionTitle>
                <OptionP style={{ marginLeft: '7px' }}>
                  {va.variantType}
                </OptionP>
              </OptionText>
              <OptionText>
                <OptionTitle>Cantidad:</OptionTitle>
                <InputContra
                  name='quantity'
                  onChange={(e) => onChangeContra(e, 'quantity', index)}
                  // placeholder={va.quantity + ' ' + va.unit}
                  // disabled={!selectedcontra.products.length}
                  value={contras[index]?.quantity}
                  style={{ marginLeft: '7px' }}></InputContra>
                {/* 	<button
									style={{
										backgroundColor: 'transparent',
										border: 'none',
										cursor: 'pointer',
									}}
								>
									<img src='img/arrowDown.png'></img>
								</button> */}
              </OptionText>
              <OptionText>
                <OptionTitle>Precio: </OptionTitle>
                <InputContra
                  name='price'
                  value={contras[index]?.price}
                  onChange={(e) => onChangeContra(e, 'price', index)}
                  // disabled={!selectedcontra.products.length}
                  // placeholder={'$ ' + va.price}
                  style={{ marginLeft: '2px' }}></InputContra>
              </OptionText>
              <OptionText>
                <OptionTitle>Descripción: </OptionTitle>
                <OptionP> {va.description}</OptionP>
              </OptionText>
              <OptionText>
                <OptionTitle>Marca: </OptionTitle>
                <OptionP style={{ marginLeft: '0px' }}> {va.brand}</OptionP>
              </OptionText>
              <OptionText>
                <OptionTitle>Color: </OptionTitle>
                <OptionP style={{ marginLeft: '0px' }}> {va.color}</OptionP>
              </OptionText>
              <button
                style={{
                  background: 'none',
                  border: 'none',
                  cursor: 'pointer',
                }}
                onClick={(e) => va.image && onModal(e, va.image)}>
                <ModalImg src='/img/ModalImage.png' alt='' />
              </button>
            </OptionBox>
          ))
        ) : null}
        {contraoferta && active && <CounterOptions>
          <CounterOptionText>Datos de Pago</CounterOptionText>
          <CounterOption onClick={() => setModalMethod(!modalMethod)}>
            <CounterOptionLabel>Medio de Pago:</CounterOptionLabel>
            <CounterOptionModalButtonContainer>
              <CounterOptionModal open={modalMethod} onMouseLeave={() => setModalMethod(false)}>
                <ModalClose open={modalMethod} onClick={() => setModalMethod(false)}><Image src="/img/hide.png" width="12px" height="12px" /></ModalClose>
                {methods.map((method, key) => {
                  return <ModalOption key={key} open={modalMethod}>
                    <ModalCheckbox type="checkbox" value={method} onClick={e => handlePaymentCheckbox(e)} />
                    <ModalCheckboxLabel>{method}</ModalCheckboxLabel>
                  </ModalOption>
                })}
              </CounterOptionModal>
              <CounterOptionModalButton clicked={modalMethod} onMouseEnter={() => setModalMethod(true)}>{paymentMethods.length ? "Seleccionado" : "Seleccionar"}</CounterOptionModalButton>
            </CounterOptionModalButtonContainer>
          </CounterOption>
          <CounterOption onClick={() => setModalCondition(!modalCondition)}>
            <CounterOptionLabel>Forma de Pago:</CounterOptionLabel>
            <CounterOptionModalButtonContainer>
              <CounterOptionModal open={modalCondition} onMouseLeave={() => setModalCondition(false)}>
                <ModalClose open={modalCondition} onClick={() => setModalCondition(false)}><Image src="/img/hide.png" width="12px" height="12px" /></ModalClose>
                {conditions.map((condition, key) => {
                  return <ModalOption key={key} open={modalCondition}>
                    <ModalCheckbox type="checkbox" value={condition} onClick={e => handleTermsCheckbox(e)} />
                    <ModalCheckboxLabel>{condition}</ModalCheckboxLabel>
                  </ModalOption>
                })}
              </CounterOptionModal>
              <CounterOptionModalButton clicked={modalCondition} onMouseEnter={() => setModalCondition(!modalCondition)}>{paymentTerms.length ? "Seleccionado" : "Seleccionar"}</CounterOptionModalButton>
            </CounterOptionModalButtonContainer>
          </CounterOption>
        </CounterOptions>}
      </Options>

      {modal.open && (
        <Modal>
          {/* 	<ModalTitle>Imagen</ModalTitle> 
          	<ModalImageContainer>
             <ModalImage src={datos?.variants?.image ? datos.variants.image  :''}/>
         		 </ModalImageContainer>
         		<ModalButtonsContainer style={{justifyContent: 'center'}}>
            <ButtonSaveModal onClick={()=>setModal(!modal)}>Salir</ButtonSaveModal>  
          	</ModalButtonsContainer>   */}
          <ModalTitle>Imagen</ModalTitle>
          <ModalImageContainer>
            <ModalImage
              src={datos?.variants[0].image ? datos.variants[0].image : ''}
            />
          </ModalImageContainer>
          <ModalButtonsContainer>
            <ButtonSaveModal onClick={() => setModal(!modal)}>
              Salir
            </ButtonSaveModal>
          </ModalButtonsContainer>
        </Modal>
      )}

      
    </Container>
  );
};
export default Posts;
