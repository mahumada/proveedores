import React, { useState, useEffect } from 'react';
import getPosts from '../../../../controllers/post/createPost';
import Modal from '../../Modal';
import axios from 'axios';
import { Image } from 'cloudinary-react';
import Switch from '../../switch';
import Accordion from '../../Accordion';
import AOS from 'aos';
import "aos/dist/aos.css";
import {
  Wrapper,
  Subtitle,
  InputWrapper,
  Input,
  InputContainer,
  InputTitle,
  OptionBox,
  Options,
  OptionInput,
  ButtonContainers,
  ButtonSave,
  ButtonDelete,
  TitleContainer,
  Company,
  Category,
  SelectCategories,
  SelectedContainer,
  Selected,
  SelectedDelete,
  CategoriesContainer,
  ModalButtonsContainer,
  ModalTitle,
  ModalParagraph,
  ButtonSaveModal,
  ButtonDeleteModal,
  ModalOpen,
  ProductsContainer,
  ProductsWrapper,
  ParagraphProducts,
  AddButton,
  AddNew,
  ModalImage,
  ModalImageContainer,
  InputDate,
  Products,
  ParagraphColumn,
  Next,
  CategoryOption,
  OptionSelect,
  SelectData,
  SelectDataContainer,
  SelectOption,
  Methods,
  Method,
  ProductButtons,
  ProductIndicator
} from "./styles";


const CreatePost = ({ categories }) => {
  //STATES
  const [modal, setModal] = useState(false);
  const [modalSubmit, setModalSubmit] = useState(false);
  const [modalImage, setModalImage] = useState(false);
  const [image, setImage] = useState("");
  const [processing, setProcessing] = useState(false);
  const [finished, setFinished] = useState(false);
  const [isToggledPdc, setToggledPdc] = useState(false);
  const [isToggledFlete, setToggledFlete] = useState(false);
  const [isToggledValidate, setToggledValidate] = useState(false);
  const [add, setAdd] = useState(false);
  const [categoriesSelected, setCategoriesSelected] = useState([]);
  const [renderCategories, setrenderCategories] = useState(false);
  const [renderBilling, setRenderBilling] = useState(false);
  const [edit, setEdit] = useState(false);
  const [product, setProduct] = useState({});

  //buttons form

  const [first, setfirst] = useState(false);
  const [second, setsecond] = useState(false);
  const [third, setthird] = useState(false);
  const [four, setfour] = useState(false);
  const [fifth, setfifth] = useState(false);
  //
  const [errors, seterrors] = useState({
    image: ''
  })

  const [data, setData] = useState({
    seller_id: "",
    quote: {
      pdc: "Comprar",
      paymentMethod: "",
      notes: "",
    },
    proposal: {
      minimum: "Sin monto mínimo",
      shippingInterval: "",
      endDate: "5",
      includesTransport: false,
      notes: "",
    },
    contact: {
      sellingAddress: "",
      contactName: "",
    },
    variants: [],
    categories: [],
  });
  const [error, setError] = useState(false);

  const [newVariant, setNewVariant] = useState({
    id: Math.round(Math.random(0, 1000)),
    variantType: "",
    quantity: '',
    price: '',
    description: "",
    brand: "",
    color: "",
    image: "",
    unit: "",
  });
  //ONTESTERROR

  function checkProperties(obj) {
    for (var key in obj) {
      if (obj[key] !== null && obj[key] != "") return false;
    }
    return true;
  }

  function checkEmpty() {
    let quote = checkProperties(data.quote);
    let proposal = checkProperties(data.proposal);
    let contact = checkProperties(data.contact);

    if (
      quote ||
      proposal ||
      contact ||
      data.variants.length === 0 ||
      data.categories.length === 0
    ) {
      setError(true);
    } else {
      setError(false);
    }
  }

  //ON CHANGES
  function onChangeQuote(e) {
    if(e.target.name === 'notes' && e.key === 'Enter' ) {
      setsecond(true);
      window.location.href = '#propuesta'
    }
    e.preventDefault();
    setData({
      ...data,
      quote: {
        ...data.quote,
        [e.target.name]: e.target.value,
      },
    });
  }

  const [paymentMethod, setPaymentMethod] = useState([]);
  function onChangeMethod(e) {
    e.preventDefault();
    setData({
      ...data,
      quote: {
        ...data.quote,
        [e.target.name]: e.target.value,
      },
    });
    if(paymentMethod.indexOf(e.target.value) === -1 && e.target.value !== "Seleccionar"){
      setPaymentMethod([...paymentMethod, e.target.value]);
    }
  }

  const deleteMethod = (method) => {
    setPaymentMethod(paymentMethod.filter(m => m !== method));
  }

  const [paymentTerms, setPaymentTerms] = useState([]);
  function onChangeTerms(e) {
    e.preventDefault();
    setData({
      ...data,
      quote: {
        ...data.quote,
        [e.target.name]: e.target.value,
      },
    });
    if(paymentTerms.indexOf(e.target.value) === -1 && e.target.value !== "Seleccionar"){
      setPaymentTerms([...paymentTerms, e.target.value]);
    }
  }

  const deleteTerm = (term) => {
    setPaymentTerms(paymentTerms.filter(m => m !== term));
  }

  // ONCHANGES --> TOGGLES
  function onTogglePdc() {
    setToggledPdc(!isToggledPdc);
    if (isToggledPdc === true);
    setData({
      ...data,
      quote: {
        ...data.quote,
        pdc: "Comprar",
      },
    });
    if (isToggledPdc === false) {
      setData({
        ...data,
        quote: {
          ...data.quote,
          pdc: "Vender",
        },
      });
    }
  }
  function onToggleFlete() {
    setToggledFlete(!isToggledFlete);
    if (isToggledFlete) {
      setData({
        ...data,
        proposal: {
          ...data.proposal,
          includesTransport: false,
        },
      });
    } else {
      setData({
        ...data,
        proposal: {
          ...data.proposal,
          includesTransport: true,
        },
      });
    }
  }

  function onToggleValidate() {
    setToggledValidate(!isToggledValidate);
    if (isToggledValidate) {
      setData({
        ...data,
        proposal: {
          ...data.proposal,
          endDate: "5",
        },
      });
    } else {
      setData({
        ...data,
        proposal: {
          ...data.proposal,
          endDate: "10",
        },
      });
    }
  }

  useEffect(() => {
    async function getId() {
      let local = await localStorage.getItem("loggedUser");
      let localParse = JSON.parse(local);
      let seller = await axios.get(
        `/api/user/getseller?_id=${localParse.id}`
      );
      setData({
        ...data,
        seller_id: seller.data._id,
      });
    }
    getId();
  }, []);

  useEffect(() => {
    AOS.init();
    AOS.refresh();
  }, []);

  //FINISH TOGGLES


  //ONCHANGE

  function onSelect(e){
    e.preventDefault();
    setData({
      ...data,
      proposal: {
        ...data.proposal,
        minimum: e.target.value === 'Sin monto mínimo' ? e.target.value : parseInt(e.target.value)
      }
    })
  }

  function editProduct(product) {
    setEdit(true);
    setProduct(product);
  }

  function onChangeProduct(e, filter) {
    e.preventDefault();
    if(filter){
      e.target.value = e.target.value.match(filter)
    }
    setProduct({
      ...product,
      [e.target.name] : e.target.value
    })
  }

  function AddNewProduct () {
    let variants = data.variants.filter(x => x.id !== product.id);
    setData({
      ...data,
      variants: [...variants, product]
    });

    setEdit(false);
    setProduct({});
  }

  async function onChangeVariants(e, filter) {
    e.preventDefault();
    if(filter){
      e.target.value = e.target.value.match(filter);
    }
    setNewVariant({
        ...newVariant,
        [e.target.name]: e.target.value,
      });
  }

  function onChangeContact(e) {
    e.preventDefault();
    setData({
      ...data,
      contact: {
        ...data.contact,
        [e.target.name]: e.target.value,
      },
    });
  }

  function onChangeProposal(e) {
    e.preventDefault();
    if (e.target.value === "true") {
      setData({
        ...data,
        proposal: {
          ...data.proposal,
          [e.target.name]: !data.proposal.includesTransport,
        },
      });
    } else {
      setData({
        ...data,
        proposal: {
          ...data.proposal,
          [e.target.name]: e.target.value,
        },
      });
    }
  }

  function onSelectCategory(e) {
  
    /* 	const index = e.target.selectedIndex;
  	const el = e.target.childNodes[index] */
    //const option =  el.getAttribute('label');
    let check = data.categories.find((x) => e.target.value === x);
    if (check) {
      return;
    } else {
      setData({
        ...data,
        categories: [...data.categories, e.target.value],
      });
      setCategoriesSelected([
        ...categoriesSelected,
        { name: e.target.name, value: e.target.value },
      ]);
    }
  }

  function onDeleteSelected(e) {
    setData({
      ...data,
      categories: data.categories.filter((x) => x !== e.target.value),
    });
    let filter = categoriesSelected.filter((x) => x.value !== e.target.value);
    setCategoriesSelected(filter);
  }

  function uploadImage(files) {
    if(files[0].size > 5242880) {
      alert('El archivo es demasiado grande. Corrobore que el mismo no supere los 5MB.');
      document.getElementById('imagen').value = '';

      return
    }
    const formData = new FormData();
    formData.append("file", files[0]);
    formData.append("upload_preset", "azihkt5g");
    axios
      .post(
        "https://api.cloudinary.com/v1_1/proveedores/image/upload",
        formData
      )
      .then((response) => {
        setNewVariant({
          ...newVariant,
          image: response.data.url,
        });
      });
  }

  //ON SUBMIT

  function AddNewVariant() {
    setData({
      ...data,
      variants: [...data.variants, newVariant],
    });
    setNewVariant({
      id: data.variants.length + 1,
      variantType: "",
      quantity: 0,
      price: 0,
      description: "",
      brand: "",
      color: "",
    });
    setAdd(!add);
  }

  function deleteVariant(id) {
    let filtered = data.variants.filter((variant) => variant.id !== id);
    setData({
      ...data,
      variants: filtered,
    });
  }

  function showImage(image) {
    setImage(image);
    setModalImage(!modalImage);
  }

  async function onSubmit() {
    let methods = '';
    paymentMethod.map(m => methods += m + ' ');
    let terms = '';
    paymentTerms.map(m => terms += m + ' ');
    const newData = {
      ...data,
      quote: {
        ...data.quote,
        paymentMethod: methods,
      },
      proposal: {
        ...data.proposal,
        paymentTerms: terms
      }
    };
    try {
      let body = JSON.stringify(newData);
      getPosts(body).then(response => {
        window.location.href = '/';
        // console.log(response);
      }).catch(err => alert(err))
      
      // setProcessing(!processing);
      // setFinished(!finished);
    } catch (error) {
      // console.log(error);
      alert('Error al crear Publicación. Revisar los datos porfavor!')
      setModalSubmit(!modalSubmit)
    }
   
  }

  function openModalSubmit() {
    checkEmpty();
    setModalSubmit(!modalSubmit);
  }



  return (
    <>
      {(modal || modalSubmit || modalImage) && <ModalOpen></ModalOpen>}
      <Wrapper>
        <TitleContainer>
          <Company>Crear Publicación</Company>
        </TitleContainer>
        <Subtitle data-aos="zoom-in-up" data-aos-duration="1500">
          Cotizacion
        </Subtitle>
        <InputWrapper>
          <InputContainer data-aos="fade-right" data-aos-duration="1500">
            <InputTitle>Tipo de Publicacion *</InputTitle>
            <Switch
              isToggled={isToggledPdc}
              onToggle={() => onTogglePdc()}
              on={"Comprar"}
              off={"Vender"}
            />
          </InputContainer>

          <InputContainer data-aos="fade-left" data-aos-duration="1500">
            <InputTitle>Medio de Pago *</InputTitle>
            <SelectData name="paymentMethod" onChange={(e)=> onChangeMethod(e)}>
                  <SelectOption>Seleccionar</SelectOption>
                  <SelectOption>Efectivo</SelectOption>
                  <SelectOption>Cheque</SelectOption>
                  <SelectOption>Transferencia Bancaria</SelectOption>
                  <SelectOption>Otros</SelectOption>
                </SelectData>
            <Methods>
              {paymentMethod.map(m => {
                return <Method onClick={() => deleteMethod(m)}>{m}</Method>
              })}
            </Methods>
          </InputContainer>
          <InputContainer data-aos="fade-left" data-aos-duration="1500">
            <InputTitle>Notas</InputTitle>
            <Input
              onChange={(e) => onChangeQuote(e)}
              // onKeyDown={ e => onkeypress1(e)}
              name="notes"
              type={"text"}
              value={data.quote?.notes}
            />
          </InputContainer>
        </InputWrapper>
        {!first && (
          <Next
            
            onClick={() => setfirst(true)}
          >
            <a href="#propuesta">
              <img
                style={{ width: "38px", height: "38px", cursor: "pointer" }}
                src="/img/next.png"
              />
            </a>
          </Next>
        )}
        {first && (
          <div
            style={{
              marginBottom: "200px",
              display: "flex",
              flexDirection: "column",
              alignItems: "center",
            }}
          >
            <div id="propuesta" style={{ height: "50px" }}></div>
            <Subtitle data-aos="zoom-in-up" data-aos-duration="1500">
              Propuesta
            </Subtitle>
            <InputWrapper>
              <InputContainer data-aos="fade-right" data-aos-duration="1500">
                <InputTitle>Incluye flete *</InputTitle>
                <Switch
                  isToggled={isToggledFlete}
                  onToggle={() => onToggleFlete()}
                  on={"No incluye"}
                  off={"Incluye"}
                />
              </InputContainer>
              <InputContainer data-aos="fade-right" data-aos-duration="1500">
                <InputTitle>Monto Mínimo *</InputTitle>
                <SelectData name="minimum" onChange={(e)=>onChangeProposal(e)}>
                  <SelectOption value={'Sin monto minimo'} defaultValue={true}>Sin monto mínimo</SelectOption>
                  <SelectOption value={'50000'}>$ 50.000</SelectOption>
                  <SelectOption value={'75000'}>$ 75.000</SelectOption>
                  <SelectOption value={'100000'}>$ 100.000</SelectOption>
                  <SelectOption value={'125000'}>$ 125.000</SelectOption>
                  <SelectOption value={'150000'}>$ 150.000</SelectOption>
                  <SelectOption value={'175000'}>$ 175.000</SelectOption>
                  <SelectOption value={'200000'}>$ 200.000 o más</SelectOption>
                </SelectData>
              </InputContainer>
              <InputContainer data-aos="fade-left" data-aos-duration="1500">
                <InputTitle>Validez de la propuesta *</InputTitle>
                <Input type={'date'} onKeyDown={(e) => e.preventDefault()} name='endDate' value={data.proposal?.endDate} onChange={(e)=>onChangeProposal(e)} min={new Date().toISOString().split('T')[0]} />
              </InputContainer>
              <InputWrapper>
              <InputContainer data-aos="fade-left" data-aos-duration="1500">
                <InputTitle>Plazo de entrega *</InputTitle>
                <SelectData name="shippingInterval" onChange={(e)=>onChangeProposal(e)}>
                  <SelectOption value={'Seleccionar'} default>Seleccionar</SelectOption>
                  <SelectOption value={'1 dia'}>1 dia</SelectOption>
                  <SelectOption value={'2 dias'}>2 dias</SelectOption>
                  <SelectOption value={'3 dias'}>3 dias</SelectOption>
                  <SelectOption value={'4 dias'}>4 dias</SelectOption>
                  <SelectOption value={'5 dias'}>5 dias</SelectOption>
                  <SelectOption value={'6 dias'}>6 dias</SelectOption>
                  <SelectOption value={'7 dias'}>7 dias</SelectOption>
                  <SelectOption value={'8 dias'}>8 dias</SelectOption>
                  <SelectOption value={'9 dias'}>9 dias</SelectOption>
                  <SelectOption value={'10 dias'}>10 dias</SelectOption>
                  <SelectOption value={'11 dias'}>11 dias</SelectOption>
                  <SelectOption value={'12 dias'}>12 dias</SelectOption>
                  <SelectOption value={'13 dias'}>13 dias</SelectOption>
                  <SelectOption value={'14+ dias'}>14+ dias</SelectOption>
                </SelectData>
              </InputContainer>

              <InputContainer data-aos="fade-right" data-aos-duration="1500">
                <InputTitle>Condiciones de pago *</InputTitle>
                <SelectData name="paymentTerms" onChange={(e)=>onChangeTerms(e)}>
                  <SelectOption value={'Seleccionar'} default>Seleccionar</SelectOption>
                  <SelectOption value={'Contado'}>Contado</SelectOption>
                  <SelectOption value={'Financiado'}>Financiado</SelectOption>
                  <SelectOption value={'Contraentrega'}>Contraentrega</SelectOption>
                </SelectData>
                <Methods>
                  {paymentTerms.map(m => {
                    return <Method onClick={() => deleteTerm(m)}>{m}</Method>
                  })}
                </Methods>
              </InputContainer>
              <InputContainer data-aos="fade-left" data-aos-duration="1500">
                <InputTitle>Notas</InputTitle>
                <Input
                  onChange={(e) => onChangeProposal(e)}
                  name="notes"
                  value={data.proposal?.notes}
                />
              </InputContainer>
              </InputWrapper>
            </InputWrapper>
            {!second && (
              <Next
                // data-aos="fade-right"
                // data-aos-duration="1500"
                onClick={() => setsecond(true)}
              >
                <a href="#contacto">
                  <img
                    style={{ width: "38px", height: "38px", cursor: "pointer" }}
                    src="/img/next.png"
                  />
                </a>
              </Next>
            )}
          </div>
        )}
        {second && (
          <div
            style={{
              marginBottom: "50px",
              display: "flex",
              flexDirection: "column",
              alignItems: "center",
              width: "100%",
            }}
          >
            <div id="contacto" style={{ height: "90px" }}></div>
            <Subtitle data-aos="zoom-in-up" data-aos-duration="1500">
              Contacto
            </Subtitle>
            <InputWrapper>
              <InputContainer data-aos="fade-right" data-aos-duration="1500">
                <InputTitle>Dirección de entrega *</InputTitle>
                <Input
                  onChange={(e) => onChangeContact(e)}
                  name="sellingAddress"
                  value={data.contact?.sellingAddress}
                />
              </InputContainer>

              <InputContainer data-aos="fade-left" data-aos-duration="1500">
                <InputTitle>Responsable de contacto (nombre)*</InputTitle>
                <Input
                  onChange={(e) => onChangeContact(e)}
                  name="contactName"
                  value={data.contact?.contactName}
                />
              </InputContainer>
            </InputWrapper>
            {/* {
          !third &&
        <Next data-aos='fade-right' data-aos-duration='1500' onClick={()=>setthird(true)}><a href='#productos'><img  style={{width: '38px', height: '38px', cursor: 'pointer'}} src='/img/next.png'/></a></Next>
        } */}
          </div>
        )}
        {second && (
          <>
            <Subtitle
              id="productos"
              
              style={{ margin: "0px 0 18px 0" }}
            >
              {" "}
              Productos
            </Subtitle>
            <Products>
              {data.variants.length && !edit
                ? data.variants.map((x, index) => {
                    return (
                      <>
                        <ProductsWrapper>    
                          <ProductIndicator>Producto {index+1}</ProductIndicator>
                          <ProductsContainer>
                            <ParagraphColumn>
                              <ParagraphProducts>
                                Nombre: {x.variantType || "-"}
                              </ParagraphProducts>

                              <ParagraphProducts>
                                Cantidad: {x.quantity || "-"}
                              </ParagraphProducts>

                              <ParagraphProducts>
                                Precio: {x.price || "-"}
                              </ParagraphProducts>
                            </ParagraphColumn>
                            <ParagraphColumn>
                              <ParagraphProducts>
                                Marca: {x.brand || "-"}
                              </ParagraphProducts>

                              <ParagraphProducts>
                                Color: {x.color || "-"}
                              </ParagraphProducts>

                              <ParagraphProducts>
                                Descripción: {x.description || "-"}
                              </ParagraphProducts>
                            </ParagraphColumn>
                          </ProductsContainer>
                          <ProductButtons>
                            {x.image && <button style={{background: 'none', border: 'none', cursor: 'pointer'}} onClick={() => showImage(x.image)}>
                              <img  src={"/img/imageUpload.png"} />
                            </button>}
                            <button style={{background: 'none', border: 'none', cursor: 'pointer'}} onClick={()=>editProduct(x)}>
                                <img src={"/img/pencilModal.png"}/>
                            </button>
                            <button onClick={() => deleteVariant(x.id)} style={{   background: "none",   border: "none",   cursor: "pointer", }}
                            >
                              <img src={"/img/delete.png"}></img>
                            </button>
                          </ProductButtons>
                        </ProductsWrapper>
                      </>
                    );
                  })
                : <>
                {
                  edit && (
                    <Options>
                <OptionBox>
                  <InputWrapper>
                    <InputContainer>
                      <InputTitle>Tipo *</InputTitle>
                      <OptionInput
                        onChange={(e) => onChangeProduct(e)}
                        name="variantType"
                        value={product.variantType}
                      />
                    </InputContainer>
                    <InputContainer>
                      <InputTitle>Cantidad *</InputTitle>
                      <OptionInput
                        onChange={(e) => onChangeProduct(e, new RegExp('[0-9]*'))}
                        name="quantity"
                        
                        value={product.quantity}
                      />
                    </InputContainer>

                    <InputContainer>
                      <InputTitle>Precio * </InputTitle>
                      <OptionInput
                        onChange={(e) => onChangeProduct(e, new RegExp('[0-9,.]*'))}
                        name="price"
                        type={"text"}
                        value={product.price}
                      />
                    </InputContainer>
                    <InputContainer>
                      <InputTitle>Unidad </InputTitle>
                      <OptionSelect
                        onChange={(e) => onChangeProduct(e)}
                        name="unit"
                        type={"text"}
                      >
                        <option value="kg">Kilogramos</option>
                        <option value="m">Metros</option>
                        <option value="u">Unidades</option>
                      </OptionSelect>
                    </InputContainer>
                    <InputContainer>
                      <InputTitle>Marca </InputTitle>
                      <OptionInput
                        onChange={(e) => onChangeProduct(e)}
                        name="brand"
                        type={"text"}
                        value={product.brand}
                      />
                    </InputContainer>

                    <InputContainer style={{ width: "90%", margin: "auto" }}>
                      <InputTitle>Descripción </InputTitle>
                      <OptionInput
                        onChange={(e) => onChangeProduct(e)}
                        name="description"
                        type={"text"}
                        value={product.description}
                      />
                    </InputContainer>

                    <InputContainer>
                      <InputTitle>Color</InputTitle>
                      <OptionInput
                        onChange={(e) => onChangeProduct(e)}
                        name="color"
                        type={"text"}
                        value={product.color}
                      />
                    </InputContainer>
                    <InputContainer>
                      <InputTitle>Imagen</InputTitle>
                      <OptionInput
                        type="file"
                        id='imagen'
                        onChange={(e) => uploadImage(e.target.files)}
                      />
                    </InputContainer>
                  </InputWrapper>
                  <AddButton
                    onClick={() => product.variantType && AddNewProduct()}
                  >
                    Finalizar
                  </AddButton>
                </OptionBox>
              </Options>
                  )
                }
                </>
                }
            </Products>
            {!add && (
              <AddNew
                
                onClick={() => setAdd(!add)}
              >
                Añadir producto +
              </AddNew>
            )}
            {add && (
              <Options>
                <OptionBox>
                  <InputWrapper>
                    <InputContainer>
                      <InputTitle>Tipo *</InputTitle>
                      <OptionInput
                        onChange={(e) => onChangeVariants(e)}
                        name="variantType"
                        value={newVariant.variantType}
                      />
                    </InputContainer>
                    <InputContainer>
                      <InputTitle>Cantidad *</InputTitle>
                      <OptionInput
                        onChange={(e) => onChangeVariants(e, new RegExp('[0-9]*'))}
                        name="quantity"
                        type={"text"}
                        value={newVariant.quantity}
                      />
                    </InputContainer>

                    <InputContainer>
                      <InputTitle>Precio * </InputTitle>
                      <OptionInput
                        onChange={(e) => onChangeVariants(e, new RegExp('[0-9,.]*'))}
                        name="price"
                        type={"text"}
                        value={newVariant.price}
                      />
                    </InputContainer>
                    <InputContainer>
                      <InputTitle>Unidad </InputTitle>
                      <OptionSelect
                        onChange={(e) => onChangeVariants(e)}
                        name="unit"
                        type={"text"}
                      >
                        <option value="kg">Kilogramos</option>
                        <option value="m">Metros</option>
                        <option value="u">Unidades</option>
                      </OptionSelect>
                    </InputContainer>
                    <InputContainer>
                      <InputTitle>Marca </InputTitle>
                      <OptionInput
                        onChange={(e) => onChangeVariants(e)}
                        name="brand"
                        type={"text"}
                        value={newVariant.brand}
                      />
                    </InputContainer>

                    <InputContainer style={{ width: "90%", margin: "auto" }}>
                      <InputTitle>Descripción </InputTitle>
                      <OptionInput
                        onChange={(e) => onChangeVariants(e)}
                        name="description"
                        type={"text"}
                        value={newVariant.description}
                      />
                    </InputContainer>

                    <InputContainer>
                      <InputTitle>Color</InputTitle>
                      <OptionInput
                        onChange={(e) => onChangeVariants(e)}
                        name="color"
                        type={"text"}
                        value={newVariant.color}
                      />
                    </InputContainer>
                    <InputContainer>
                      <InputTitle>Imagen</InputTitle>
                      <OptionInput
                        type="file"
                        id='imagen'
                        onChange={(e) => uploadImage(e.target.files)}
                      />
                    </InputContainer>
                  </InputWrapper>
                  <AddButton
                    onClick={() => newVariant.variantType && AddNewVariant()}
                  >
                    Finalizar
                  </AddButton>
                </OptionBox>
              </Options>
            )}
            {!four && (
              <Next
              // data-aos="fade-right"
              // data-aos-duration="1500"
                onClick={() => setfour(true)}
              >
                <a href="#categorias">
                  <img
                    style={{ width: "38px", height: "38px", cursor: "pointer", marginTop: '18px' }}
                    src="/img/next.png"
                  />
                </a>
              </Next>
            )}
          </>
        )}
        {four && (
          <>
            <div id="categorias" style={{ height: "50px" }}></div>
            <Subtitle data-aos="fade-right" data-aos-duration="1500">
              Categorias
            </Subtitle>
            <CategoriesContainer data-aos="flip-up" data-aos-duration="1500">
              <p
                style={{ display: "flex", gap: "10px", alignItems: "center", cursor: 'pointer' }}
                onClick={() => {
                  setrenderCategories(!renderCategories);
                }}
              >
                Seleccionar
                {renderCategories ? (
                  <img src="/img/arrowUp.png" />
                ) : (
                  <img src="/img/arrowDown.png" />
                )}
              </p>

              <SelectCategories renderCategories={renderCategories}>
                {categories.map((x, i) => {
                  return (
                    <div key={i}>
                      <CategoryOption
                        onClick={(e) => onSelectCategory(e)}
                        name={x.name}
                        key={x._id}
                        value={x._id}
                      >
                        {x.name}
                      </CategoryOption>
                    </div>
                  );
                })}
              </SelectCategories>
              <SelectedContainer>
                {categoriesSelected.length
                  ? categoriesSelected.map((cat) => {
                      return (
                        <Selected key={cat.name}>
                          {cat.name}{" "}
                          <SelectedDelete
                            onClick={(e) => onDeleteSelected(e)}
                            value={cat.value}
                          >
                            x
                          </SelectedDelete>
                        </Selected>
                      );
                    })
                  : null}
              </SelectedContainer>
            </CategoriesContainer>
          </>
        )}
        {four && (
          <ButtonContainers>
            <div >
              <ButtonDelete onClick={() => setModal(!modal)}>
                Cancelar
              </ButtonDelete>
            </div>
            <div >
              <ButtonSave onClick={() => openModalSubmit()}>
                Crear Publicacion
              </ButtonSave>
            </div>
          </ButtonContainers>
        )}
      </Wrapper>
      {modal && (
        <Modal>
          <ModalTitle>Cancelar</ModalTitle>
          <ModalParagraph>
            Seguro que desea salir? Sus cambios no se guardarán
          </ModalParagraph>
          <ModalButtonsContainer>
            <ButtonDeleteModal onClick={() => setModal(!modal)}>
              Cancelar
            </ButtonDeleteModal>
            <ButtonSaveModal onClick={() => (window.location.href = "/")}>
              Salir
            </ButtonSaveModal>
          </ModalButtonsContainer>
        </Modal>
      )}
      {modalSubmit && (
        <Modal>
          {error ? (
            <>
              <ModalTitle>Crear publicación</ModalTitle>
              <ModalParagraph>
                Faltan datos por completar. Por favor, revise el formulario.{" "}
              </ModalParagraph>
              <ModalButtonsContainer style={{ justifyContent: "center" }}>
                <ButtonSaveModal onClick={() => setModalSubmit(!modalSubmit)}>
                  Volver
                </ButtonSaveModal>
              </ModalButtonsContainer>
            </>
          ) : !finished ? (
            <>
              <ModalTitle>Crear publicación</ModalTitle>
              <ModalParagraph>
                Se creará la publicación con los datos ingresados. ¿Desea
                continuar?
              </ModalParagraph>
              <ModalButtonsContainer>
                <ButtonDeleteModal onClick={() => setModalSubmit(!modalSubmit)}>
                  Cancelar
                </ButtonDeleteModal>
                <ButtonSaveModal disabled={error} onClick={() => onSubmit()}>
                  Continuar
                </ButtonSaveModal>
              </ModalButtonsContainer>
            </>
          ) : (<></>
            // finished && (
            //   <>
            //     <ModalTitle>Crear publicación</ModalTitle>
            //     <ModalParagraph>Publicacion creada con éxito! </ModalParagraph>
            //     <ModalButtonsContainer style={{ justifyContent: "center" }}>
            //       <ButtonSaveModal onClick={() => (window.location.href = "/")}>
            //         Continuar
            //       </ButtonSaveModal>
            //     </ModalButtonsContainer>
            //   </>
            // )
          )}
        </Modal>
      )}

      {modalImage && (
        <Modal>
          <ModalTitle>Imagen</ModalTitle>
          <ModalImageContainer>
            <ModalImage src={image ? image : ""} />
          </ModalImageContainer>
          <ModalButtonsContainer style={{ justifyContent: "center" }}>
            <ButtonSaveModal
              onClick={() => {
                setModalImage(!modalImage);
              }}
            >
              Salir
            </ButtonSaveModal>
          </ModalButtonsContainer>
        </Modal>
      )}
    </>
  );
};

export default CreatePost;
