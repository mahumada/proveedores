import React from 'react';
import { useCategoriesContext } from '../../../context/categoriesContext';
import { Button, ButtonIcon, Container, Item, Label, Selector } from './styles';

const Filter = ({ setOrderBy }) => {
  const { category } = useCategoriesContext();
  return (
    <Container>
        <Item><Label>Categoria seleccionada:</Label>{category.name && <Button onClick={() => window.location = window.location.pathname}>{category.name}<ButtonIcon src="/img/cancel.png" /></Button>}</Item>
        <Item>
            <Label>Ordenar por:</Label>
            <Selector defaultValue='' onChange={(e) => setOrderBy(e.target.value)}>
                <option value=''>Seleccionar</option>
                <option value='newest'>Más Recientes</option>
                <option value='olds'>Más Antiguos</option>
                {/* <option value='relevants'>Relevantes</option> */}
            </Selector>
        </Item>
    </Container>
  )
}

export default Filter