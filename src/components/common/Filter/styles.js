import styled from "styled-components";

export const Container = styled.div`
    display: flex;
    flex-direction: row;
    justify-content: space-evenly;
    align-items: center;
    width: 100%;
    position: fixed;
    top: 90px;
    font-size: 14.5px;
    z-index: 10;
    @media (max-width: 1100px){
        top: 85px;
        font-size: min(13px, 2.5vw);
        color: black;
    }
    @media (min-width: 1500px){
        top: 120px;
        font-size: 16px;
    }
`

export const Item = styled.div`
    display: flex;
    align-items: center;
    gap: 6px;
`

export const Label = styled.p`
    font-weight: 600;
`

export const Selector = styled.select`
    background: none;
    border: none;
    color: #476ade;
    font-weight: 700;
    font-size: 14.5px;
    @media (max-width: 1100px){
        font-size: min(13px, 2.5vw);
    }
    @media (min-width: 1500px){
        font-size: 16px;
    }
`

export const Button = styled.button`
    background: #476ade;
    border-radius: 4px;
    border: none;
    font-weight: 500;
    color: #FFF;
    padding: 2px 10px;
    font-size: 15px;
    display: flex;
    align-items: center;
    gap: min(6px, 0.6vw);
    cursor: pointer;

    @media (max-width: 600px){
        font-size: 2vw;
        padding: .3vw .6vw;
    }
`
export const ButtonIcon = styled.img`
    width: 10px;
    height: 10px;
    cursor: pointer;
    @media (max-width: 600px){
        width: 1.2vw;
        height: 1.2vw;
    }
`