import styled from 'styled-components';

export const Wrapper = styled.div`
  width: 100%;
  height: 100%;
  display:flex;
  justify-content: center;
  align-items: center;
  position: absolute;
  z-index: 999999999999999999999999;
  background: #FFF;
`

export const Logo = styled.img`
  width: 250px;
`