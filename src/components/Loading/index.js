import React from 'react'
import { Logo, Wrapper } from './styles'

const Loading = () => {
  return (
    <Wrapper>
      <Logo src="/img/logo2.0.png" />
    </Wrapper>
  )
}

export default Loading